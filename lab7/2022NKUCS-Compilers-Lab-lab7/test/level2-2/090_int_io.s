	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.section .rodata
	.global ascii_0
	.align 4
	.size ascii_0, 4
ascii_0:
	.word 48
	.text
	.global my_getint
	.type my_getint , %function
my_getint:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L73:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L77
.L76:
	bl getch
	mov r4, r0
	ldr r5, addr_ascii_00
	ldr r6, [r5]
	sub r5, r4, r6
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L82
	b .L88
.L77:
	ldr r4, =1
	cmp r4, #0
	bne .L76
	b .L80
.L78:
	ldr r4, [fp, #-4]
	str r4, [fp, #-8]
	b .L95
.L80:
	b .L78
.L82:
	b .L77
.L83:
	b .L78
.L84:
	b .L77
.L85:
	ldr r4, [fp, #-4]
	cmp r4, #9
	movgt r4, #1
	movle r4, #0
	bgt .L82
	b .L91
.L88:
	b .L85
.L91:
	b .L83
.L92:
	b .L84
.L93:
	b .L84
.L94:
	bl getch
	mov r4, r0
	ldr r5, addr_ascii_00
	ldr r6, [r5]
	sub r5, r4, r6
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	movge r4, #1
	movlt r4, #0
	bge .L103
	b .L106
.L95:
	ldr r4, =1
	cmp r4, #0
	bne .L94
	b .L98
.L96:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
.L98:
	b .L96
.L100:
	ldr r4, [fp, #-8]
	ldr r5, =10
	mul r6, r4, r5
	ldr r4, [fp, #-4]
	add r5, r6, r4
	str r5, [fp, #-8]
	b .L102
.L101:
	b .L96
.L102:
	b .L95
.L103:
	ldr r4, [fp, #-4]
	cmp r4, #9
	movle r4, #1
	movgt r4, #0
	ble .L100
	b .L109
.L106:
	b .L101
.L109:
	b .L101
.L110:
	b .L102

	.global my_putint
	.type my_putint , %function
my_putint:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #72
.L111:
	str r0, [fp, #-72]
	ldr r4, =0
	str r4, [fp, #-4]
	b .L116
.L115:
	ldr r4, [fp, #-72]
	ldr r5, =10
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	ldr r4, addr_ascii_00
	ldr r5, [r4]
	add r4, r6, r5
	ldr r5, [fp, #-4]
	mov r6, #-68
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	str r4, [r6]
	ldr r4, [fp, #-72]
	ldr r5, =10
	sdiv r6, r4, r5
	str r6, [fp, #-72]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L116
.L116:
	ldr r4, [fp, #-72]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L115
	b .L120
.L117:
	b .L122
.L118:
	add sp, sp, #72
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L119:
	add sp, sp, #72
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L120:
	b .L117
.L121:
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r5, #-68
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	mov r0, r4
	bl putch
	b .L122
.L122:
	ldr r4, [fp, #-4]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L121
	b .L126
.L123:
	add sp, sp, #72
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L124:
	add sp, sp, #72
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L125:
	add sp, sp, #72
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L126:
	b .L123

	.global main
	.type main , %function
main:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L128:
	bl my_getint
	mov r4, r0
	str r4, [fp, #-8]
	b .L131
.L130:
	bl my_getint
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl my_putint
	mov r0, #10
	bl putch
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	str r5, [fp, #-8]
	b .L131
.L131:
	ldr r4, [fp, #-8]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L130
	b .L135
.L132:
	mov r0, #0
	add sp, sp, #8
	pop {r4, r5, fp, lr}
	bx lr
.L135:
	b .L132

addr_ascii_00:
	.word ascii_0
