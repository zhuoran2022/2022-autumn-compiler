	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.comm arr1, 57600, 4
	.comm arr2, 107520, 4
	.text
	.global loop1
	.type loop1 , %function
loop1:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #36
.L296:
	str r0, [fp, #-36]
	str r1, [fp, #-32]
	ldr r4, =0
	str r4, [fp, #-28]
	b .L307
.L306:
	ldr r4, =0
	str r4, [fp, #-24]
	b .L317
.L307:
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-36]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L309
	b .L312
.L308:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L309:
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-32]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L306
	b .L315
.L310:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L311:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L312:
	b .L308
.L313:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L314:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L315:
	b .L308
.L316:
	ldr r4, =0
	str r4, [fp, #-20]
	b .L323
.L317:
	ldr r4, [fp, #-24]
	cmp r4, #2
	movlt r4, #1
	movge r4, #0
	blt .L316
	b .L321
.L318:
	ldr r4, [fp, #-28]
	add r5, r4, #1
	str r5, [fp, #-28]
	b .L307
.L319:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L320:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L321:
	b .L318
.L322:
	ldr r4, =0
	str r4, [fp, #-16]
	b .L329
.L323:
	ldr r4, [fp, #-20]
	cmp r4, #3
	movlt r4, #1
	movge r4, #0
	blt .L322
	b .L327
.L324:
	ldr r4, [fp, #-24]
	add r5, r4, #1
	str r5, [fp, #-24]
	b .L317
.L325:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L326:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L327:
	b .L324
.L328:
	ldr r4, =0
	str r4, [fp, #-12]
	b .L335
.L329:
	ldr r4, [fp, #-16]
	cmp r4, #4
	movlt r4, #1
	movge r4, #0
	blt .L328
	b .L333
.L330:
	ldr r4, [fp, #-20]
	add r5, r4, #1
	str r5, [fp, #-20]
	b .L323
.L331:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L332:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L333:
	b .L330
.L334:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L341
.L335:
	ldr r4, [fp, #-12]
	cmp r4, #5
	movlt r4, #1
	movge r4, #0
	blt .L334
	b .L339
.L336:
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L329
.L337:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L338:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L339:
	b .L336
.L340:
	ldr r4, =0
	str r4, [fp, #-4]
	b .L347
.L341:
	ldr r4, [fp, #-8]
	cmp r4, #6
	movlt r4, #1
	movge r4, #0
	blt .L340
	b .L345
.L342:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L335
.L343:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L344:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L345:
	b .L342
.L346:
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-24]
	add r6, r4, r5
	ldr r4, [fp, #-20]
	add r5, r6, r4
	ldr r4, [fp, #-16]
	add r6, r5, r4
	ldr r4, [fp, #-12]
	add r5, r6, r4
	ldr r4, [fp, #-8]
	add r6, r5, r4
	ldr r4, [fp, #-4]
	add r5, r6, r4
	ldr r4, [fp, #-36]
	add r6, r5, r4
	ldr r4, [fp, #-32]
	add r5, r6, r4
	ldr r4, [fp, #-28]
	ldr r6, addr_arr10
	ldr r7, =5760
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-24]
	ldr r7, =2880
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-20]
	ldr r8, =960
	mul r7, r6, r8
	add r6, r4, r7
	ldr r4, [fp, #-16]
	mov r8, #240
	mul r7, r4, r8
	add r4, r6, r7
	ldr r6, [fp, #-12]
	mov r7, #48
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [fp, #-8]
	mov r7, #8
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-4]
	mov r7, #4
	mul r8, r6, r7
	add r6, r4, r8
	str r5, [r6]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L347
	b .F0
.LTORG
addr_arr10:
	.word arr1
addr_arr20:
	.word arr2
.F0:
.L347:
	ldr r4, [fp, #-4]
	cmp r4, #2
	movlt r4, #1
	movge r4, #0
	blt .L346
	b .L351
.L348:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L341
.L349:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L350:
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L351:
	b .L348

	.global loop2
	.type loop2 , %function
loop2:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #28
.L358:
	ldr r4, =0
	str r4, [fp, #-28]
	b .L367
.L366:
	ldr r4, =0
	str r4, [fp, #-24]
	b .L373
.L367:
	ldr r4, [fp, #-28]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L366
	b .L371
.L368:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L369:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L370:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L371:
	b .L368
.L372:
	ldr r4, =0
	str r4, [fp, #-20]
	b .L379
.L373:
	ldr r4, [fp, #-24]
	cmp r4, #2
	movlt r4, #1
	movge r4, #0
	blt .L372
	b .L377
.L374:
	ldr r4, [fp, #-28]
	add r5, r4, #1
	str r5, [fp, #-28]
	b .L367
.L375:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L376:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L377:
	b .L374
.L378:
	ldr r4, =0
	str r4, [fp, #-16]
	b .L385
.L379:
	ldr r4, [fp, #-20]
	cmp r4, #3
	movlt r4, #1
	movge r4, #0
	blt .L378
	b .L383
.L380:
	ldr r4, [fp, #-24]
	add r5, r4, #1
	str r5, [fp, #-24]
	b .L373
.L381:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L382:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L383:
	b .L380
.L384:
	ldr r4, =0
	str r4, [fp, #-12]
	b .L391
.L385:
	ldr r4, [fp, #-16]
	cmp r4, #2
	movlt r4, #1
	movge r4, #0
	blt .L384
	b .L389
.L386:
	ldr r4, [fp, #-20]
	add r5, r4, #1
	str r5, [fp, #-20]
	b .L379
.L387:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L388:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L389:
	b .L386
.L390:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L397
.L391:
	ldr r4, [fp, #-12]
	cmp r4, #4
	movlt r4, #1
	movge r4, #0
	blt .L390
	b .L395
.L392:
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L385
.L393:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L394:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L395:
	b .L392
.L396:
	ldr r4, =0
	str r4, [fp, #-4]
	b .L403
.L397:
	ldr r4, [fp, #-8]
	cmp r4, #8
	movlt r4, #1
	movge r4, #0
	blt .L396
	b .L401
.L398:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L391
.L399:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L400:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L401:
	b .L398
.L402:
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-24]
	add r6, r4, r5
	ldr r4, [fp, #-16]
	add r5, r6, r4
	ldr r4, [fp, #-4]
	add r6, r5, r4
	ldr r4, [fp, #-28]
	ldr r5, addr_arr21
	ldr r7, =10752
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [fp, #-24]
	ldr r7, =5376
	mul r8, r4, r7
	add r4, r5, r8
	ldr r5, [fp, #-20]
	ldr r7, =1792
	mul r8, r5, r7
	add r5, r4, r8
	ldr r4, [fp, #-16]
	ldr r8, =896
	mul r7, r4, r8
	add r4, r5, r7
	ldr r5, [fp, #-12]
	mov r7, #224
	mul r8, r5, r7
	add r5, r4, r8
	ldr r4, [fp, #-8]
	mov r7, #28
	mul r8, r4, r7
	add r4, r5, r8
	ldr r5, [fp, #-4]
	mov r7, #4
	mul r8, r5, r7
	add r5, r4, r8
	str r6, [r5]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L403
.L403:
	ldr r4, [fp, #-4]
	cmp r4, #7
	movlt r4, #1
	movge r4, #0
	blt .L402
	b .L407
.L404:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L397
	b .F1
.LTORG
addr_arr11:
	.word arr1
addr_arr21:
	.word arr2
.F1:
.L405:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L406:
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L407:
	b .L404

	.global loop3
	.type loop3 , %function
loop3:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #60
.L414:
	str r0, [fp, #-60]
	str r1, [fp, #-56]
	str r2, [fp, #-52]
	str r3, [fp, #-48]
	ldr r3, [fp, #28]
	str r3, [fp, #-44]
	ldr r3, [fp, #32]
	str r3, [fp, #-40]
	ldr r3, [fp, #36]
	str r3, [fp, #-36]
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, =0
	str r4, [fp, #-32]
	b .L431
.L430:
	ldr r4, =0
	str r4, [fp, #-28]
	b .L437
.L431:
	ldr r4, [fp, #-32]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L430
	b .L435
.L432:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #60
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L435:
	b .L432
.L436:
	ldr r4, =0
	str r4, [fp, #-24]
	b .L443
.L437:
	ldr r4, [fp, #-28]
	cmp r4, #100
	movlt r4, #1
	movge r4, #0
	blt .L436
	b .L441
.L438:
	ldr r4, [fp, #-32]
	add r5, r4, #1
	str r5, [fp, #-32]
	ldr r4, [fp, #-32]
	ldr r5, [fp, #-60]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L522
	b .L526
.L441:
	b .L438
.L442:
	ldr r4, =0
	str r4, [fp, #-20]
	b .L449
.L443:
	ldr r4, [fp, #-24]
	ldr r5, =1000
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L442
	b .L447
.L444:
	ldr r4, [fp, #-28]
	add r5, r4, #1
	str r5, [fp, #-28]
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-56]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L516
	b .L520
.L447:
	b .L444
.L448:
	ldr r4, =0
	str r4, [fp, #-16]
	b .L455
.L449:
	ldr r4, [fp, #-20]
	ldr r5, =10000
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L448
	b .L453
.L450:
	ldr r4, [fp, #-24]
	add r5, r4, #1
	str r5, [fp, #-24]
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-52]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L510
	b .L514
.L453:
	b .L450
.L454:
	ldr r4, =0
	str r4, [fp, #-12]
	b .L461
.L455:
	ldr r4, [fp, #-16]
	ldr r5, =100000
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L454
	b .L459
.L456:
	ldr r4, [fp, #-20]
	add r5, r4, #1
	str r5, [fp, #-20]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-48]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L504
	b .L508
.L459:
	b .L456
.L460:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L467
.L461:
	ldr r4, [fp, #-12]
	ldr r5, =1000000
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L460
	b .L465
.L462:
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-44]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L498
	b .L502
.L465:
	b .L462
.L466:
	ldr r4, [fp, #-4]
	ldr r5, =817
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	ldr r4, [fp, #-32]
	ldr r5, addr_arr12
	ldr r7, =5760
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [fp, #-28]
	ldr r7, =2880
	mul r8, r4, r7
	add r4, r5, r8
	ldr r5, [fp, #-24]
	ldr r7, =960
	mul r8, r5, r7
	add r5, r4, r8
	ldr r4, [fp, #-20]
	mov r7, #240
	mul r8, r4, r7
	add r4, r5, r8
	ldr r5, [fp, #-16]
	mov r8, #48
	mul r7, r5, r8
	add r5, r4, r7
	ldr r4, [fp, #-12]
	mov r7, #8
	mul r8, r4, r7
	add r4, r5, r8
	ldr r5, [fp, #-8]
	mov r8, #4
	mul r7, r5, r8
	add r5, r4, r7
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, [fp, #-32]
	ldr r6, addr_arr22
	ldr r7, =10752
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-28]
	ldr r7, =5376
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-24]
	ldr r8, =1792
	mul r7, r6, r8
	add r6, r4, r7
	ldr r4, [fp, #-20]
	ldr r7, =896
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-16]
	mov r7, #224
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [fp, #-12]
	mov r7, #28
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-8]
	mov r7, #4
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	str r6, [fp, #-4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-36]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L486
	b .L490
	b .F2
.LTORG
addr_arr12:
	.word arr1
addr_arr22:
	.word arr2
.F2:
.L467:
	ldr r4, [fp, #-8]
	ldr r5, =10000000
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L466
	b .L471
.L468:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-40]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L492
	b .L496
.L471:
	b .L468
.L486:
	b .L468
.L487:
	b .L467
.L490:
	b .L487
.L491:
	b .L487
.L492:
	b .L462
.L493:
	b .L461
.L496:
	b .L493
.L497:
	b .L493
.L498:
	b .L456
.L499:
	b .L455
.L502:
	b .L499
.L503:
	b .L499
.L504:
	b .L450
.L505:
	b .L449
.L508:
	b .L505
.L509:
	b .L505
.L510:
	b .L444
.L511:
	b .L443
.L514:
	b .L511
.L515:
	b .L511
.L516:
	b .L438
.L517:
	b .L437
.L520:
	b .L517
.L521:
	b .L517
.L522:
	b .L432
.L523:
	b .L431
.L526:
	b .L523
.L527:
	b .L523

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	mov fp, sp
	sub sp, sp, #36
.L528:
	bl getint
	mov r4, r0
	str r4, [fp, #-36]
	bl getint
	mov r4, r0
	str r4, [fp, #-32]
	bl getint
	mov r4, r0
	str r4, [fp, #-28]
	bl getint
	mov r4, r0
	str r4, [fp, #-24]
	bl getint
	mov r4, r0
	str r4, [fp, #-20]
	bl getint
	mov r4, r0
	str r4, [fp, #-16]
	bl getint
	mov r4, r0
	str r4, [fp, #-12]
	bl getint
	mov r4, r0
	str r4, [fp, #-8]
	bl getint
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-36]
	ldr r5, [fp, #-32]
	mov r0, r4
	mov r1, r5
	bl loop1
	bl loop2
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-24]
	ldr r6, [fp, #-20]
	ldr r7, [fp, #-16]
	ldr r8, [fp, #-12]
	ldr r9, [fp, #-8]
	ldr r10, [fp, #-4]
	mov r0, r4
	mov r1, r5
	mov r2, r6
	mov r3, r7
	push {r10}
	push {r9}
	push {r8}
	bl loop3
	add sp, sp, #12
	mov r4, r0
	mov r0, r4
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	bx lr

addr_arr13:
	.word arr1
addr_arr23:
	.word arr2
