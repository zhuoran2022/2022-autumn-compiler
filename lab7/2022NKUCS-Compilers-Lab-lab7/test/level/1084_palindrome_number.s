	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global palindrome
	.type palindrome , %function
palindrome:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #28
.L45:
	str r0, [fp, #-28]
	ldr r4, =0
	str r4, [fp, #-8]
	b .L51
.L50:
	ldr r4, [fp, #-28]
	ldr r5, =10
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	ldr r4, [fp, #-8]
	mov r5, #-24
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	add r5, fp, r4
	str r6, [r5]
	ldr r4, [fp, #-28]
	ldr r5, =10
	sdiv r6, r4, r5
	str r6, [fp, #-28]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L51
.L51:
	ldr r4, [fp, #-8]
	cmp r4, #4
	movlt r4, #1
	movge r4, #0
	blt .L50
	b .L55
.L52:
	mov r4, #0
	mov r5, #-24
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	mov r5, #3
	mov r6, #-24
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	cmp r4, r5
	beq .L59
	b .L64
.L55:
	b .L52
.L56:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L58
.L57:
	ldr r4, =0
	str r4, [fp, #-4]
	b .L58
.L58:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #28
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L59:
	mov r4, #1
	mov r5, #-24
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	mov r5, #2
	mov r6, #-24
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	cmp r4, r5
	beq .L56
	b .L69
.L64:
	b .L57
.L69:
	b .L57

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L70:
	ldr r4, =1221
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	mov r0, r4
	bl palindrome
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #1
	beq .L73
	b .L78
.L73:
	ldr r4, [fp, #-8]
	mov r0, r4
	bl putint
	b .L75
.L74:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	b .L75
.L75:
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putch
	mov r0, #0
	add sp, sp, #8
	pop {r4, fp, lr}
	bx lr
.L78:
	b .L74

