	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.comm buf, 800, 4
	.text
	.global merge_sort
	.type merge_sort , %function
merge_sort:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #24
.L100:
	str r0, [fp, #-24]
	str r1, [fp, #-20]
	ldr r4, [fp, #-24]
	add r5, r4, #1
	ldr r4, [fp, #-20]
	cmp r5, r4
	movge r4, #1
	movlt r4, #0
	bge .L103
	b .L107
.L103:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .L104
.L104:
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-20]
	add r6, r4, r5
	ldr r4, =2
	sdiv r5, r6, r4
	str r5, [fp, #-16]
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-16]
	mov r0, r4
	mov r1, r5
	bl merge_sort
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-20]
	mov r0, r4
	mov r1, r5
	bl merge_sort
	ldr r4, [fp, #-24]
	str r4, [fp, #-12]
	ldr r4, [fp, #-16]
	str r4, [fp, #-8]
	ldr r4, [fp, #-24]
	str r4, [fp, #-4]
	b .L113
.L105:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L106:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L107:
	b .L104
.L112:
	mov r4, #0
	ldr r5, addr_buf0
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-12]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	mov r4, #0
	ldr r6, addr_buf0
	ldr r7, =400
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-8]
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [r4]
	cmp r5, r6
	movlt r4, #1
	movge r4, #0
	blt .L122
	b .L131
.L113:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L115
	b .L118
.L114:
	b .L139
.L115:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-20]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L112
	b .L121
.L116:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L117:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L118:
	b .L114
.L119:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L120:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L121:
	b .L114
.L122:
	mov r4, #0
	ldr r5, addr_buf0
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-12]
	mov r7, #4
	mul r6, r4, r7
	add r4, r5, r6
	ldr r5, [r4]
	mov r4, #1
	ldr r6, addr_buf0
	ldr r7, =400
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-4]
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	str r5, [r4]
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L124
.L123:
	mov r4, #0
	ldr r5, addr_buf0
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-8]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	mov r4, #1
	ldr r6, addr_buf0
	ldr r7, =400
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-4]
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	str r5, [r4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L124
.L124:
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L113
.L129:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L130:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L131:
	b .L123
.L138:
	mov r4, #0
	ldr r5, addr_buf0
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-12]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	mov r4, #1
	ldr r6, addr_buf0
	ldr r7, =400
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-4]
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	str r5, [r4]
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L139
	b .F0
.LTORG
addr_buf0:
	.word buf
.F0:
.L139:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L138
	b .L143
.L140:
	b .L148
.L141:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L142:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L143:
	b .L140
.L147:
	mov r4, #0
	ldr r5, addr_buf1
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-8]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	mov r4, #1
	ldr r6, addr_buf1
	ldr r8, =400
	mul r7, r4, r8
	add r4, r6, r7
	mov r6, r4
	ldr r4, [fp, #-4]
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	str r5, [r4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L148
.L148:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-20]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L147
	b .L152
.L149:
	b .L157
.L150:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L151:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L152:
	b .L149
.L156:
	mov r4, #1
	ldr r5, addr_buf1
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-24]
	mov r7, #4
	mul r6, r4, r7
	add r4, r5, r6
	ldr r5, [r4]
	mov r4, #0
	ldr r6, addr_buf1
	ldr r7, =400
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-24]
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	str r5, [r4]
	ldr r4, [fp, #-24]
	add r5, r4, #1
	str r5, [fp, #-24]
	b .L157
.L157:
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-20]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L156
	b .L161
.L158:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L159:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L160:
	add sp, sp, #24
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L161:
	b .L158

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L165:
	mov r4, #0
	ldr r5, addr_buf1
	ldr r6, =400
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	mov r4, #0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r0, r4
	bl getarray
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, #0
	mov r1, r4
	bl merge_sort
	ldr r4, [fp, #-4]
	mov r5, #0
	ldr r6, addr_buf1
	ldr r7, =400
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	mov r5, #0
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r0, r4
	mov r1, r5
	bl putarray
	mov r0, #0
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr

addr_buf1:
	.word buf
