	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global uniquePaths
	.type uniquePaths , %function
uniquePaths:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #52
.L84:
	str r0, [fp, #-52]
	str r1, [fp, #-48]
	ldr r4, [fp, #-52]
	cmp r4, #1
	beq .L87
	b .L92
.L87:
	mov r0, #1
	add sp, sp, #52
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .L88
.L88:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L100
.L89:
	ldr r4, [fp, #-48]
	cmp r4, #1
	beq .L87
	b .L95
.L92:
	b .L89
.L95:
	b .L88
.L99:
	ldr r4, [fp, #-8]
	ldr r5, =3
	mul r6, r4, r5
	ldr r4, [fp, #-48]
	add r5, r6, r4
	sub r4, r5, #1
	mov r5, #-44
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, =1
	str r4, [r5]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L100
.L100:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-52]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L99
	b .L104
.L101:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L106
.L104:
	b .L101
.L105:
	ldr r4, [fp, #-52]
	sub r5, r4, #1
	ldr r4, =3
	mul r6, r5, r4
	ldr r4, [fp, #-8]
	add r5, r6, r4
	mov r4, #-44
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	add r4, fp, r5
	ldr r5, =1
	str r5, [r4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L106
.L106:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-48]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L105
	b .L110
.L107:
	ldr r4, [fp, #-52]
	sub r5, r4, #2
	str r5, [fp, #-8]
	b .L112
.L110:
	b .L107
.L111:
	ldr r4, [fp, #-48]
	sub r5, r4, #2
	str r5, [fp, #-4]
	b .L118
.L112:
	ldr r4, [fp, #-8]
	ldr r5, =0
	sub r6, r5, #1
	cmp r4, r6
	movgt r4, #1
	movle r4, #0
	bgt .L111
	b .L116
.L113:
	mov r4, #0
	mov r5, #-44
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	mov r0, r4
	add sp, sp, #52
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L116:
	b .L113
.L117:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	ldr r4, =3
	mul r6, r5, r4
	ldr r4, [fp, #-4]
	add r5, r6, r4
	mov r4, #-44
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	add r4, fp, r5
	ldr r5, [r4]
	ldr r4, [fp, #-8]
	ldr r6, =3
	mul r7, r4, r6
	ldr r4, [fp, #-4]
	add r6, r7, r4
	add r4, r6, #1
	mov r6, #-44
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	add r6, fp, r4
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, [fp, #-8]
	ldr r5, =3
	mul r7, r4, r5
	ldr r4, [fp, #-4]
	add r5, r7, r4
	mov r4, #-44
	mov r7, #4
	mul r8, r5, r7
	add r5, r4, r8
	add r4, fp, r5
	str r6, [r4]
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-4]
	b .L118
.L118:
	ldr r4, [fp, #-4]
	ldr r5, =0
	sub r6, r5, #1
	cmp r4, r6
	movgt r4, #1
	movle r4, #0
	bgt .L117
	b .L122
.L119:
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	str r5, [fp, #-8]
	b .L112
.L122:
	b .L119

	.global main
	.type main , %function
main:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L126:
	ldr r4, =3
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-4]
	mov r0, r4
	mov r1, r5
	bl uniquePaths
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, fp, lr}
	bx lr

