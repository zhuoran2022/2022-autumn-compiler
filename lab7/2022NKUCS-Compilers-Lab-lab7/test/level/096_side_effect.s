	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word -1
	.global b
	.align 4
	.size b, 4
b:
	.word 1
	.text
	.global inc_a
	.type inc_a , %function
inc_a:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L64:
	ldr r4, addr_a0
	ldr r5, [r4]
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, addr_a0
	str r4, [r5]
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {r4, r5, fp, lr}
	bx lr

	.global main
	.type main , %function
main:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L66:
	ldr r4, =5
	str r4, [fp, #-4]
	b .L69
.L68:
	bl inc_a
	mov r4, r0
	cmp r4, #0
	bne .L77
	b .L79
.L69:
	ldr r4, [fp, #-4]
	cmp r4, #0
	movge r4, #1
	movlt r4, #0
	bge .L68
	b .L73
.L70:
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #32
	bl putch
	ldr r4, addr_b0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #10
	bl putch
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {r4, r5, r6, fp, lr}
	bx lr
.L73:
	b .L70
.L74:
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #32
	bl putch
	ldr r4, addr_b0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #10
	bl putch
	b .L75
.L75:
	bl inc_a
	mov r4, r0
	cmp r4, #14
	movlt r4, #1
	movge r4, #0
	blt .L87
	b .L93
.L76:
	bl inc_a
	mov r4, r0
	cmp r4, #0
	bne .L74
	b .L85
.L77:
	bl inc_a
	mov r4, r0
	cmp r4, #0
	bne .L76
	b .L82
.L79:
	b .L75
.L82:
	b .L75
.L85:
	b .L75
.L87:
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #10
	bl putch
	ldr r4, addr_b0
	ldr r5, [r4]
	ldr r4, =2
	mul r6, r5, r4
	ldr r4, addr_b0
	str r6, [r4]
	b .L89
.L88:
	bl inc_a
	mov r4, r0
	b .L89
.L89:
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-4]
	b .L69
.L90:
	bl inc_a
	mov r4, r0
	cmp r4, #0
	bne .L94
	b .L96
.L93:
	b .L90
.L94:
	bl inc_a
	mov r4, r0
	bl inc_a
	mov r5, r0
	sub r6, r4, r5
	add r4, r6, #1
	cmp r4, #0
	bne .L87
	b .L99
.L96:
	b .L88
.L99:
	b .L88

addr_a0:
	.word a
addr_b0:
	.word b
