	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global func
	.type func , %function
func:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L35:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #50
	movle r4, #1
	movgt r4, #0
	ble .L37
	b .L42
.L37:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	mov r0, #1
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
	b .L39
.L38:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	mov r0, #0
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
	b .L39
.L39:
	mov r0, #0
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
.L42:
	b .L38

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L43:
	mov r0, #0
	bl func
	mov r4, r0
	cmp r4, #1
	beq .L45
	b .L51
.L45:
	ldr r4, =0
	str r4, [fp, #-4]
	b .L47
.L46:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L47
.L47:
	mov r0, #50
	bl func
	mov r4, r0
	cmp r4, #1
	beq .L63
	b .L66
.L48:
	mov r0, #50
	bl func
	mov r4, r0
	cmp r4, #1
	beq .L52
	b .L55
.L51:
	b .L48
.L52:
	mov r0, #100
	bl func
	mov r4, r0
	cmp r4, #0
	beq .L45
	b .L58
.L55:
	b .L46
.L58:
	b .L46
.L59:
	ldr r4, =0
	str r4, [fp, #-4]
	b .L61
.L60:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L61
.L61:
	mov r0, #0
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
.L62:
	mov r0, #1
	bl func
	mov r4, r0
	cmp r4, #1
	beq .L59
	b .L72
.L63:
	mov r0, #40
	bl func
	mov r4, r0
	cmp r4, #1
	beq .L59
	b .L69
.L66:
	b .L62
.L69:
	b .L62
.L72:
	b .L60

