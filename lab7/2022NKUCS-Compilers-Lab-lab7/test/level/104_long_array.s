	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.section .rodata
	.global N
	.align 4
	.size N, 4
N:
	.word 10000
	.text
	.global long_array
	.type long_array , %function
long_array:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	ldr r4, =120020
	sub sp, sp, r4
.L140:
	ldr r4, =-120020
	str r0, [fp, r4]
	ldr r4, =0
	str r4, [fp, #-16]
	b .L147
.L146:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-16]
	mul r6, r4, r5
	ldr r4, =10
	sdiv r5, r6, r4
	mul r4, r5, r4
	sub r5, r6, r4
	ldr r4, [fp, #-16]
	ldr r6, =-120016
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	add r6, fp, r4
	str r5, [r6]
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L147
.L147:
	ldr r4, [fp, #-16]
	ldr r5, addr_N0
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L146
	b .L151
.L148:
	ldr r4, =0
	str r4, [fp, #-16]
	b .L153
.L151:
	b .L148
.L152:
	ldr r4, [fp, #-16]
	ldr r5, =-120016
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	ldr r5, [fp, #-16]
	ldr r6, =-120016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =10
	sdiv r5, r6, r4
	mul r4, r5, r4
	sub r5, r6, r4
	ldr r4, [fp, #-16]
	ldr r6, =-80016
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	add r6, fp, r4
	str r5, [r6]
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L153
.L153:
	ldr r4, [fp, #-16]
	ldr r5, addr_N0
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L152
	b .L157
.L154:
	ldr r4, =0
	str r4, [fp, #-16]
	b .L161
.L157:
	b .L154
.L160:
	ldr r4, [fp, #-16]
	ldr r5, =-80016
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	ldr r5, [fp, #-16]
	ldr r6, =-80016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =100
	sdiv r5, r6, r4
	mul r4, r5, r4
	sub r5, r6, r4
	ldr r4, [fp, #-16]
	ldr r6, =-120016
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	add r6, fp, r4
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, [fp, #-16]
	ldr r5, =-40016
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	add r5, fp, r4
	str r6, [r5]
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L161
.L161:
	ldr r4, [fp, #-16]
	ldr r5, addr_N0
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L160
	b .L165
.L162:
	ldr r4, =0
	str r4, [fp, #-12]
	ldr r4, =0
	str r4, [fp, #-16]
	b .L171
.L165:
	b .L162
.L170:
	ldr r4, [fp, #-16]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L176
	b .L181
.L171:
	ldr r4, [fp, #-16]
	ldr r5, addr_N0
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L170
	b .L175
.L172:
	ldr r4, [fp, #-12]
	mov r0, r4
	ldr r1, =120020
	add sp, sp, r1
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L175:
	b .L172
.L176:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	ldr r6, =-40016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	add r6, r4, r5
	ldr r4, =1333
	sdiv r5, r6, r4
	mul r5, r5, r4
	sub r4, r6, r5
	str r4, [fp, #-12]
	ldr r4, [fp, #-12]
	mov r0, r4
	bl putint
	b .L178
	b .F0
.LTORG
addr_N0:
	.word N
.F0:
.L177:
	ldr r4, [fp, #-16]
	cmp r4, #20
	movlt r4, #1
	movge r4, #0
	blt .L183
	b .L188
.L178:
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L171
.L181:
	b .L177
.L183:
	ldr r4, addr_N1
	ldr r5, [r4]
	ldr r4, =2
	sdiv r6, r5, r4
	str r6, [fp, #-8]
	b .L191
.L184:
	ldr r4, [fp, #-16]
	cmp r4, #30
	movlt r4, #1
	movge r4, #0
	blt .L198
	b .L203
.L185:
	b .L178
.L188:
	b .L184
.L190:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	ldr r6, =-40016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	add r6, r4, r5
	ldr r4, [fp, #-8]
	ldr r5, =-120016
	mov r8, #4
	mul r7, r4, r8
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	sub r5, r6, r4
	str r5, [fp, #-12]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L191
.L191:
	ldr r4, [fp, #-8]
	ldr r5, addr_N1
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L190
	b .L195
.L192:
	ldr r4, [fp, #-12]
	mov r0, r4
	bl putint
	b .L185
.L195:
	b .L192
.L198:
	ldr r4, addr_N1
	ldr r5, [r4]
	ldr r4, =2
	sdiv r6, r5, r4
	str r6, [fp, #-4]
	b .L206
.L199:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	ldr r6, =-40016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	ldr r7, =-120020
	ldr r6, [fp, r7]
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =99988
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	str r4, [fp, #-12]
	b .L200
.L200:
	b .L185
.L203:
	b .L199
.L205:
	ldr r4, [fp, #-4]
	ldr r5, =2233
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L211
	b .L216
.L206:
	ldr r4, [fp, #-4]
	ldr r5, addr_N1
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L205
	b .L210
.L207:
	ldr r4, [fp, #-12]
	mov r0, r4
	bl putint
	b .L200
.L210:
	b .L207
.L211:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	ldr r6, =-80016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	add r6, r4, r5
	ldr r4, [fp, #-4]
	ldr r5, =-120016
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	add r5, fp, r4
	ldr r4, [r5]
	sub r5, r6, r4
	str r5, [fp, #-12]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L213
.L212:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	ldr r6, =-120016
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	add r6, r4, r5
	ldr r4, [fp, #-4]
	ldr r5, =-40016
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	add r5, fp, r4
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =13333
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	str r6, [fp, #-12]
	ldr r4, [fp, #-4]
	add r5, r4, #2
	str r5, [fp, #-4]
	b .L213
.L213:
	b .L206
.L216:
	b .L212

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L222:
	mov r0, #9
	bl long_array
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {r4, fp, lr}
	bx lr

addr_N1:
	.word N
