	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.section .rodata
	.global maxn
	.align 4
	.size maxn, 4
maxn:
	.word 18
	.global mod
	.align 4
	.size mod, 4
mod:
	.word 1000000007
	.comm dp, 52907904, 4
	.comm list, 800, 4
	.comm cns, 80, 4
	.text
	.global equal
	.type equal , %function
equal:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L259:
	str r0, [fp, #-8]
	str r1, [fp, #-4]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	cmp r4, r5
	beq .L262
	b .L266
.L262:
	mov r0, #1
	add sp, sp, #8
	pop {r4, r5, fp, lr}
	bx lr
	b .L263
.L263:
	mov r0, #0
	add sp, sp, #8
	pop {r4, r5, fp, lr}
	bx lr
.L266:
	b .L263

	.global dfs
	.type dfs , %function
dfs:
	push {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	mov fp, sp
	sub sp, sp, #32
.L267:
	str r0, [fp, #-28]
	str r1, [fp, #-24]
	str r2, [fp, #-20]
	str r3, [fp, #-16]
	ldr r3, [fp, #36]
	str r3, [fp, #-12]
	ldr r3, [fp, #40]
	str r3, [fp, #-8]
	ldr r4, [fp, #-28]
	ldr r5, addr_dp0
	ldr r6, =2939328
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-24]
	ldr r6, =163296
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [fp, #-20]
	ldr r7, =9072
	mul r6, r5, r7
	add r5, r4, r6
	ldr r4, [fp, #-16]
	ldr r6, =504
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [fp, #-12]
	mov r6, #28
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-8]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	ldr r4, =0
	sub r6, r4, #1
	cmp r5, r6
	bne .L274
	b .L284
.L274:
	ldr r4, [fp, #-28]
	ldr r5, addr_dp0
	ldr r6, =2939328
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-24]
	ldr r7, =163296
	mul r6, r4, r7
	add r4, r5, r6
	ldr r5, [fp, #-20]
	ldr r6, =9072
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-16]
	ldr r6, =504
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [fp, #-12]
	mov r6, #28
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-8]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #32
	pop {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	bx lr
	b .L275
.L275:
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-24]
	add r6, r4, r5
	ldr r4, [fp, #-20]
	add r5, r6, r4
	ldr r4, [fp, #-16]
	add r6, r5, r4
	ldr r4, [fp, #-12]
	add r5, r6, r4
	cmp r5, #0
	beq .L291
	b .L295
.L284:
	b .L275
.L291:
	mov r0, #1
	add sp, sp, #32
	pop {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	bx lr
	b .L292
.L292:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, [fp, #-28]
	cmp r4, #0
	bne .L297
	b .L300
.L295:
	b .L292
.L297:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-28]
	ldr r6, [fp, #-8]
	mov r0, r6
	mov r1, #2
	bl equal
	mov r6, r0
	sub r7, r5, r6
	ldr r5, [fp, #-28]
	sub r6, r5, #1
	ldr r5, [fp, #-24]
	ldr r8, [fp, #-20]
	ldr r9, [fp, #-16]
	ldr r10, [fp, #-12]
	mov r0, r6
	mov r1, r5
	mov r2, r8
	mov r3, r9
	mov r5, #1
	push {r5}
	push {r10}
	bl dfs
	add sp, sp, #8
	mov r5, r0
	mul r6, r7, r5
	add r5, r4, r6
	ldr r4, addr_mod0
	ldr r6, [r4]
	sdiv r4, r5, r6
	mul r4, r4, r6
	sub r6, r5, r4
	str r6, [fp, #-4]
	b .L298
.L298:
	ldr r4, [fp, #-24]
	cmp r4, #0
	bne .L302
	b .L305
.L300:
	b .L298
.L302:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-24]
	ldr r6, [fp, #-8]
	mov r0, r6
	mov r1, #3
	bl equal
	mov r6, r0
	sub r7, r5, r6
	ldr r5, [fp, #-28]
	add r6, r5, #1
	ldr r5, [fp, #-24]
	sub r8, r5, #1
	ldr r5, [fp, #-20]
	ldr r9, [fp, #-16]
	ldr r10, [fp, #-12]
	mov r0, r6
	mov r1, r8
	mov r2, r5
	mov r3, r9
	mov r5, #2
	push {r5}
	push {r10}
	bl dfs
	add sp, sp, #8
	mov r5, r0
	mul r6, r7, r5
	add r5, r4, r6
	ldr r4, addr_mod0
	ldr r6, [r4]
	sdiv r4, r5, r6
	mul r4, r4, r6
	sub r6, r5, r4
	str r6, [fp, #-4]
	b .L303
	b .F0
.LTORG
addr_maxn0:
	.word maxn
addr_mod0:
	.word mod
addr_dp0:
	.word dp
addr_list0:
	.word list
addr_cns0:
	.word cns
.F0:
.L303:
	ldr r4, [fp, #-20]
	cmp r4, #0
	bne .L307
	b .L310
.L305:
	b .L303
.L307:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-20]
	ldr r6, [fp, #-8]
	mov r0, r6
	mov r1, #4
	bl equal
	mov r6, r0
	sub r7, r5, r6
	ldr r5, [fp, #-28]
	ldr r6, [fp, #-24]
	add r8, r6, #1
	ldr r6, [fp, #-20]
	sub r9, r6, #1
	ldr r6, [fp, #-16]
	ldr r10, [fp, #-12]
	mov r0, r5
	mov r1, r8
	mov r2, r9
	mov r3, r6
	mov r5, #3
	push {r5}
	push {r10}
	bl dfs
	add sp, sp, #8
	mov r5, r0
	mul r6, r7, r5
	add r5, r4, r6
	ldr r4, addr_mod1
	ldr r6, [r4]
	sdiv r4, r5, r6
	mul r4, r4, r6
	sub r6, r5, r4
	str r6, [fp, #-4]
	b .L308
.L308:
	ldr r4, [fp, #-16]
	cmp r4, #0
	bne .L312
	b .L315
.L310:
	b .L308
.L312:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-16]
	ldr r6, [fp, #-8]
	mov r0, r6
	mov r1, #5
	bl equal
	mov r6, r0
	sub r7, r5, r6
	ldr r5, [fp, #-28]
	ldr r6, [fp, #-24]
	ldr r8, [fp, #-20]
	add r9, r8, #1
	ldr r8, [fp, #-16]
	sub r10, r8, #1
	ldr r8, [fp, #-12]
	mov r0, r5
	mov r1, r6
	mov r2, r9
	mov r3, r10
	mov r5, #4
	push {r5}
	push {r8}
	bl dfs
	add sp, sp, #8
	mov r5, r0
	mul r6, r7, r5
	add r5, r4, r6
	ldr r4, addr_mod1
	ldr r6, [r4]
	sdiv r4, r5, r6
	mul r4, r4, r6
	sub r6, r5, r4
	str r6, [fp, #-4]
	b .L313
.L313:
	ldr r4, [fp, #-12]
	cmp r4, #0
	bne .L317
	b .L320
.L315:
	b .L313
.L317:
	ldr r4, [fp, #-4]
	str r4, [fp, #-32]
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-28]
	ldr r6, [fp, #-24]
	ldr r7, [fp, #-20]
	ldr r8, [fp, #-16]
	add r9, r8, #1
	ldr r8, [fp, #-12]
	sub r10, r8, #1
	mov r0, r5
	mov r1, r6
	mov r2, r7
	mov r3, r9
	mov r5, #5
	push {r5}
	push {r10}
	bl dfs
	add sp, sp, #8
	mov r5, r0
	mul r6, r4, r5
	ldr r4, [fp, #-32]
	add r5, r4, r6
	ldr r4, addr_mod1
	ldr r6, [r4]
	sdiv r4, r5, r6
	mul r4, r4, r6
	sub r6, r5, r4
	str r6, [fp, #-4]
	b .L318
.L318:
	ldr r4, [fp, #-4]
	ldr r5, addr_mod1
	ldr r6, [r5]
	sdiv r5, r4, r6
	mul r6, r5, r6
	sub r5, r4, r6
	ldr r4, [fp, #-28]
	ldr r6, addr_dp1
	ldr r7, =2939328
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-24]
	ldr r7, =163296
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-20]
	ldr r7, =9072
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [fp, #-16]
	ldr r7, =504
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-12]
	mov r7, #28
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [fp, #-8]
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	str r5, [r4]
	ldr r4, [fp, #-28]
	ldr r5, addr_dp1
	ldr r6, =2939328
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [fp, #-24]
	ldr r6, =163296
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [fp, #-20]
	ldr r6, =9072
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-16]
	ldr r6, =504
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [fp, #-12]
	mov r6, #28
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-8]
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #32
	pop {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	bx lr
	b .F1
.LTORG
addr_maxn1:
	.word maxn
addr_mod1:
	.word mod
addr_dp1:
	.word dp
addr_list1:
	.word list
addr_cns1:
	.word cns
.F1:
.L320:
	b .L318

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	mov fp, sp
	sub sp, sp, #36
.L333:
	bl getint
	mov r4, r0
	str r4, [fp, #-32]
	ldr r4, =0
	str r4, [fp, #-28]
	b .L337
.L336:
	ldr r4, =0
	str r4, [fp, #-24]
	b .L344
.L337:
	ldr r4, [fp, #-28]
	ldr r5, addr_maxn2
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L336
	b .L341
.L338:
	ldr r4, =0
	str r4, [fp, #-28]
	b .L383
.L341:
	b .L338
.L343:
	ldr r4, =0
	str r4, [fp, #-20]
	b .L351
.L344:
	ldr r4, [fp, #-24]
	ldr r5, addr_maxn2
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L343
	b .L348
.L345:
	ldr r4, [fp, #-28]
	add r5, r4, #1
	str r5, [fp, #-28]
	b .L337
.L348:
	b .L345
.L350:
	ldr r4, =0
	str r4, [fp, #-16]
	b .L358
.L351:
	ldr r4, [fp, #-20]
	ldr r5, addr_maxn2
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L350
	b .L355
.L352:
	ldr r4, [fp, #-24]
	add r5, r4, #1
	str r5, [fp, #-24]
	b .L344
.L355:
	b .L352
.L357:
	ldr r4, =0
	str r4, [fp, #-12]
	b .L365
.L358:
	ldr r4, [fp, #-16]
	ldr r5, addr_maxn2
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L357
	b .L362
.L359:
	ldr r4, [fp, #-20]
	add r5, r4, #1
	str r5, [fp, #-20]
	b .L351
.L362:
	b .L359
.L364:
	ldr r4, =0
	str r4, [fp, #-8]
	b .L372
.L365:
	ldr r4, [fp, #-12]
	ldr r5, addr_maxn2
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L364
	b .L369
.L366:
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L358
.L369:
	b .L366
.L371:
	ldr r4, =0
	sub r5, r4, #1
	ldr r4, [fp, #-28]
	ldr r6, addr_dp2
	ldr r7, =2939328
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [fp, #-24]
	ldr r7, =163296
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-20]
	ldr r7, =9072
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [fp, #-16]
	ldr r7, =504
	mul r8, r4, r7
	add r4, r6, r8
	ldr r6, [fp, #-12]
	mov r7, #28
	mul r8, r6, r7
	add r6, r4, r8
	ldr r4, [fp, #-8]
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	str r5, [r4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L372
.L372:
	ldr r4, [fp, #-8]
	cmp r4, #7
	movlt r4, #1
	movge r4, #0
	blt .L371
	b .L376
.L373:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L365
.L376:
	b .L373
.L382:
	bl getint
	mov r4, r0
	ldr r5, [fp, #-28]
	ldr r6, addr_list2
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	str r4, [r6]
	ldr r4, [fp, #-28]
	ldr r5, addr_list2
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	ldr r5, addr_cns2
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	add r5, r4, #1
	ldr r4, [fp, #-28]
	ldr r6, addr_list2
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, addr_cns2
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	mov r6, r4
	str r5, [r6]
	ldr r4, [fp, #-28]
	add r5, r4, #1
	str r5, [fp, #-28]
	b .L383
	b .F2
.LTORG
addr_maxn2:
	.word maxn
addr_mod2:
	.word mod
addr_dp2:
	.word dp
addr_list2:
	.word list
addr_cns2:
	.word cns
.F2:
.L383:
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-32]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L382
	b .L387
.L384:
	mov r4, #1
	ldr r5, addr_cns3
	mov r7, #4
	mul r6, r4, r7
	add r4, r5, r6
	mov r5, r4
	ldr r4, [r5]
	mov r5, #2
	ldr r6, addr_cns3
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mov r6, #3
	ldr r7, addr_cns3
	mov r9, #4
	mul r8, r6, r9
	add r6, r7, r8
	mov r7, r6
	ldr r6, [r7]
	mov r7, #4
	ldr r8, addr_cns3
	mov r9, #4
	mul r10, r7, r9
	add r7, r8, r10
	mov r8, r7
	ldr r7, [r8]
	str r7, [fp, #-36]
	mov r7, #5
	ldr r8, addr_cns3
	mov r9, #4
	mul r10, r7, r9
	add r7, r8, r10
	mov r8, r7
	ldr r7, [r8]
	mov r0, r4
	mov r1, r5
	mov r2, r6
	ldr r4, [fp, #-36]
	mov r3, r4
	mov r4, #0
	push {r4}
	push {r7}
	bl dfs
	add sp, sp, #8
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	bx lr
.L387:
	b .L384

addr_maxn3:
	.word maxn
addr_mod3:
	.word mod
addr_dp3:
	.word dp
addr_list3:
	.word list
addr_cns3:
	.word cns
