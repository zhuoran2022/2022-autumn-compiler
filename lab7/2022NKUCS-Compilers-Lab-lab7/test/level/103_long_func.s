	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.section .rodata
	.global SHIFT_TABLE
	.align 4
	.size SHIFT_TABLE, 64
SHIFT_TABLE:
	.word 1
	.word 2
	.word 4
	.word 8
	.word 16
	.word 32
	.word 64
	.word 128
	.word 256
	.word 512
	.word 1024
	.word 2048
	.word 4096
	.word 8192
	.word 16384
	.word 32768
	.text
	.global long_func
	.type long_func , %function
long_func:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	ldr r4, =356
	sub sp, sp, r4
.L2863:
	ldr r4, =2
	ldr r5, =-336
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-332
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-328
	str r4, [fp, r5]
	b .L2873
.L2872:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-332
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L2879
.L2873:
	ldr r5, =-332
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L2872
	b .L2877
.L2874:
	ldr r5, =-328
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	mov r0, r4
	bl putint
	mov r0, #10
	bl putch
	ldr r4, =2
	str r4, [fp, #-252]
	ldr r4, =1
	str r4, [fp, #-248]
	ldr r4, =1
	str r4, [fp, #-244]
	b .L3332
.L2877:
	b .L2874
.L2878:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2886
	b .L2888
.L2879:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L2878
	b .L2883
.L2880:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L2894
	b .L2897
.L2883:
	b .L2880
.L2884:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE0
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L2885
.L2885:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L2879
.L2886:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L2884
	b .L2891
.L2888:
	b .L2885
.L2891:
	b .L2885
.L2894:
	ldr r5, =-328
	ldr r4, [fp, r5]
	ldr r5, =-324
	str r4, [fp, r5]
	ldr r4, =-336
	ldr r5, [fp, r4]
	ldr r4, =-320
	str r5, [fp, r4]
	ldr r4, =0
	ldr r5, =-316
	str r4, [fp, r5]
	b .L2903
.L2895:
	ldr r5, =-336
	ldr r4, [fp, r5]
	ldr r5, =-288
	str r4, [fp, r5]
	ldr r5, =-336
	ldr r4, [fp, r5]
	ldr r5, =-284
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-280
	str r4, [fp, r5]
	b .L3104
.L2897:
	b .L2895
.L2902:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-320
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L2909
.L2903:
	ldr r5, =-320
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L2902
	b .L2906
.L2904:
	ldr r5, =-316
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-328
	str r4, [fp, r5]
	b .L2895
	b .F0
.LTORG
addr_SHIFT_TABLE0:
	.word SHIFT_TABLE
.F0:
.L2906:
	b .L2904
.L2908:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2916
	b .L2918
.L2909:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L2908
	b .L2913
.L2910:
	ldr r4, =-356
	ldr r5, [fp, r4]
	cmp r5, #0
	bne .L2924
	b .L2927
.L2913:
	b .L2910
.L2914:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE1
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L2915
.L2915:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L2909
.L2916:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L2914
	b .L2921
.L2918:
	b .L2915
.L2921:
	b .L2915
.L2924:
	ldr r5, =-316
	ldr r4, [fp, r5]
	ldr r5, =-312
	str r4, [fp, r5]
	ldr r4, =-324
	ldr r5, [fp, r4]
	ldr r4, =-308
	str r5, [fp, r4]
	b .L2933
.L2925:
	ldr r5, =-324
	ldr r4, [fp, r5]
	ldr r5, =-300
	str r4, [fp, r5]
	ldr r5, =-324
	ldr r4, [fp, r5]
	ldr r5, =-296
	str r4, [fp, r5]
	b .L3005
.L2927:
	b .L2925
.L2932:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-312
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-308
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L2939
.L2933:
	ldr r5, =-308
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L2932
	b .L2936
.L2934:
	ldr r5, =-312
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r4, =-316
	str r5, [fp, r4]
	b .L2925
.L2936:
	b .L2934
.L2938:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2944
	b .L2948
.L2939:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L2938
	b .L2943
.L2940:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r4, =-304
	str r5, [fp, r4]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, =-312
	ldr r5, [fp, r4]
	ldr r4, =-348
	str r5, [fp, r4]
	ldr r5, =-308
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L2963
.L2943:
	b .L2940
.L2944:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L2950
	b .L2954
	b .F1
.LTORG
addr_SHIFT_TABLE1:
	.word SHIFT_TABLE
.F1:
.L2945:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2956
	b .L2959
.L2946:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L2939
.L2948:
	b .L2945
.L2950:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE2
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L2951
.L2951:
	b .L2946
.L2954:
	b .L2951
.L2956:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE2
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L2957
.L2957:
	b .L2946
.L2959:
	b .L2957
.L2962:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2970
	b .L2972
.L2963:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L2962
	b .L2967
.L2964:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-308
	str r4, [fp, r5]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L2978
	b .L2983
.L2967:
	b .L2964
.L2968:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE2
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L2969
.L2969:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L2963
.L2970:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2968
	b .L2975
.L2972:
	b .L2969
.L2975:
	b .L2969
.L2978:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L2980
.L2979:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-308
	ldr r4, [fp, r5]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE2
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L2986
	b .F2
.LTORG
addr_SHIFT_TABLE2:
	.word SHIFT_TABLE
.F2:
.L2980:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-308
	str r4, [fp, r5]
	ldr r5, =-304
	ldr r4, [fp, r5]
	ldr r5, =-312
	str r4, [fp, r5]
	b .L2933
.L2983:
	b .L2979
.L2985:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2993
	b .L2995
.L2986:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L2985
	b .L2990
.L2987:
	b .L2980
.L2990:
	b .L2987
.L2991:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE3
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L2992
.L2992:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L2986
.L2993:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L2991
	b .L2998
.L2995:
	b .L2992
.L2998:
	b .L2992
.L3004:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-300
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-296
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3011
.L3005:
	ldr r5, =-296
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3004
	b .L3008
.L3006:
	ldr r5, =-300
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-324
	str r4, [fp, r5]
	ldr r5, =-320
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L3073
	b .L3078
.L3008:
	b .L3006
.L3010:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3016
	b .L3020
.L3011:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3010
	b .L3015
.L3012:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-292
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-300
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-296
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3035
.L3015:
	b .L3012
.L3016:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	beq .L3022
	b .L3026
	b .F3
.LTORG
addr_SHIFT_TABLE3:
	.word SHIFT_TABLE
.F3:
.L3017:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3028
	b .L3031
.L3018:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3011
.L3020:
	b .L3017
.L3022:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE4
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3023
.L3023:
	b .L3018
.L3026:
	b .L3023
.L3028:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE4
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3029
.L3029:
	b .L3018
.L3031:
	b .L3029
.L3034:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3042
	b .L3044
.L3035:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3034
	b .L3039
.L3036:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-296
	str r4, [fp, r5]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3050
	b .L3055
.L3039:
	b .L3036
.L3040:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE4
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3041
.L3041:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3035
.L3042:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3040
	b .L3047
.L3044:
	b .L3041
.L3047:
	b .L3041
.L3050:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3052
.L3051:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-296
	ldr r4, [fp, r5]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE4
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3058
	b .F4
.LTORG
addr_SHIFT_TABLE4:
	.word SHIFT_TABLE
.F4:
.L3052:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-296
	str r4, [fp, r5]
	ldr r5, =-292
	ldr r4, [fp, r5]
	ldr r5, =-300
	str r4, [fp, r5]
	b .L3005
.L3055:
	b .L3051
.L3057:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3065
	b .L3067
.L3058:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3057
	b .L3062
.L3059:
	b .L3052
.L3062:
	b .L3059
.L3063:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE5
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3064
.L3064:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3058
.L3065:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L3063
	b .L3070
.L3067:
	b .L3064
.L3070:
	b .L3064
.L3073:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L3079
	b .L3084
.L3074:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3085
	b .L3090
.L3075:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r4, =-320
	str r5, [fp, r4]
	b .L2903
.L3078:
	b .L3074
.L3079:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3081
.L3080:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3081
.L3081:
	b .L3075
.L3084:
	b .L3080
.L3085:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =32767
	cmp r5, r4
	movgt r4, #1
	movle r4, #0
	bgt .L3091
	b .L3096
.L3086:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3087
.L3087:
	b .L3075
.L3090:
	b .L3086
.L3091:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-344
	ldr r6, [fp, r4]
	ldr r4, addr_SHIFT_TABLE5
	mov r8, #4
	mul r7, r6, r8
	add r6, r4, r7
	mov r4, r6
	ldr r6, [r4]
	sdiv r4, r5, r6
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =65536
	add r6, r5, r4
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE5
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3093
.L3092:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r6, =-344
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE5
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	sdiv r6, r5, r4
	ldr r4, =-356
	str r6, [fp, r4]
	b .L3093
	b .F5
.LTORG
addr_SHIFT_TABLE5:
	.word SHIFT_TABLE
.F5:
.L3093:
	b .L3087
.L3096:
	b .L3092
.L3103:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-284
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3110
.L3104:
	ldr r5, =-284
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3103
	b .L3107
.L3105:
	ldr r5, =-280
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-336
	str r4, [fp, r5]
	ldr r5, =-332
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L3301
	b .L3306
.L3107:
	b .L3105
.L3109:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3117
	b .L3119
.L3110:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3109
	b .L3114
.L3111:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3125
	b .L3128
.L3114:
	b .L3111
.L3115:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE6
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3116
.L3116:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3110
.L3117:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3115
	b .L3122
.L3119:
	b .L3116
.L3122:
	b .L3116
.L3125:
	ldr r5, =-280
	ldr r4, [fp, r5]
	ldr r5, =-276
	str r4, [fp, r5]
	ldr r5, =-288
	ldr r4, [fp, r5]
	ldr r5, =-272
	str r4, [fp, r5]
	b .L3134
.L3126:
	ldr r5, =-288
	ldr r4, [fp, r5]
	ldr r5, =-264
	str r4, [fp, r5]
	ldr r5, =-288
	ldr r4, [fp, r5]
	ldr r5, =-260
	str r4, [fp, r5]
	b .L3206
.L3128:
	b .L3126
.L3133:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-276
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-272
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3140
.L3134:
	ldr r5, =-272
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3133
	b .L3137
.L3135:
	ldr r5, =-276
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-280
	str r4, [fp, r5]
	b .L3126
.L3137:
	b .L3135
.L3139:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3145
	b .L3149
	b .F6
.LTORG
addr_SHIFT_TABLE6:
	.word SHIFT_TABLE
.F6:
.L3140:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3139
	b .L3144
.L3141:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-268
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-276
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-272
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3164
.L3144:
	b .L3141
.L3145:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3151
	b .L3155
.L3146:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3157
	b .L3160
.L3147:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3140
.L3149:
	b .L3146
.L3151:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE7
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3152
.L3152:
	b .L3147
.L3155:
	b .L3152
.L3157:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE7
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3158
.L3158:
	b .L3147
.L3160:
	b .L3158
.L3163:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3171
	b .L3173
.L3164:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3163
	b .L3168
.L3165:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-272
	str r4, [fp, r5]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3179
	b .L3184
.L3168:
	b .L3165
.L3169:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE7
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3170
.L3170:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3164
	b .F7
.LTORG
addr_SHIFT_TABLE7:
	.word SHIFT_TABLE
.F7:
.L3171:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3169
	b .L3176
.L3173:
	b .L3170
.L3176:
	b .L3170
.L3179:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3181
.L3180:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, =-272
	ldr r5, [fp, r4]
	mov r4, #1
	ldr r6, addr_SHIFT_TABLE8
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	mul r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3187
.L3181:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r4, =-272
	str r5, [fp, r4]
	ldr r4, =-268
	ldr r5, [fp, r4]
	ldr r4, =-276
	str r5, [fp, r4]
	b .L3134
.L3184:
	b .L3180
.L3186:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L3194
	b .L3196
.L3187:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3186
	b .L3191
.L3188:
	b .L3181
.L3191:
	b .L3188
.L3192:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE8
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3193
.L3193:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3187
.L3194:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L3192
	b .L3199
.L3196:
	b .L3193
.L3199:
	b .L3193
.L3205:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-264
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-260
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3212
.L3206:
	ldr r4, =-260
	ldr r5, [fp, r4]
	cmp r5, #0
	bne .L3205
	b .L3209
.L3207:
	ldr r5, =-264
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-288
	str r4, [fp, r5]
	ldr r4, =-284
	ldr r5, [fp, r4]
	ldr r4, =-348
	str r5, [fp, r4]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L3274
	b .L3279
.L3209:
	b .L3207
.L3211:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L3217
	b .L3221
	b .F8
.LTORG
addr_SHIFT_TABLE8:
	.word SHIFT_TABLE
.F8:
.L3212:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3211
	b .L3216
.L3213:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-256
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-264
	ldr r4, [fp, r5]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r5, =-260
	ldr r4, [fp, r5]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3236
.L3216:
	b .L3213
.L3217:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3223
	b .L3227
.L3218:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3229
	b .L3232
.L3219:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3212
.L3221:
	b .L3218
.L3223:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE9
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3224
.L3224:
	b .L3219
.L3227:
	b .L3224
.L3229:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE9
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3230
.L3230:
	b .L3219
.L3232:
	b .L3230
.L3235:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3243
	b .L3245
.L3236:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3235
	b .L3240
.L3237:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-260
	str r4, [fp, r5]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3251
	b .L3256
.L3240:
	b .L3237
.L3241:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE9
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3242
.L3242:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3236
	b .F9
.LTORG
addr_SHIFT_TABLE9:
	.word SHIFT_TABLE
.F9:
.L3243:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3241
	b .L3248
.L3245:
	b .L3242
.L3248:
	b .L3242
.L3251:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3253
.L3252:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r5, =-260
	ldr r4, [fp, r5]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE10
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3259
.L3253:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-260
	str r4, [fp, r5]
	ldr r5, =-256
	ldr r4, [fp, r5]
	ldr r5, =-264
	str r4, [fp, r5]
	b .L3206
.L3256:
	b .L3252
.L3258:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3266
	b .L3268
.L3259:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3258
	b .L3263
.L3260:
	b .L3253
.L3263:
	b .L3260
.L3264:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE10
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3265
.L3265:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3259
.L3266:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3264
	b .L3271
.L3268:
	b .L3265
.L3271:
	b .L3265
.L3274:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L3280
	b .L3285
.L3275:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3286
	b .L3291
.L3276:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-284
	str r4, [fp, r5]
	b .L3104
.L3279:
	b .L3275
.L3280:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3282
.L3281:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3282
.L3282:
	b .L3276
.L3285:
	b .L3281
.L3286:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L3292
	b .L3297
.L3287:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3288
.L3288:
	b .L3276
.L3291:
	b .L3287
.L3292:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE10
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =65536
	add r6, r5, r4
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =15
	sub r7, r4, r5
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE10
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3294
	b .F10
.LTORG
addr_SHIFT_TABLE10:
	.word SHIFT_TABLE
.F10:
.L3293:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r6, =-344
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE11
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	mov r6, r4
	ldr r4, [r6]
	sdiv r6, r5, r4
	ldr r4, =-356
	str r6, [fp, r4]
	b .L3294
.L3294:
	b .L3288
.L3297:
	b .L3293
.L3301:
	ldr r4, =-348
	ldr r5, [fp, r4]
	cmp r5, #0
	movlt r4, #1
	movge r4, #0
	blt .L3307
	b .L3312
.L3302:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3313
	b .L3318
.L3303:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-332
	str r4, [fp, r5]
	b .L2873
.L3306:
	b .L3302
.L3307:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3309
.L3308:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3309
.L3309:
	b .L3303
.L3312:
	b .L3308
.L3313:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =32767
	cmp r5, r4
	movgt r4, #1
	movle r4, #0
	bgt .L3319
	b .L3324
.L3314:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3315
.L3315:
	b .L3303
.L3318:
	b .L3314
.L3319:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-344
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE11
	mov r8, #4
	mul r7, r6, r8
	add r6, r5, r7
	mov r5, r6
	ldr r6, [r5]
	sdiv r5, r4, r6
	ldr r4, =-348
	str r5, [fp, r4]
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =65536
	add r6, r5, r4
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE11
	mov r8, #4
	mul r7, r4, r8
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3321
.L3320:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-344
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE11
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	sdiv r5, r4, r6
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3321
.L3321:
	b .L3315
.L3324:
	b .L3320
.L3331:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-248]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3338
.L3332:
	ldr r4, [fp, #-248]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3331
	b .L3336
.L3333:
	ldr r4, [fp, #-244]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	mov r0, r4
	bl putint
	mov r0, #10
	bl putch
	ldr r4, =2
	ldr r5, =-340
	str r4, [fp, r5]
	b .L3788
.L3336:
	b .L3333
.L3337:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3345
	b .L3347
.L3338:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3337
	b .L3342
.L3339:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3353
	b .L3356
	b .F11
.LTORG
addr_SHIFT_TABLE11:
	.word SHIFT_TABLE
.F11:
.L3342:
	b .L3339
.L3343:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE12
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3344
.L3344:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3338
.L3345:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3343
	b .L3350
.L3347:
	b .L3344
.L3350:
	b .L3344
.L3353:
	ldr r4, [fp, #-244]
	str r4, [fp, #-240]
	ldr r4, [fp, #-252]
	str r4, [fp, #-236]
	ldr r4, =0
	str r4, [fp, #-232]
	b .L3362
.L3354:
	ldr r4, [fp, #-252]
	str r4, [fp, #-204]
	ldr r4, [fp, #-252]
	str r4, [fp, #-200]
	ldr r4, =0
	str r4, [fp, #-196]
	b .L3563
.L3356:
	b .L3354
.L3361:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-236]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3368
.L3362:
	ldr r4, [fp, #-236]
	cmp r4, #0
	bne .L3361
	b .L3365
.L3363:
	ldr r4, [fp, #-232]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-244]
	b .L3354
.L3365:
	b .L3363
.L3367:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3375
	b .L3377
.L3368:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3367
	b .L3372
.L3369:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3383
	b .L3386
.L3372:
	b .L3369
.L3373:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE12
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3374
.L3374:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3368
.L3375:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3373
	b .L3380
.L3377:
	b .L3374
.L3380:
	b .L3374
.L3383:
	ldr r4, [fp, #-232]
	str r4, [fp, #-228]
	ldr r4, [fp, #-240]
	str r4, [fp, #-224]
	b .L3392
.L3384:
	ldr r4, [fp, #-240]
	str r4, [fp, #-216]
	ldr r4, [fp, #-240]
	str r4, [fp, #-212]
	b .L3464
	b .F12
.LTORG
addr_SHIFT_TABLE12:
	.word SHIFT_TABLE
.F12:
.L3386:
	b .L3384
.L3391:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-228]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-224]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3398
.L3392:
	ldr r4, [fp, #-224]
	cmp r4, #0
	bne .L3391
	b .L3395
.L3393:
	ldr r4, [fp, #-228]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-232]
	b .L3384
.L3395:
	b .L3393
.L3397:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3403
	b .L3407
.L3398:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3397
	b .L3402
.L3399:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-220]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-228]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-224]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3422
.L3402:
	b .L3399
.L3403:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3409
	b .L3413
.L3404:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3415
	b .L3418
.L3405:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3398
.L3407:
	b .L3404
.L3409:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE13
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3410
.L3410:
	b .L3405
.L3413:
	b .L3410
.L3415:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE13
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3416
.L3416:
	b .L3405
.L3418:
	b .L3416
.L3421:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3429
	b .L3431
.L3422:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3421
	b .L3426
.L3423:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-224]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3437
	b .L3442
.L3426:
	b .L3423
.L3427:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE13
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3428
	b .F13
.LTORG
addr_SHIFT_TABLE13:
	.word SHIFT_TABLE
.F13:
.L3428:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3422
.L3429:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3427
	b .L3434
.L3431:
	b .L3428
.L3434:
	b .L3428
.L3437:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3439
.L3438:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-224]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE14
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3445
.L3439:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-224]
	ldr r4, [fp, #-220]
	str r4, [fp, #-228]
	b .L3392
.L3442:
	b .L3438
.L3444:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3452
	b .L3454
.L3445:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3444
	b .L3449
.L3446:
	b .L3439
.L3449:
	b .L3446
.L3450:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE14
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3451
.L3451:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3445
.L3452:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3450
	b .L3457
.L3454:
	b .L3451
.L3457:
	b .L3451
.L3463:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-216]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-212]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3470
.L3464:
	ldr r4, [fp, #-212]
	cmp r4, #0
	bne .L3463
	b .L3467
.L3465:
	ldr r4, [fp, #-216]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-240]
	ldr r4, [fp, #-236]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r4, =-344
	ldr r5, [fp, r4]
	cmp r5, #15
	movge r4, #1
	movlt r4, #0
	bge .L3532
	b .L3537
.L3467:
	b .L3465
	b .F14
.LTORG
addr_SHIFT_TABLE14:
	.word SHIFT_TABLE
.F14:
.L3469:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3475
	b .L3479
.L3470:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3469
	b .L3474
.L3471:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-208]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-216]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-212]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3494
.L3474:
	b .L3471
.L3475:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3481
	b .L3485
.L3476:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L3487
	b .L3490
.L3477:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L3470
.L3479:
	b .L3476
.L3481:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r4, =-352
	ldr r6, [fp, r4]
	ldr r4, addr_SHIFT_TABLE15
	mov r8, #4
	mul r7, r6, r8
	add r6, r4, r7
	mov r4, r6
	ldr r6, [r4]
	ldr r4, =1
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3482
.L3482:
	b .L3477
.L3485:
	b .L3482
.L3487:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE15
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3488
.L3488:
	b .L3477
.L3490:
	b .L3488
.L3493:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3501
	b .L3503
.L3494:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3493
	b .L3498
.L3495:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-212]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3509
	b .L3514
.L3498:
	b .L3495
.L3499:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE15
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3500
.L3500:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3494
	b .F15
.LTORG
addr_SHIFT_TABLE15:
	.word SHIFT_TABLE
.F15:
.L3501:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3499
	b .L3506
.L3503:
	b .L3500
.L3506:
	b .L3500
.L3509:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3511
.L3510:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-212]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE16
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3517
.L3511:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-212]
	ldr r4, [fp, #-208]
	str r4, [fp, #-216]
	b .L3464
.L3514:
	b .L3510
.L3516:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L3524
	b .L3526
.L3517:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3516
	b .L3521
.L3518:
	b .L3511
.L3521:
	b .L3518
.L3522:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE16
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3523
.L3523:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3517
.L3524:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3522
	b .L3529
.L3526:
	b .L3523
.L3529:
	b .L3523
.L3532:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L3538
	b .L3543
.L3533:
	ldr r4, =-344
	ldr r5, [fp, r4]
	cmp r5, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3544
	b .L3549
.L3534:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-236]
	b .L3362
.L3537:
	b .L3533
.L3538:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3540
.L3539:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3540
.L3540:
	b .L3534
.L3543:
	b .L3539
.L3544:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L3550
	b .L3555
.L3545:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3546
.L3546:
	b .L3534
.L3549:
	b .L3545
.L3550:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-344
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE16
	mov r8, #4
	mul r7, r6, r8
	add r6, r5, r7
	mov r5, r6
	ldr r6, [r5]
	sdiv r5, r4, r6
	ldr r4, =-348
	str r5, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE16
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3552
	b .F16
.LTORG
addr_SHIFT_TABLE16:
	.word SHIFT_TABLE
.F16:
.L3551:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-344
	ldr r6, [fp, r4]
	ldr r4, addr_SHIFT_TABLE17
	mov r8, #4
	mul r7, r6, r8
	add r6, r4, r7
	mov r4, r6
	ldr r6, [r4]
	sdiv r4, r5, r6
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3552
.L3552:
	b .L3546
.L3555:
	b .L3551
.L3562:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-200]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3569
.L3563:
	ldr r4, [fp, #-200]
	cmp r4, #0
	bne .L3562
	b .L3566
.L3564:
	ldr r4, [fp, #-196]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-252]
	ldr r4, [fp, #-248]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r4, =-344
	ldr r5, [fp, r4]
	cmp r5, #15
	movge r4, #1
	movlt r4, #0
	bge .L3760
	b .L3765
.L3566:
	b .L3564
.L3568:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L3576
	b .L3578
.L3569:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3568
	b .L3573
.L3570:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L3584
	b .L3587
.L3573:
	b .L3570
.L3574:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE17
	mov r8, #4
	mul r7, r4, r8
	add r4, r6, r7
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3575
.L3575:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3569
.L3576:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3574
	b .L3581
.L3578:
	b .L3575
.L3581:
	b .L3575
.L3584:
	ldr r4, [fp, #-196]
	str r4, [fp, #-192]
	ldr r4, [fp, #-204]
	str r4, [fp, #-188]
	b .L3593
.L3585:
	ldr r4, [fp, #-204]
	str r4, [fp, #-180]
	ldr r4, [fp, #-204]
	str r4, [fp, #-176]
	b .L3665
.L3587:
	b .L3585
.L3592:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-192]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-188]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3599
.L3593:
	ldr r4, [fp, #-188]
	cmp r4, #0
	bne .L3592
	b .L3596
.L3594:
	ldr r4, [fp, #-192]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-196]
	b .L3585
.L3596:
	b .L3594
.L3598:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3604
	b .L3608
	b .F17
.LTORG
addr_SHIFT_TABLE17:
	.word SHIFT_TABLE
.F17:
.L3599:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3598
	b .L3603
.L3600:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-184]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-192]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-188]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3623
.L3603:
	b .L3600
.L3604:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3610
	b .L3614
.L3605:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3616
	b .L3619
.L3606:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3599
.L3608:
	b .L3605
.L3610:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE18
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3611
.L3611:
	b .L3606
.L3614:
	b .L3611
.L3616:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE18
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3617
.L3617:
	b .L3606
.L3619:
	b .L3617
.L3622:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3630
	b .L3632
.L3623:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3622
	b .L3627
.L3624:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-188]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3638
	b .L3643
.L3627:
	b .L3624
.L3628:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE18
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3629
.L3629:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L3623
.L3630:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L3628
	b .L3635
	b .F18
.LTORG
addr_SHIFT_TABLE18:
	.word SHIFT_TABLE
.F18:
.L3632:
	b .L3629
.L3635:
	b .L3629
.L3638:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3640
.L3639:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-188]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE19
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3646
.L3640:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-188]
	ldr r4, [fp, #-184]
	str r4, [fp, #-192]
	b .L3593
.L3643:
	b .L3639
.L3645:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3653
	b .L3655
.L3646:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3645
	b .L3650
.L3647:
	b .L3640
.L3650:
	b .L3647
.L3651:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE19
	mov r8, #4
	mul r7, r6, r8
	add r6, r5, r7
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3652
.L3652:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3646
.L3653:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3651
	b .L3658
.L3655:
	b .L3652
.L3658:
	b .L3652
.L3664:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-180]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-176]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3671
.L3665:
	ldr r4, [fp, #-176]
	cmp r4, #0
	bne .L3664
	b .L3668
.L3666:
	ldr r4, [fp, #-180]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-204]
	ldr r4, [fp, #-200]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L3733
	b .L3738
.L3668:
	b .L3666
.L3670:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3676
	b .L3680
.L3671:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3670
	b .L3675
.L3672:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-172]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-180]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-176]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3695
	b .F19
.LTORG
addr_SHIFT_TABLE19:
	.word SHIFT_TABLE
.F19:
.L3675:
	b .L3672
.L3676:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3682
	b .L3686
.L3677:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3688
	b .L3691
.L3678:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3671
.L3680:
	b .L3677
.L3682:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE20
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3683
.L3683:
	b .L3678
.L3686:
	b .L3683
.L3688:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE20
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3689
.L3689:
	b .L3678
.L3691:
	b .L3689
.L3694:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3702
	b .L3704
.L3695:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3694
	b .L3699
.L3696:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-176]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3710
	b .L3715
.L3699:
	b .L3696
.L3700:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE20
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3701
.L3701:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3695
.L3702:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3700
	b .L3707
.L3704:
	b .L3701
.L3707:
	b .L3701
.L3710:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3712
.L3711:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-176]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE20
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3718
	b .F20
.LTORG
addr_SHIFT_TABLE20:
	.word SHIFT_TABLE
.F20:
.L3712:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-176]
	ldr r4, [fp, #-172]
	str r4, [fp, #-180]
	b .L3665
.L3715:
	b .L3711
.L3717:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3725
	b .L3727
.L3718:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3717
	b .L3722
.L3719:
	b .L3712
.L3722:
	b .L3719
.L3723:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE21
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3724
.L3724:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3718
.L3725:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3723
	b .L3730
.L3727:
	b .L3724
.L3730:
	b .L3724
.L3733:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L3739
	b .L3744
.L3734:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3745
	b .L3750
.L3735:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-200]
	b .L3563
.L3738:
	b .L3734
.L3739:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3741
.L3740:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3741
.L3741:
	b .L3735
.L3744:
	b .L3740
.L3745:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L3751
	b .L3756
.L3746:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3747
.L3747:
	b .L3735
.L3750:
	b .L3746
.L3751:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE21
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =15
	sub r7, r4, r5
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE21
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3753
.L3752:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE21
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-356
	str r6, [fp, r4]
	b .L3753
.L3753:
	b .L3747
	b .F21
.LTORG
addr_SHIFT_TABLE21:
	.word SHIFT_TABLE
.F21:
.L3756:
	b .L3752
.L3760:
	ldr r4, =-348
	ldr r5, [fp, r4]
	cmp r5, #0
	movlt r4, #1
	movge r4, #0
	blt .L3766
	b .L3771
.L3761:
	ldr r4, =-344
	ldr r5, [fp, r4]
	cmp r5, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3772
	b .L3777
.L3762:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-248]
	b .L3332
.L3765:
	b .L3761
.L3766:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3768
.L3767:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3768
.L3768:
	b .L3762
.L3771:
	b .L3767
.L3772:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =32767
	cmp r5, r4
	movgt r4, #1
	movle r4, #0
	bgt .L3778
	b .L3783
.L3773:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3774
.L3774:
	b .L3762
.L3777:
	b .L3773
.L3778:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE22
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =65536
	add r6, r5, r4
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =15
	sub r7, r4, r5
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE22
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3780
.L3779:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE22
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-356
	str r6, [fp, r4]
	b .L3780
.L3780:
	b .L3774
.L3783:
	b .L3779
.L3787:
	ldr r4, =2
	str r4, [fp, #-168]
	ldr r5, =-340
	ldr r4, [fp, r5]
	str r4, [fp, #-164]
	ldr r4, =1
	str r4, [fp, #-160]
	b .L3797
.L3788:
	ldr r4, =-340
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3787
	b .L3792
.L3789:
	ldr r4, =0
	ldr r5, =-340
	str r4, [fp, r5]
	b .L4253
.L3792:
	b .L3789
.L3796:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-164]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3803
.L3797:
	ldr r4, [fp, #-164]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L3796
	b .L3801
.L3798:
	ldr r4, [fp, #-160]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	mov r0, r4
	bl putint
	mov r0, #10
	bl putch
	ldr r5, =-340
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-340
	str r5, [fp, r4]
	b .L3788
.L3801:
	b .L3798
.L3802:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L3810
	b .L3812
.L3803:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3802
	b .L3807
	b .F22
.LTORG
addr_SHIFT_TABLE22:
	.word SHIFT_TABLE
.F22:
.L3804:
	ldr r4, =-356
	ldr r5, [fp, r4]
	cmp r5, #0
	bne .L3818
	b .L3821
.L3807:
	b .L3804
.L3808:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE23
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3809
.L3809:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L3803
.L3810:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3808
	b .L3815
.L3812:
	b .L3809
.L3815:
	b .L3809
.L3818:
	ldr r4, [fp, #-160]
	str r4, [fp, #-156]
	ldr r4, [fp, #-168]
	str r4, [fp, #-152]
	ldr r4, =0
	str r4, [fp, #-148]
	b .L3827
.L3819:
	ldr r4, [fp, #-168]
	str r4, [fp, #-120]
	ldr r4, [fp, #-168]
	str r4, [fp, #-116]
	ldr r4, =0
	str r4, [fp, #-112]
	b .L4028
.L3821:
	b .L3819
.L3826:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-152]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3833
.L3827:
	ldr r4, [fp, #-152]
	cmp r4, #0
	bne .L3826
	b .L3830
.L3828:
	ldr r4, [fp, #-148]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-160]
	b .L3819
.L3830:
	b .L3828
.L3832:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L3840
	b .L3842
.L3833:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3832
	b .L3837
.L3834:
	ldr r4, =-356
	ldr r5, [fp, r4]
	cmp r5, #0
	bne .L3848
	b .L3851
.L3837:
	b .L3834
.L3838:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE23
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3839
.L3839:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3833
.L3840:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3838
	b .L3845
.L3842:
	b .L3839
.L3845:
	b .L3839
.L3848:
	ldr r4, [fp, #-148]
	str r4, [fp, #-144]
	ldr r4, [fp, #-156]
	str r4, [fp, #-140]
	b .L3857
	b .F23
.LTORG
addr_SHIFT_TABLE23:
	.word SHIFT_TABLE
.F23:
.L3849:
	ldr r4, [fp, #-156]
	str r4, [fp, #-132]
	ldr r4, [fp, #-156]
	str r4, [fp, #-128]
	b .L3929
.L3851:
	b .L3849
.L3856:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-144]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-140]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3863
.L3857:
	ldr r4, [fp, #-140]
	cmp r4, #0
	bne .L3856
	b .L3860
.L3858:
	ldr r4, [fp, #-144]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-148]
	b .L3849
.L3860:
	b .L3858
.L3862:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3868
	b .L3872
.L3863:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L3862
	b .L3867
.L3864:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-136]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-144]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-140]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3887
.L3867:
	b .L3864
.L3868:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3874
	b .L3878
.L3869:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L3880
	b .L3883
.L3870:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3863
.L3872:
	b .L3869
.L3874:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE24
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3875
.L3875:
	b .L3870
.L3878:
	b .L3875
.L3880:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE24
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3881
.L3881:
	b .L3870
.L3883:
	b .L3881
.L3886:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3894
	b .L3896
.L3887:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3886
	b .L3891
.L3888:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-140]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3902
	b .L3907
	b .F24
.LTORG
addr_SHIFT_TABLE24:
	.word SHIFT_TABLE
.F24:
.L3891:
	b .L3888
.L3892:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE25
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3893
.L3893:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3887
.L3894:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3892
	b .L3899
.L3896:
	b .L3893
.L3899:
	b .L3893
.L3902:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3904
.L3903:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-140]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE25
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3910
.L3904:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-140]
	ldr r4, [fp, #-136]
	str r4, [fp, #-144]
	b .L3857
.L3907:
	b .L3903
.L3909:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3917
	b .L3919
.L3910:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3909
	b .L3914
.L3911:
	b .L3904
.L3914:
	b .L3911
.L3915:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE25
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3916
.L3916:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L3910
.L3917:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3915
	b .L3922
.L3919:
	b .L3916
.L3922:
	b .L3916
.L3928:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-132]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-128]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3935
.L3929:
	ldr r4, [fp, #-128]
	cmp r4, #0
	bne .L3928
	b .L3932
.L3930:
	ldr r4, [fp, #-132]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-156]
	ldr r4, [fp, #-152]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L3997
	b .L4002
	b .F25
.LTORG
addr_SHIFT_TABLE25:
	.word SHIFT_TABLE
.F25:
.L3932:
	b .L3930
.L3934:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3940
	b .L3944
.L3935:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3934
	b .L3939
.L3936:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-124]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-132]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-128]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3959
.L3939:
	b .L3936
.L3940:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L3946
	b .L3950
.L3941:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3952
	b .L3955
.L3942:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3935
.L3944:
	b .L3941
.L3946:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE26
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3947
.L3947:
	b .L3942
.L3950:
	b .L3947
.L3952:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE26
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3953
.L3953:
	b .L3942
.L3955:
	b .L3953
.L3958:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3966
	b .L3968
.L3959:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3958
	b .L3963
.L3960:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-128]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L3974
	b .L3979
.L3963:
	b .L3960
.L3964:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE26
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3965
.L3965:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3959
	b .F26
.LTORG
addr_SHIFT_TABLE26:
	.word SHIFT_TABLE
.F26:
.L3966:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3964
	b .L3971
.L3968:
	b .L3965
.L3971:
	b .L3965
.L3974:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L3976
.L3975:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-128]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE27
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L3982
.L3976:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-128]
	ldr r4, [fp, #-124]
	str r4, [fp, #-132]
	b .L3929
.L3979:
	b .L3975
.L3981:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3989
	b .L3991
.L3982:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L3981
	b .L3986
.L3983:
	b .L3976
.L3986:
	b .L3983
.L3987:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE27
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L3988
.L3988:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L3982
.L3989:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L3987
	b .L3994
.L3991:
	b .L3988
.L3994:
	b .L3988
.L3997:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L4003
	b .L4008
.L3998:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4009
	b .L4014
.L3999:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-152]
	b .L3827
.L4002:
	b .L3998
.L4003:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4005
.L4004:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4005
.L4005:
	b .L3999
.L4008:
	b .L4004
.L4009:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L4015
	b .L4020
.L4010:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4011
.L4011:
	b .L3999
.L4014:
	b .L4010
.L4015:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE27
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE27
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4017
	b .F27
.LTORG
addr_SHIFT_TABLE27:
	.word SHIFT_TABLE
.F27:
.L4016:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE28
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-356
	str r6, [fp, r4]
	b .L4017
.L4017:
	b .L4011
.L4020:
	b .L4016
.L4027:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-116]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4034
.L4028:
	ldr r4, [fp, #-116]
	cmp r4, #0
	bne .L4027
	b .L4031
.L4029:
	ldr r4, [fp, #-112]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-168]
	ldr r4, [fp, #-164]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L4225
	b .L4230
.L4031:
	b .L4029
.L4033:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4041
	b .L4043
.L4034:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4033
	b .L4038
.L4035:
	ldr r4, =-356
	ldr r5, [fp, r4]
	cmp r5, #0
	bne .L4049
	b .L4052
.L4038:
	b .L4035
.L4039:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE28
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4040
.L4040:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4034
.L4041:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4039
	b .L4046
.L4043:
	b .L4040
.L4046:
	b .L4040
.L4049:
	ldr r4, [fp, #-112]
	str r4, [fp, #-108]
	ldr r4, [fp, #-120]
	str r4, [fp, #-104]
	b .L4058
.L4050:
	ldr r4, [fp, #-120]
	str r4, [fp, #-96]
	ldr r4, [fp, #-120]
	str r4, [fp, #-92]
	b .L4130
.L4052:
	b .L4050
.L4057:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-108]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-104]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4064
.L4058:
	ldr r4, [fp, #-104]
	cmp r4, #0
	bne .L4057
	b .L4061
.L4059:
	ldr r4, [fp, #-108]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-112]
	b .L4050
.L4061:
	b .L4059
.L4063:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4069
	b .L4073
	b .F28
.LTORG
addr_SHIFT_TABLE28:
	.word SHIFT_TABLE
.F28:
.L4064:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4063
	b .L4068
.L4065:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-100]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-108]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-104]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4088
.L4068:
	b .L4065
.L4069:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L4075
	b .L4079
.L4070:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4081
	b .L4084
.L4071:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4064
.L4073:
	b .L4070
.L4075:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE29
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4076
.L4076:
	b .L4071
.L4079:
	b .L4076
.L4081:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE29
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4082
.L4082:
	b .L4071
.L4084:
	b .L4082
.L4087:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4095
	b .L4097
.L4088:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4087
	b .L4092
.L4089:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-104]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L4103
	b .L4108
.L4092:
	b .L4089
.L4093:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE29
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4094
.L4094:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4088
.L4095:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L4093
	b .L4100
	b .F29
.LTORG
addr_SHIFT_TABLE29:
	.word SHIFT_TABLE
.F29:
.L4097:
	b .L4094
.L4100:
	b .L4094
.L4103:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4105
.L4104:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-104]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE30
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4111
.L4105:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-104]
	ldr r4, [fp, #-100]
	str r4, [fp, #-108]
	b .L4058
.L4108:
	b .L4104
.L4110:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4118
	b .L4120
.L4111:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4110
	b .L4115
.L4112:
	b .L4105
.L4115:
	b .L4112
.L4116:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE30
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4117
.L4117:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4111
.L4118:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4116
	b .L4123
.L4120:
	b .L4117
.L4123:
	b .L4117
.L4129:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-96]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-92]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4136
.L4130:
	ldr r4, [fp, #-92]
	cmp r4, #0
	bne .L4129
	b .L4133
.L4131:
	ldr r4, [fp, #-96]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-120]
	ldr r4, [fp, #-116]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L4198
	b .L4203
.L4133:
	b .L4131
.L4135:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4141
	b .L4145
.L4136:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4135
	b .L4140
.L4137:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-88]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-96]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-92]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4160
	b .F30
.LTORG
addr_SHIFT_TABLE30:
	.word SHIFT_TABLE
.F30:
.L4140:
	b .L4137
.L4141:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L4147
	b .L4151
.L4142:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4153
	b .L4156
.L4143:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4136
.L4145:
	b .L4142
.L4147:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE31
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4148
.L4148:
	b .L4143
.L4151:
	b .L4148
.L4153:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE31
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4154
.L4154:
	b .L4143
.L4156:
	b .L4154
.L4159:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4167
	b .L4169
.L4160:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4159
	b .L4164
.L4161:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-92]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L4175
	b .L4180
.L4164:
	b .L4161
.L4165:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE31
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4166
.L4166:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4160
.L4167:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L4165
	b .L4172
.L4169:
	b .L4166
.L4172:
	b .L4166
.L4175:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4177
.L4176:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-92]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE31
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4183
	b .F31
.LTORG
addr_SHIFT_TABLE31:
	.word SHIFT_TABLE
.F31:
.L4177:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-92]
	ldr r4, [fp, #-88]
	str r4, [fp, #-96]
	b .L4130
.L4180:
	b .L4176
.L4182:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4190
	b .L4192
.L4183:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4182
	b .L4187
.L4184:
	b .L4177
.L4187:
	b .L4184
.L4188:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE32
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4189
.L4189:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4183
.L4190:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4188
	b .L4195
.L4192:
	b .L4189
.L4195:
	b .L4189
.L4198:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L4204
	b .L4209
.L4199:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4210
	b .L4215
.L4200:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-116]
	b .L4028
.L4203:
	b .L4199
.L4204:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4206
.L4205:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4206
.L4206:
	b .L4200
.L4209:
	b .L4205
.L4210:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L4216
	b .L4221
.L4211:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4212
.L4212:
	b .L4200
.L4215:
	b .L4211
.L4216:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE32
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE32
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4218
.L4217:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE32
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-356
	str r6, [fp, r4]
	b .L4218
.L4218:
	b .L4212
	b .F32
.LTORG
addr_SHIFT_TABLE32:
	.word SHIFT_TABLE
.F32:
.L4221:
	b .L4217
.L4225:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L4231
	b .L4236
.L4226:
	ldr r4, =-344
	ldr r5, [fp, r4]
	cmp r5, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4237
	b .L4242
.L4227:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-164]
	b .L3797
.L4230:
	b .L4226
.L4231:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4233
.L4232:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4233
.L4233:
	b .L4227
.L4236:
	b .L4232
.L4237:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L4243
	b .L4248
.L4238:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4239
.L4239:
	b .L4227
.L4242:
	b .L4238
.L4243:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE33
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE33
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4245
.L4244:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE33
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-356
	str r6, [fp, r4]
	b .L4245
.L4245:
	b .L4239
.L4248:
	b .L4244
.L4252:
	ldr r4, =2
	str r4, [fp, #-84]
	ldr r4, =-340
	ldr r5, [fp, r4]
	str r5, [fp, #-80]
	ldr r4, =1
	str r4, [fp, #-76]
	b .L4262
.L4253:
	ldr r5, =-340
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4252
	b .L4257
.L4254:
	mov r0, #0
	ldr r1, =356
	add sp, sp, r1
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L4257:
	b .L4254
.L4261:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-80]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4268
.L4262:
	ldr r4, [fp, #-80]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4261
	b .L4266
.L4263:
	ldr r4, [fp, #-76]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-340
	ldr r4, [fp, r5]
	ldr r5, addr_SHIFT_TABLE33
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	ldr r6, =-356
	ldr r5, [fp, r6]
	cmp r4, r5
	bne .L4717
	b .L4722
.L4266:
	b .L4263
.L4267:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4275
	b .L4277
.L4268:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4267
	b .L4272
	b .F33
.LTORG
addr_SHIFT_TABLE33:
	.word SHIFT_TABLE
.F33:
.L4269:
	ldr r4, =-356
	ldr r5, [fp, r4]
	cmp r5, #0
	bne .L4283
	b .L4286
.L4272:
	b .L4269
.L4273:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE34
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4274
.L4274:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L4268
.L4275:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L4273
	b .L4280
.L4277:
	b .L4274
.L4280:
	b .L4274
.L4283:
	ldr r4, [fp, #-76]
	str r4, [fp, #-72]
	ldr r4, [fp, #-84]
	str r4, [fp, #-68]
	ldr r4, =0
	str r4, [fp, #-64]
	b .L4292
.L4284:
	ldr r4, [fp, #-84]
	str r4, [fp, #-36]
	ldr r4, [fp, #-84]
	str r4, [fp, #-32]
	ldr r4, =0
	str r4, [fp, #-28]
	b .L4493
.L4286:
	b .L4284
.L4291:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-68]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4298
.L4292:
	ldr r4, [fp, #-68]
	cmp r4, #0
	bne .L4291
	b .L4295
.L4293:
	ldr r4, [fp, #-64]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-76]
	b .L4284
.L4295:
	b .L4293
.L4297:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4305
	b .L4307
.L4298:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4297
	b .L4302
.L4299:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L4313
	b .L4316
.L4302:
	b .L4299
.L4303:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE34
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4304
.L4304:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4298
.L4305:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r6, r6, r4
	sub r4, r5, r6
	cmp r4, #0
	bne .L4303
	b .L4310
.L4307:
	b .L4304
.L4310:
	b .L4304
.L4313:
	ldr r4, [fp, #-64]
	str r4, [fp, #-60]
	ldr r4, [fp, #-72]
	str r4, [fp, #-56]
	b .L4322
	b .F34
.LTORG
addr_SHIFT_TABLE34:
	.word SHIFT_TABLE
.F34:
.L4314:
	ldr r4, [fp, #-72]
	str r4, [fp, #-48]
	ldr r4, [fp, #-72]
	str r4, [fp, #-44]
	b .L4394
.L4316:
	b .L4314
.L4321:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-60]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-56]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4328
.L4322:
	ldr r4, [fp, #-56]
	cmp r4, #0
	bne .L4321
	b .L4325
.L4323:
	ldr r4, [fp, #-60]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-64]
	b .L4314
.L4325:
	b .L4323
.L4327:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4333
	b .L4337
.L4328:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4327
	b .L4332
.L4329:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-52]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-60]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-56]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4352
.L4332:
	b .L4329
.L4333:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L4339
	b .L4343
.L4334:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4345
	b .L4348
.L4335:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4328
.L4337:
	b .L4334
.L4339:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE35
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4340
.L4340:
	b .L4335
.L4343:
	b .L4340
.L4345:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r4, =-352
	ldr r6, [fp, r4]
	ldr r4, addr_SHIFT_TABLE35
	mov r7, #4
	mul r8, r6, r7
	add r6, r4, r8
	mov r4, r6
	ldr r6, [r4]
	ldr r4, =1
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4346
.L4346:
	b .L4335
.L4348:
	b .L4346
.L4351:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4359
	b .L4361
.L4352:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4351
	b .L4356
.L4353:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-56]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L4367
	b .L4372
	b .F35
.LTORG
addr_SHIFT_TABLE35:
	.word SHIFT_TABLE
.F35:
.L4356:
	b .L4353
.L4357:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE36
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4358
.L4358:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4352
.L4359:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4357
	b .L4364
.L4361:
	b .L4358
.L4364:
	b .L4358
.L4367:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4369
.L4368:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-56]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE36
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4375
.L4369:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-56]
	ldr r4, [fp, #-52]
	str r4, [fp, #-60]
	b .L4322
.L4372:
	b .L4368
.L4374:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4382
	b .L4384
.L4375:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4374
	b .L4379
.L4376:
	b .L4369
.L4379:
	b .L4376
.L4380:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE36
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4381
.L4381:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L4375
.L4382:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4380
	b .L4387
.L4384:
	b .L4381
.L4387:
	b .L4381
.L4393:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-48]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-44]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4400
.L4394:
	ldr r4, [fp, #-44]
	cmp r4, #0
	bne .L4393
	b .L4397
.L4395:
	ldr r4, [fp, #-48]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-72]
	ldr r4, [fp, #-68]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L4462
	b .L4467
	b .F36
.LTORG
addr_SHIFT_TABLE36:
	.word SHIFT_TABLE
.F36:
.L4397:
	b .L4395
.L4399:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4405
	b .L4409
.L4400:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4399
	b .L4404
.L4401:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-40]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-48]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-44]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4424
.L4404:
	b .L4401
.L4405:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	beq .L4411
	b .L4415
.L4406:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4417
	b .L4420
.L4407:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4400
.L4409:
	b .L4406
.L4411:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE37
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4412
.L4412:
	b .L4407
.L4415:
	b .L4412
.L4417:
	ldr r4, =-356
	ldr r5, [fp, r4]
	ldr r6, =-352
	ldr r4, [fp, r6]
	ldr r6, addr_SHIFT_TABLE37
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	ldr r4, [r6]
	ldr r6, =1
	mul r7, r6, r4
	add r4, r5, r7
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4418
.L4418:
	b .L4407
.L4420:
	b .L4418
.L4423:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4431
	b .L4433
.L4424:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4423
	b .L4428
.L4425:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-44]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L4439
	b .L4444
.L4428:
	b .L4425
.L4429:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE37
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4430
.L4430:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4424
	b .F37
.LTORG
addr_SHIFT_TABLE37:
	.word SHIFT_TABLE
.F37:
.L4431:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4429
	b .L4436
.L4433:
	b .L4430
.L4436:
	b .L4430
.L4439:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4441
.L4440:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-44]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE38
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4447
.L4441:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-44]
	ldr r4, [fp, #-40]
	str r4, [fp, #-48]
	b .L4394
.L4444:
	b .L4440
.L4446:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4454
	b .L4456
.L4447:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4446
	b .L4451
.L4448:
	b .L4441
.L4451:
	b .L4448
.L4452:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE38
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4453
.L4453:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r4, =-352
	ldr r5, [fp, r4]
	add r4, r5, #1
	ldr r5, =-352
	str r4, [fp, r5]
	b .L4447
.L4454:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4452
	b .L4459
.L4456:
	b .L4453
.L4459:
	b .L4453
.L4462:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L4468
	b .L4473
.L4463:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4474
	b .L4479
.L4464:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-68]
	b .L4292
.L4467:
	b .L4463
.L4468:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4470
.L4469:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4470
.L4470:
	b .L4464
.L4473:
	b .L4469
.L4474:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L4480
	b .L4485
.L4475:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4476
.L4476:
	b .L4464
.L4479:
	b .L4475
.L4480:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE38
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =15
	sub r7, r5, r4
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE38
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4482
	b .F38
.LTORG
addr_SHIFT_TABLE38:
	.word SHIFT_TABLE
.F38:
.L4481:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-344
	ldr r6, [fp, r4]
	ldr r4, addr_SHIFT_TABLE39
	mov r7, #4
	mul r8, r6, r7
	add r6, r4, r8
	mov r4, r6
	ldr r6, [r4]
	sdiv r4, r5, r6
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4482
.L4482:
	b .L4476
.L4485:
	b .L4481
.L4492:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-32]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4499
.L4493:
	ldr r4, [fp, #-32]
	cmp r4, #0
	bne .L4492
	b .L4496
.L4494:
	ldr r4, [fp, #-28]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-84]
	ldr r4, [fp, #-80]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L4690
	b .L4695
.L4496:
	b .L4494
.L4498:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4506
	b .L4508
.L4499:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4498
	b .L4503
.L4500:
	ldr r5, =-356
	ldr r4, [fp, r5]
	cmp r4, #0
	bne .L4514
	b .L4517
.L4503:
	b .L4500
.L4504:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE39
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4505
.L4505:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4499
.L4506:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4504
	b .L4511
.L4508:
	b .L4505
.L4511:
	b .L4505
.L4514:
	ldr r4, [fp, #-28]
	str r4, [fp, #-24]
	ldr r4, [fp, #-36]
	str r4, [fp, #-20]
	b .L4523
.L4515:
	ldr r4, [fp, #-36]
	str r4, [fp, #-12]
	ldr r4, [fp, #-36]
	str r4, [fp, #-8]
	b .L4595
.L4517:
	b .L4515
.L4522:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-24]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-20]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4529
.L4523:
	ldr r4, [fp, #-20]
	cmp r4, #0
	bne .L4522
	b .L4526
.L4524:
	ldr r4, [fp, #-24]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-28]
	b .L4515
.L4526:
	b .L4524
.L4528:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4534
	b .L4538
	b .F39
.LTORG
addr_SHIFT_TABLE39:
	.word SHIFT_TABLE
.F39:
.L4529:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4528
	b .L4533
.L4530:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-16]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-24]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-20]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4553
.L4533:
	b .L4530
.L4534:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	beq .L4540
	b .L4544
.L4535:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4546
	b .L4549
.L4536:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4529
.L4538:
	b .L4535
.L4540:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE40
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4541
.L4541:
	b .L4536
.L4544:
	b .L4541
.L4546:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE40
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4547
.L4547:
	b .L4536
.L4549:
	b .L4547
.L4552:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4560
	b .L4562
.L4553:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4552
	b .L4557
.L4554:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-20]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L4568
	b .L4573
.L4557:
	b .L4554
.L4558:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE40
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4559
.L4559:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4553
.L4560:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L4558
	b .L4565
	b .F40
.LTORG
addr_SHIFT_TABLE40:
	.word SHIFT_TABLE
.F40:
.L4562:
	b .L4559
.L4565:
	b .L4559
.L4568:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4570
.L4569:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-20]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE41
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4576
.L4570:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-20]
	ldr r4, [fp, #-16]
	str r4, [fp, #-24]
	b .L4523
.L4573:
	b .L4569
.L4575:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4583
	b .L4585
.L4576:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4575
	b .L4580
.L4577:
	b .L4570
.L4580:
	b .L4577
.L4581:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE41
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4582
.L4582:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4576
.L4583:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4581
	b .L4588
.L4585:
	b .L4582
.L4588:
	b .L4582
.L4594:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-12]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-8]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4601
.L4595:
	ldr r4, [fp, #-8]
	cmp r4, #0
	bne .L4594
	b .L4598
.L4596:
	ldr r4, [fp, #-12]
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-36]
	ldr r4, [fp, #-32]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, =1
	ldr r5, =-344
	str r4, [fp, r5]
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #15
	movge r4, #1
	movlt r4, #0
	bge .L4663
	b .L4668
.L4598:
	b .L4596
.L4600:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4606
	b .L4610
.L4601:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4600
	b .L4605
.L4602:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-4]
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-12]
	ldr r5, =-348
	str r4, [fp, r5]
	ldr r4, [fp, #-8]
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4625
	b .F41
.LTORG
addr_SHIFT_TABLE41:
	.word SHIFT_TABLE
.F41:
.L4605:
	b .L4602
.L4606:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	beq .L4612
	b .L4616
.L4607:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L4618
	b .L4621
.L4608:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4601
.L4610:
	b .L4607
.L4612:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE42
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4613
.L4613:
	b .L4608
.L4616:
	b .L4613
.L4618:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r5, =-352
	ldr r6, [fp, r5]
	ldr r5, addr_SHIFT_TABLE42
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	ldr r6, [r5]
	ldr r5, =1
	mul r7, r5, r6
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4619
.L4619:
	b .L4608
.L4621:
	b .L4619
.L4624:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4632
	b .L4634
.L4625:
	ldr r4, =-352
	ldr r5, [fp, r4]
	cmp r5, #16
	movlt r4, #1
	movge r4, #0
	blt .L4624
	b .L4629
.L4626:
	ldr r4, =-356
	ldr r5, [fp, r4]
	str r5, [fp, #-8]
	ldr r4, =1
	cmp r4, #15
	movgt r4, #1
	movle r4, #0
	bgt .L4640
	b .L4645
.L4629:
	b .L4626
.L4630:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE42
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4631
.L4631:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4625
.L4632:
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	cmp r6, #0
	bne .L4630
	b .L4637
.L4634:
	b .L4631
.L4637:
	b .L4631
.L4640:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4642
.L4641:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	ldr r4, =0
	ldr r5, =-352
	str r4, [fp, r5]
	ldr r4, [fp, #-8]
	mov r5, #1
	ldr r6, addr_SHIFT_TABLE42
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	mul r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r4, =65535
	ldr r5, =-344
	str r4, [fp, r5]
	b .L4648
	b .F42
.LTORG
addr_SHIFT_TABLE42:
	.word SHIFT_TABLE
.F42:
.L4642:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-8]
	ldr r4, [fp, #-4]
	str r4, [fp, #-12]
	b .L4595
.L4645:
	b .L4641
.L4647:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r6, r6, r5
	sub r5, r4, r6
	cmp r5, #0
	bne .L4655
	b .L4657
.L4648:
	ldr r5, =-352
	ldr r4, [fp, r5]
	cmp r4, #16
	movlt r4, #1
	movge r4, #0
	blt .L4647
	b .L4652
.L4649:
	b .L4642
.L4652:
	b .L4649
.L4653:
	ldr r5, =-356
	ldr r4, [fp, r5]
	ldr r6, =-352
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE43
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	ldr r6, =1
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4654
.L4654:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-344
	ldr r4, [fp, r5]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, =-344
	str r6, [fp, r4]
	ldr r5, =-352
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-352
	str r5, [fp, r4]
	b .L4648
.L4655:
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =2
	sdiv r6, r5, r4
	mul r4, r6, r4
	sub r6, r5, r4
	cmp r6, #0
	bne .L4653
	b .L4660
.L4657:
	b .L4654
.L4660:
	b .L4654
.L4663:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L4669
	b .L4674
.L4664:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4675
	b .L4680
.L4665:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-32]
	b .L4493
.L4668:
	b .L4664
.L4669:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4671
.L4670:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4671
.L4671:
	b .L4665
.L4674:
	b .L4670
.L4675:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =32767
	cmp r5, r4
	movgt r4, #1
	movle r4, #0
	bgt .L4681
	b .L4686
.L4676:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4677
.L4677:
	b .L4665
.L4680:
	b .L4676
.L4681:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE43
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =15
	sub r7, r4, r5
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE43
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4683
.L4682:
	ldr r4, =-348
	ldr r5, [fp, r4]
	ldr r4, =-344
	ldr r6, [fp, r4]
	ldr r4, addr_SHIFT_TABLE43
	mov r7, #4
	mul r8, r6, r7
	add r6, r4, r8
	mov r4, r6
	ldr r6, [r4]
	sdiv r4, r5, r6
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4683
.L4683:
	b .L4677
	b .F43
.LTORG
addr_SHIFT_TABLE43:
	.word SHIFT_TABLE
.F43:
.L4686:
	b .L4682
.L4690:
	ldr r5, =-348
	ldr r4, [fp, r5]
	cmp r4, #0
	movlt r4, #1
	movge r4, #0
	blt .L4696
	b .L4701
.L4691:
	ldr r5, =-344
	ldr r4, [fp, r5]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L4702
	b .L4707
.L4692:
	ldr r5, =-356
	ldr r4, [fp, r5]
	str r4, [fp, #-80]
	b .L4262
.L4695:
	b .L4691
.L4696:
	ldr r4, =65535
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4698
.L4697:
	ldr r4, =0
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4698
.L4698:
	b .L4692
.L4701:
	b .L4697
.L4702:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =32767
	cmp r4, r5
	movgt r4, #1
	movle r4, #0
	bgt .L4708
	b .L4713
.L4703:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =-356
	str r4, [fp, r5]
	b .L4704
.L4704:
	b .L4692
.L4707:
	b .L4703
.L4708:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE44
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-348
	str r6, [fp, r4]
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r5, =65536
	add r6, r4, r5
	ldr r4, =-344
	ldr r5, [fp, r4]
	ldr r4, =15
	sub r7, r4, r5
	add r4, r7, #1
	ldr r5, addr_SHIFT_TABLE44
	mov r7, #4
	mul r8, r4, r7
	add r4, r5, r8
	mov r5, r4
	ldr r4, [r5]
	sub r5, r6, r4
	ldr r4, =-356
	str r5, [fp, r4]
	b .L4710
.L4709:
	ldr r5, =-348
	ldr r4, [fp, r5]
	ldr r6, =-344
	ldr r5, [fp, r6]
	ldr r6, addr_SHIFT_TABLE44
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	ldr r5, [r6]
	sdiv r6, r4, r5
	ldr r4, =-356
	str r6, [fp, r4]
	b .L4710
.L4710:
	b .L4704
.L4713:
	b .L4709
.L4717:
	mov r0, #1
	ldr r1, =356
	add sp, sp, r1
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .L4718
.L4718:
	ldr r5, =-340
	ldr r4, [fp, r5]
	add r5, r4, #1
	ldr r4, =-340
	str r5, [fp, r4]
	b .L4253
.L4722:
	b .L4718

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L4723:
	bl long_func
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {r4, fp, lr}
	bx lr

addr_SHIFT_TABLE44:
	.word SHIFT_TABLE
