	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global ifElseIf
	.type ifElseIf , %function
ifElseIf:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L32:
	ldr r4, =5
	str r4, [fp, #-8]
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	cmp r4, #6
	beq .L35
	b .L41
.L35:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
	b .L37
.L36:
	ldr r4, [fp, #-4]
	cmp r4, #10
	beq .L48
	b .L51
.L37:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
.L38:
	ldr r4, [fp, #-4]
	cmp r4, #11
	beq .L35
	b .L44
.L41:
	b .L38
.L44:
	b .L36
.L45:
	ldr r4, =25
	str r4, [fp, #-8]
	b .L47
.L46:
	ldr r4, [fp, #-4]
	cmp r4, #10
	beq .L58
	b .L61
.L47:
	b .L37
.L48:
	ldr r4, [fp, #-8]
	cmp r4, #1
	beq .L45
	b .L54
.L51:
	b .L46
.L54:
	b .L46
.L55:
	ldr r4, [fp, #-8]
	add r5, r4, #15
	str r5, [fp, #-8]
	b .L57
.L56:
	ldr r4, [fp, #-8]
	ldr r5, =0
	sub r6, r5, r4
	str r6, [fp, #-8]
	b .L57
.L57:
	b .L47
.L58:
	ldr r4, [fp, #-8]
	ldr r5, =0
	sub r6, r5, #5
	cmp r4, r6
	beq .L55
	b .L64
.L61:
	b .L56
.L64:
	b .L56

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L65:
	bl ifElseIf
	mov r4, r0
	mov r0, r4
	bl putint
	mov r0, #0
	add sp, sp, #0
	pop {r4, fp, lr}
	bx lr

