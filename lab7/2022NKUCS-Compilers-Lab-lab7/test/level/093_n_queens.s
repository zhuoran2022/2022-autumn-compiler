	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global sum
	.align 4
	.size sum, 4
sum:
	.word 0
	.global n
	.align 4
	.size n, 4
n:
	.word 0
	.comm ans, 200, 4
	.comm row, 200, 4
	.comm line1, 200, 4
	.comm line2, 400, 4
	.text
	.global printans
	.type printans , %function
printans:
	push {r4, r5, r6, r7, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L98:
	ldr r4, addr_sum0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_sum0
	str r4, [r5]
	ldr r4, =1
	str r4, [fp, #-4]
	b .L101
.L100:
	ldr r4, [fp, #-4]
	ldr r5, addr_ans0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	mov r0, r4
	bl putint
	ldr r4, [fp, #-4]
	ldr r5, addr_n0
	ldr r6, [r5]
	cmp r4, r6
	beq .L107
	b .L112
.L101:
	ldr r4, [fp, #-4]
	ldr r5, addr_n0
	ldr r6, [r5]
	cmp r4, r6
	movle r4, #1
	movgt r4, #0
	ble .L100
	b .L105
.L102:
	add sp, sp, #4
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L103:
	add sp, sp, #4
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L104:
	add sp, sp, #4
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L105:
	b .L102
.L107:
	mov r0, #10
	bl putch
	add sp, sp, #4
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
	b .L109
.L108:
	mov r0, #32
	bl putch
	b .L109
.L109:
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L101
.L110:
	add sp, sp, #4
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L111:
	add sp, sp, #4
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L112:
	b .L108

	.global f
	.type f , %function
f:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L113:
	str r0, [fp, #-8]
	ldr r4, =1
	str r4, [fp, #-4]
	b .L117
.L116:
	ldr r4, [fp, #-4]
	ldr r5, addr_row0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	cmp r4, #1
	bne .L125
	b .L129
.L117:
	ldr r4, [fp, #-4]
	ldr r5, addr_n0
	ldr r6, [r5]
	cmp r4, r6
	movle r4, #1
	movgt r4, #0
	ble .L116
	b .L121
.L118:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L119:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L120:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L121:
	b .L118
.L122:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	ldr r6, addr_ans0
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	str r4, [r6]
	ldr r4, [fp, #-8]
	ldr r5, addr_n0
	ldr r6, [r5]
	cmp r4, r6
	beq .L139
	b .L143
.L123:
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L117
.L124:
	ldr r4, addr_n0
	ldr r5, [r4]
	ldr r4, [fp, #-8]
	add r6, r5, r4
	ldr r4, [fp, #-4]
	sub r5, r6, r4
	ldr r4, addr_line20
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #0
	moveq r4, #1
	movne r4, #0
	cmp r4, #0
	bne .L122
	b .L137
.L125:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	add r6, r4, r5
	ldr r4, addr_line10
	mov r5, #4
	mul r7, r6, r5
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #0
	beq .L124
	b .L133
.L127:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L128:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L129:
	b .L123
.L131:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L132:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L133:
	b .L123
.L136:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L137:
	b .L123
.L138:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L139:
	bl printans
	b .L140
.L140:
	ldr r4, [fp, #-4]
	ldr r5, addr_row0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, =1
	str r4, [r5]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	add r6, r4, r5
	ldr r4, addr_line10
	mov r5, #4
	mul r7, r6, r5
	add r5, r4, r7
	mov r4, r5
	ldr r5, =1
	str r5, [r4]
	ldr r4, addr_n0
	ldr r5, [r4]
	ldr r4, [fp, #-8]
	add r6, r5, r4
	ldr r4, [fp, #-4]
	sub r5, r6, r4
	ldr r4, addr_line20
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, =1
	str r5, [r4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	mov r0, r5
	bl f
	ldr r4, [fp, #-4]
	ldr r5, addr_row0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, =0
	str r4, [r5]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	add r6, r4, r5
	ldr r4, addr_line10
	mov r5, #4
	mul r7, r6, r5
	add r5, r4, r7
	mov r4, r5
	ldr r5, =0
	str r5, [r4]
	ldr r4, addr_n0
	ldr r5, [r4]
	ldr r4, [fp, #-8]
	add r6, r5, r4
	ldr r4, [fp, #-4]
	sub r5, r6, r4
	ldr r4, addr_line20
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, =0
	str r5, [r4]
	b .L123
	b .F0
.LTORG
addr_ans0:
	.word ans
addr_sum0:
	.word sum
addr_n0:
	.word n
addr_row0:
	.word row
addr_line10:
	.word line1
addr_line20:
	.word line2
.F0:
.L141:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L142:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L143:
	b .L140

	.global main
	.type main , %function
main:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L144:
	bl getint
	mov r4, r0
	str r4, [fp, #-4]
	b .L147
.L146:
	bl getint
	mov r4, r0
	ldr r5, addr_n1
	str r4, [r5]
	mov r0, #1
	bl f
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-4]
	b .L147
.L147:
	ldr r4, [fp, #-4]
	cmp r4, #0
	movgt r4, #1
	movle r4, #0
	bgt .L146
	b .L151
.L148:
	ldr r4, addr_sum1
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {r4, r5, fp, lr}
	bx lr
.L151:
	b .L148

addr_ans1:
	.word ans
addr_sum1:
	.word sum
addr_n1:
	.word n
addr_row1:
	.word row
addr_line11:
	.word line1
addr_line21:
	.word line2
