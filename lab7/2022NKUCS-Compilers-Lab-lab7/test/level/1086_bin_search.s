	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #68
.L67:
	ldr r4, =0
	str r4, [fp, #-64]
	ldr r4, =0
	str r4, [fp, #-68]
	b .L72
.L71:
	ldr r4, [fp, #-68]
	add r5, r4, #1
	ldr r4, [fp, #-68]
	mov r6, #-60
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	add r6, fp, r4
	str r5, [r6]
	ldr r4, [fp, #-68]
	add r5, r4, #1
	str r5, [fp, #-68]
	b .L72
.L72:
	ldr r4, [fp, #-68]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L71
	b .L76
.L73:
	ldr r4, =10
	str r4, [fp, #-4]
	bl getint
	mov r4, r0
	str r4, [fp, #-20]
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-16]
	ldr r4, =0
	str r4, [fp, #-12]
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-12]
	add r6, r4, r5
	ldr r4, =2
	sdiv r5, r6, r4
	str r5, [fp, #-8]
	b .L83
.L76:
	b .L73
.L82:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-12]
	add r6, r4, r5
	ldr r4, =2
	sdiv r5, r6, r4
	str r5, [fp, #-8]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-8]
	mov r6, #-60
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	add r6, fp, r5
	ldr r5, [r6]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L93
	b .L99
.L83:
	ldr r4, [fp, #-8]
	mov r5, #-60
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	ldr r5, [fp, #-20]
	cmp r4, r5
	bne .L85
	b .L89
.L84:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-8]
	mov r6, #-60
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	cmp r4, r5
	beq .L100
	b .L106
.L85:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L82
	b .L92
.L89:
	b .L84
.L92:
	b .L84
.L93:
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	str r5, [fp, #-16]
	b .L95
.L94:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L95
.L95:
	b .L83
.L99:
	b .L94
.L100:
	ldr r4, [fp, #-20]
	mov r0, r4
	bl putint
	b .L102
.L101:
	ldr r4, =0
	str r4, [fp, #-20]
	ldr r4, [fp, #-20]
	mov r0, r4
	bl putint
	b .L102
.L102:
	ldr r4, =10
	str r4, [fp, #-20]
	ldr r4, [fp, #-20]
	mov r0, r4
	bl putch
	mov r0, #0
	add sp, sp, #68
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L106:
	b .L101

