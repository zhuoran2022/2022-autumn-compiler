	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global if_ifElse_
	.type if_ifElse_ , %function
if_ifElse_:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L15:
	ldr r4, =5
	str r4, [fp, #-8]
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	cmp r4, #5
	beq .L18
	b .L22
.L18:
	ldr r4, [fp, #-4]
	cmp r4, #10
	beq .L23
	b .L28
.L19:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, fp, lr}
	bx lr
.L22:
	b .L19
.L23:
	ldr r4, =25
	str r4, [fp, #-8]
	b .L25
.L24:
	ldr r4, [fp, #-8]
	add r5, r4, #15
	str r5, [fp, #-8]
	b .L25
.L25:
	b .L19
.L28:
	b .L24

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L29:
	bl if_ifElse_
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {r4, fp, lr}
	bx lr

