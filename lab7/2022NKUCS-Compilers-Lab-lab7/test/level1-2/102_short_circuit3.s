	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word 0
	.global b
	.align 4
	.size b, 4
b:
	.word 0
	.global d
	.align 4
	.size d, 4
d:
	.word 0
	.text
	.global set_a
	.type set_a , %function
set_a:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L153:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, addr_a0
	str r4, [r5]
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {r4, r5, fp, lr}
	bx lr

	.global set_b
	.type set_b , %function
set_b:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L155:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, addr_b0
	str r4, [r5]
	ldr r4, addr_b0
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {r4, r5, fp, lr}
	bx lr

	.global set_d
	.type set_d , %function
set_d:
	push {r4, r5, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L157:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, addr_d0
	str r4, [r5]
	ldr r4, addr_d0
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {r4, r5, fp, lr}
	bx lr

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, fp, lr}
	mov fp, sp
	sub sp, sp, #24
.L159:
	ldr r4, =2
	ldr r5, addr_a0
	str r4, [r5]
	ldr r4, =3
	ldr r5, addr_b0
	str r4, [r5]
	mov r0, #0
	bl set_a
	mov r4, r0
	cmp r4, #0
	bne .L162
	b .L164
.L160:
	b .L161
.L161:
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #32
	bl putch
	ldr r4, addr_b0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #32
	bl putch
	ldr r4, =2
	ldr r5, addr_a0
	str r4, [r5]
	ldr r4, =3
	ldr r5, addr_b0
	str r4, [r5]
	mov r0, #0
	bl set_a
	mov r4, r0
	cmp r4, #0
	bne .L171
	b .L173
.L162:
	mov r0, #1
	bl set_b
	mov r4, r0
	cmp r4, #0
	bne .L160
	b .L167
.L164:
	b .L161
.L167:
	b .L161
.L169:
	b .L170
.L170:
	ldr r4, addr_a0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #32
	bl putch
	ldr r4, addr_b0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #10
	bl putch
	ldr r4, =1
	str r4, [fp, #-24]
	ldr r4, =2
	ldr r5, addr_d0
	str r4, [r5]
	ldr r4, [fp, #-24]
	cmp r4, #1
	movge r4, #1
	movlt r4, #0
	bge .L181
	b .L184
.L171:
	mov r0, #1
	bl set_b
	mov r4, r0
	cmp r4, #0
	bne .L169
	b .L176
.L173:
	b .L170
.L176:
	b .L170
.L179:
	b .L180
.L180:
	ldr r4, addr_d0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #32
	bl putch
	ldr r4, [fp, #-24]
	cmp r4, #1
	movle r4, #1
	movgt r4, #0
	ble .L188
	b .L193
.L181:
	mov r0, #3
	bl set_d
	mov r4, r0
	cmp r4, #0
	bne .L179
	b .L186
.L184:
	b .L180
.L186:
	b .L180
.L188:
	b .L189
.L189:
	ldr r4, addr_d0
	ldr r5, [r4]
	mov r0, r5
	bl putint
	mov r0, #10
	bl putch
	ldr r4, =2
	add r5, r4, #1
	ldr r4, =3
	sub r6, r4, r5
	ldr r4, =16
	cmp r4, r6
	movge r4, #1
	movlt r4, #0
	bge .L197
	b .L201
.L190:
	mov r0, #4
	bl set_d
	mov r4, r0
	cmp r4, #0
	bne .L188
	b .L195
.L193:
	b .L190
.L195:
	b .L189
.L197:
	mov r0, #65
	bl putch
	b .L198
.L198:
	ldr r4, =25
	sub r5, r4, #7
	ldr r4, =6
	ldr r6, =3
	mul r7, r4, r6
	ldr r4, =36
	sub r6, r4, r7
	cmp r5, r6
	bne .L202
	b .L206
.L201:
	b .L198
.L202:
	mov r0, #66
	bl putch
	b .L203
.L203:
	ldr r4, =1
	cmp r4, #8
	movlt r4, #1
	movge r4, #0
	ldr r5, =7
	ldr r6, =2
	sdiv r7, r5, r6
	mul r6, r7, r6
	sub r7, r5, r6
	mov r5, r4
	cmp r5, r7
	bne .L207
	b .L215
.L206:
	b .L203
.L207:
	mov r0, #67
	bl putch
	b .L208
.L208:
	ldr r4, =3
	cmp r4, #4
	movgt r4, #1
	movle r4, #0
	mov r5, r4
	cmp r5, #0
	beq .L216
	b .L224
	b .F0
.LTORG
addr_a0:
	.word a
addr_b0:
	.word b
addr_d0:
	.word d
.F0:
.L210:
	mov r0, #0
	add sp, sp, #24
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L211:
	b .L210
.L215:
	b .L208
.L216:
	mov r0, #68
	bl putch
	b .L217
.L217:
	ldr r4, =102
	cmp r4, #63
	movle r4, #1
	movgt r4, #0
	mov r5, r4
	ldr r4, =1
	cmp r4, r5
	beq .L225
	b .L233
.L219:
	mov r0, #0
	add sp, sp, #24
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L220:
	b .L219
.L224:
	b .L217
.L225:
	mov r0, #69
	bl putch
	b .L226
.L226:
	ldr r4, =5
	sub r5, r4, #6
	ldr r4, =0
	cmp r4, #0
	moveq r4, #1
	movne r4, #0
	mov r6, r4
	ldr r4, =0
	sub r7, r4, r6
	cmp r5, r7
	beq .L234
	b .L240
.L228:
	mov r0, #0
	add sp, sp, #24
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L229:
	b .L228
.L233:
	b .L226
.L234:
	mov r0, #70
	bl putch
	b .L235
.L235:
	mov r0, #10
	bl putch
	ldr r4, =0
	str r4, [fp, #-20]
	ldr r4, =1
	str r4, [fp, #-16]
	ldr r4, =2
	str r4, [fp, #-12]
	ldr r4, =3
	str r4, [fp, #-8]
	ldr r4, =4
	str r4, [fp, #-4]
	b .L247
.L240:
	b .L235
.L246:
	mov r0, #32
	bl putch
	b .L247
.L247:
	ldr r4, [fp, #-20]
	cmp r4, #0
	bne .L249
	b .L251
.L248:
	ldr r4, [fp, #-20]
	cmp r4, #0
	bne .L256
	b .L260
.L249:
	ldr r4, [fp, #-16]
	cmp r4, #0
	bne .L246
	b .L254
.L251:
	b .L248
.L254:
	b .L248
.L256:
	mov r0, #67
	bl putch
	b .L257
.L257:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-16]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L265
	b .L270
.L258:
	ldr r4, [fp, #-16]
	cmp r4, #0
	bne .L256
	b .L263
.L260:
	b .L258
.L263:
	b .L257
.L265:
	mov r0, #72
	bl putch
	b .L266
.L266:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L276
	b .L279
.L267:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-20]
	cmp r4, r5
	movle r4, #1
	movgt r4, #0
	ble .L265
	b .L273
.L270:
	b .L267
.L273:
	b .L266
.L274:
	mov r0, #73
	bl putch
	b .L275
.L275:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-16]
	cmp r5, #0
	moveq r5, #1
	movne r5, #0
	mov r6, r5
	cmp r4, r6
	beq .L286
	b .L291
.L276:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	cmp r4, r5
	bne .L274
	b .L282
.L279:
	b .L275
.L282:
	b .L275
.L283:
	mov r0, #74
	bl putch
	b .L284
.L284:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-16]
	cmp r5, #0
	moveq r5, #1
	movne r5, #0
	mov r6, r5
	cmp r4, r6
	beq .L298
	b .L305
.L285:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-4]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L283
	b .L297
.L286:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L283
	b .L294
.L291:
	b .L285
.L294:
	b .L285
.L297:
	b .L284
.L298:
	mov r0, #75
	bl putch
	b .L299
	b .F1
.LTORG
addr_a1:
	.word a
addr_b1:
	.word b
addr_d1:
	.word d
.F1:
.L299:
	mov r0, #10
	bl putch
	mov r0, #0
	add sp, sp, #24
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L300:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L306
	b .L309
.L305:
	b .L300
.L306:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-4]
	cmp r4, r5
	movge r4, #1
	movlt r4, #0
	bge .L298
	b .L312
.L309:
	b .L299
.L312:
	b .L299

addr_a2:
	.word a
addr_b2:
	.word b
addr_d2:
	.word d
