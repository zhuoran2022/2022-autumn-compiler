	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global whileIf
	.type whileIf , %function
whileIf:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L21:
	ldr r4, =0
	str r4, [fp, #-8]
	ldr r4, =0
	str r4, [fp, #-4]
	b .L25
.L24:
	ldr r4, [fp, #-8]
	cmp r4, #5
	beq .L30
	b .L35
.L25:
	ldr r4, [fp, #-8]
	cmp r4, #100
	movlt r4, #1
	movge r4, #0
	blt .L24
	b .L29
.L26:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
.L29:
	b .L26
.L30:
	ldr r4, =25
	str r4, [fp, #-4]
	b .L32
.L31:
	ldr r4, [fp, #-8]
	cmp r4, #10
	beq .L36
	b .L41
.L32:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L25
.L35:
	b .L31
.L36:
	ldr r4, =42
	str r4, [fp, #-4]
	b .L38
.L37:
	ldr r4, [fp, #-8]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-4]
	b .L38
.L38:
	b .L32
.L41:
	b .L37

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L42:
	bl whileIf
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {r4, fp, lr}
	bx lr

