	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.comm a, 400000, 4
	.text
	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L16003:
	mov r4, #4
	ldr r5, addr_a0
	ldr r6, =80000
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, =19999
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	ldr r5, =1
	str r5, [r4]
	ldr r4, =2
	ldr r5, =2
	mul r6, r4, r5
	ldr r4, addr_a0
	ldr r7, =80000
	mul r5, r6, r7
	add r6, r4, r5
	mov r4, r6
	ldr r5, =20000
	sub r6, r5, #1
	mov r5, #4
	mul r7, r6, r5
	add r5, r4, r7
	ldr r4, [r5]
	ldr r5, =2
	ldr r6, =2
	mul r7, r5, r6
	ldr r5, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r5, r8
	mov r5, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r5, r8
	ldr r5, [r6]
	add r6, r4, r5
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a0
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a0
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B0
.LTORG
addr_a0:
	.word a
.B0:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a1
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a1
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	b .B1
.LTORG
addr_a1:
	.word a
.B1:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a2
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a2
	b .B2
.LTORG
addr_a2:
	.word a
.B2:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a3
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a3
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B3
.LTORG
addr_a3:
	.word a
.B3:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a4
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a4
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B4
.LTORG
addr_a4:
	.word a
.B4:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a5
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a5
	b .B5
.LTORG
addr_a5:
	.word a
.B5:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a6
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a6
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B6
.LTORG
addr_a6:
	.word a
.B6:
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a7
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a7
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B7
.LTORG
addr_a7:
	.word a
.B7:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a8
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a8
	b .B8
.LTORG
addr_a8:
	.word a
.B8:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a9
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a9
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B9
.LTORG
addr_a9:
	.word a
.B9:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a10
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a10
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B10
.LTORG
addr_a10:
	.word a
.B10:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a11
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a11
	b .B11
.LTORG
addr_a11:
	.word a
.B11:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a12
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a12
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B12
.LTORG
addr_a12:
	.word a
.B12:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a13
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a13
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B13
.LTORG
addr_a13:
	.word a
.B13:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a14
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a14
	b .B14
.LTORG
addr_a14:
	.word a
.B14:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a15
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a15
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B15
.LTORG
addr_a15:
	.word a
.B15:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a16
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a16
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B16
.LTORG
addr_a16:
	.word a
.B16:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a17
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a17
	b .B17
.LTORG
addr_a17:
	.word a
.B17:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a18
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a18
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B18
.LTORG
addr_a18:
	.word a
.B18:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a19
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a19
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B19
.LTORG
addr_a19:
	.word a
.B19:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a20
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a20
	b .B20
.LTORG
addr_a20:
	.word a
.B20:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a21
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a21
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B21
.LTORG
addr_a21:
	.word a
.B21:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a22
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a22
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B22
.LTORG
addr_a22:
	.word a
.B22:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a23
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a23
	b .B23
.LTORG
addr_a23:
	.word a
.B23:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a24
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a24
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B24
.LTORG
addr_a24:
	.word a
.B24:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a25
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a25
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B25
.LTORG
addr_a25:
	.word a
.B25:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a26
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a26
	b .B26
.LTORG
addr_a26:
	.word a
.B26:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a27
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a27
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B27
.LTORG
addr_a27:
	.word a
.B27:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a28
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a28
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B28
.LTORG
addr_a28:
	.word a
.B28:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a29
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a29
	b .B29
.LTORG
addr_a29:
	.word a
.B29:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a30
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a30
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B30
.LTORG
addr_a30:
	.word a
.B30:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a31
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a31
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B31
.LTORG
addr_a31:
	.word a
.B31:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a32
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a32
	b .B32
.LTORG
addr_a32:
	.word a
.B32:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a33
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a33
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B33
.LTORG
addr_a33:
	.word a
.B33:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a34
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a34
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B34
.LTORG
addr_a34:
	.word a
.B34:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a35
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a35
	b .B35
.LTORG
addr_a35:
	.word a
.B35:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a36
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a36
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B36
.LTORG
addr_a36:
	.word a
.B36:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a37
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a37
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B37
.LTORG
addr_a37:
	.word a
.B37:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a38
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a38
	b .B38
.LTORG
addr_a38:
	.word a
.B38:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a39
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a39
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B39
.LTORG
addr_a39:
	.word a
.B39:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a40
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a40
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B40
.LTORG
addr_a40:
	.word a
.B40:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a41
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a41
	b .B41
.LTORG
addr_a41:
	.word a
.B41:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a42
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a42
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B42
.LTORG
addr_a42:
	.word a
.B42:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a43
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a43
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B43
.LTORG
addr_a43:
	.word a
.B43:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a44
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a44
	b .B44
.LTORG
addr_a44:
	.word a
.B44:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a45
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a45
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B45
.LTORG
addr_a45:
	.word a
.B45:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a46
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a46
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B46
.LTORG
addr_a46:
	.word a
.B46:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a47
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a47
	b .B47
.LTORG
addr_a47:
	.word a
.B47:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a48
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a48
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B48
.LTORG
addr_a48:
	.word a
.B48:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a49
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a49
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B49
.LTORG
addr_a49:
	.word a
.B49:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a50
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a50
	b .B50
.LTORG
addr_a50:
	.word a
.B50:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a51
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a51
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B51
.LTORG
addr_a51:
	.word a
.B51:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a52
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a52
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B52
.LTORG
addr_a52:
	.word a
.B52:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a53
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a53
	b .B53
.LTORG
addr_a53:
	.word a
.B53:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a54
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a54
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B54
.LTORG
addr_a54:
	.word a
.B54:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a55
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a55
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B55
.LTORG
addr_a55:
	.word a
.B55:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a56
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a56
	b .B56
.LTORG
addr_a56:
	.word a
.B56:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a57
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a57
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B57
.LTORG
addr_a57:
	.word a
.B57:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a58
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a58
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	b .B58
.LTORG
addr_a58:
	.word a
.B58:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a59
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a59
	b .B59
.LTORG
addr_a59:
	.word a
.B59:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a60
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a60
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B60
.LTORG
addr_a60:
	.word a
.B60:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a61
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a61
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	b .B61
.LTORG
addr_a61:
	.word a
.B61:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a62
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a62
	b .B62
.LTORG
addr_a62:
	.word a
.B62:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a63
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a63
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B63
.LTORG
addr_a63:
	.word a
.B63:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a64
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a64
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B64
.LTORG
addr_a64:
	.word a
.B64:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a65
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a65
	b .B65
.LTORG
addr_a65:
	.word a
.B65:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a66
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a66
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B66
.LTORG
addr_a66:
	.word a
.B66:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a67
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a67
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B67
.LTORG
addr_a67:
	.word a
.B67:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a68
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a68
	b .B68
.LTORG
addr_a68:
	.word a
.B68:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a69
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a69
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B69
.LTORG
addr_a69:
	.word a
.B69:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a70
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a70
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B70
.LTORG
addr_a70:
	.word a
.B70:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a71
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a71
	b .B71
.LTORG
addr_a71:
	.word a
.B71:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a72
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a72
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B72
.LTORG
addr_a72:
	.word a
.B72:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a73
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a73
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B73
.LTORG
addr_a73:
	.word a
.B73:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a74
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a74
	b .B74
.LTORG
addr_a74:
	.word a
.B74:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a75
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a75
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B75
.LTORG
addr_a75:
	.word a
.B75:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a76
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a76
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B76
.LTORG
addr_a76:
	.word a
.B76:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a77
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a77
	b .B77
.LTORG
addr_a77:
	.word a
.B77:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a78
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a78
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B78
.LTORG
addr_a78:
	.word a
.B78:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a79
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a79
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B79
.LTORG
addr_a79:
	.word a
.B79:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a80
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a80
	b .B80
.LTORG
addr_a80:
	.word a
.B80:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a81
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a81
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B81
.LTORG
addr_a81:
	.word a
.B81:
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a82
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a82
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B82
.LTORG
addr_a82:
	.word a
.B82:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a83
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a83
	b .B83
.LTORG
addr_a83:
	.word a
.B83:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a84
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a84
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B84
.LTORG
addr_a84:
	.word a
.B84:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a85
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a85
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B85
.LTORG
addr_a85:
	.word a
.B85:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a86
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a86
	b .B86
.LTORG
addr_a86:
	.word a
.B86:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a87
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a87
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B87
.LTORG
addr_a87:
	.word a
.B87:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a88
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a88
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B88
.LTORG
addr_a88:
	.word a
.B88:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a89
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a89
	b .B89
.LTORG
addr_a89:
	.word a
.B89:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a90
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a90
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B90
.LTORG
addr_a90:
	.word a
.B90:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a91
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a91
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	b .B91
.LTORG
addr_a91:
	.word a
.B91:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a92
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a92
	b .B92
.LTORG
addr_a92:
	.word a
.B92:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a93
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a93
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B93
.LTORG
addr_a93:
	.word a
.B93:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a94
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a94
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B94
.LTORG
addr_a94:
	.word a
.B94:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a95
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a95
	b .B95
.LTORG
addr_a95:
	.word a
.B95:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a96
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a96
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B96
.LTORG
addr_a96:
	.word a
.B96:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a97
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a97
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B97
.LTORG
addr_a97:
	.word a
.B97:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a98
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a98
	b .B98
.LTORG
addr_a98:
	.word a
.B98:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a99
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a99
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B99
.LTORG
addr_a99:
	.word a
.B99:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a100
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a100
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B100
.LTORG
addr_a100:
	.word a
.B100:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a101
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a101
	b .B101
.LTORG
addr_a101:
	.word a
.B101:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a102
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a102
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B102
.LTORG
addr_a102:
	.word a
.B102:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a103
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a103
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B103
.LTORG
addr_a103:
	.word a
.B103:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a104
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a104
	b .B104
.LTORG
addr_a104:
	.word a
.B104:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a105
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a105
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B105
.LTORG
addr_a105:
	.word a
.B105:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a106
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a106
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	b .B106
.LTORG
addr_a106:
	.word a
.B106:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a107
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a107
	b .B107
.LTORG
addr_a107:
	.word a
.B107:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a108
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a108
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B108
.LTORG
addr_a108:
	.word a
.B108:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a109
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a109
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B109
.LTORG
addr_a109:
	.word a
.B109:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a110
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a110
	b .B110
.LTORG
addr_a110:
	.word a
.B110:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a111
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a111
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B111
.LTORG
addr_a111:
	.word a
.B111:
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a112
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a112
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B112
.LTORG
addr_a112:
	.word a
.B112:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a113
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a113
	b .B113
.LTORG
addr_a113:
	.word a
.B113:
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a114
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a114
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	b .B114
.LTORG
addr_a114:
	.word a
.B114:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a115
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a115
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B115
.LTORG
addr_a115:
	.word a
.B115:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a116
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a116
	b .B116
.LTORG
addr_a116:
	.word a
.B116:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a117
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a117
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	b .B117
.LTORG
addr_a117:
	.word a
.B117:
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r8, #4
	mul r5, r7, r8
	add r7, r4, r5
	ldr r4, [r7]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r8, #4
	mul r6, r7, r8
	add r7, r4, r6
	ldr r4, [r7]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a118
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a118
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	b .B118
.LTORG
addr_a118:
	.word a
.B118:
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r8, =80000
	mul r5, r7, r8
	add r7, r4, r5
	mov r4, r7
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r8, =80000
	mul r6, r7, r8
	add r7, r4, r6
	mov r4, r7
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	ldr r4, =2
	ldr r5, =2
	mul r7, r4, r5
	ldr r4, addr_a119
	ldr r5, =80000
	mul r8, r7, r5
	add r5, r4, r8
	mov r4, r5
	ldr r5, =20000
	sub r7, r5, #1
	mov r5, #4
	mul r8, r7, r5
	add r5, r4, r8
	ldr r4, [r5]
	add r5, r6, r4
	ldr r4, =2
	ldr r6, =2
	mul r7, r4, r6
	ldr r4, addr_a119
	b .B119
.LTORG
addr_a119:
	.word a
.B119:
	ldr r6, =80000
	mul r8, r7, r6
	add r6, r4, r8
	mov r4, r6
	ldr r6, =20000
	sub r7, r6, #1
	mov r6, #4
	mul r8, r7, r6
	add r6, r4, r8
	ldr r4, [r6]
	add r6, r5, r4
	str r6, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .F120
.LTORG
addr_a120:
	.word a
.F120:

addr_a121:
	.word a
