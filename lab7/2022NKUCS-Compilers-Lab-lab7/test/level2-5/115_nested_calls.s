	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global func1
	.type func1 , %function
func1:
	push {r4, r5, r6, r7, fp, lr}
	mov fp, sp
	sub sp, sp, #12
.L192:
	str r0, [fp, #-12]
	str r1, [fp, #-8]
	str r2, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	beq .L196
	b .L201
.L196:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	mul r6, r4, r5
	mov r0, r6
	add sp, sp, #12
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
	b .L198
.L197:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	ldr r6, [fp, #-4]
	sub r7, r5, r6
	mov r0, r4
	mov r1, r7
	mov r2, #0
	bl func1
	mov r4, r0
	mov r0, r4
	add sp, sp, #12
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
	b .L198
.L198:
	mov r0, #0
	add sp, sp, #12
	pop {r4, r5, r6, r7, fp, lr}
	bx lr
.L201:
	b .L197

	.global func2
	.type func2 , %function
func2:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L202:
	str r0, [fp, #-8]
	str r1, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	bne .L205
	b .L209
.L205:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	mov r0, r6
	mov r1, #0
	bl func2
	mov r4, r0
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
	b .L207
.L206:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
	b .L207
.L207:
	mov r0, #0
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
.L209:
	b .L206

	.global func3
	.type func3 , %function
func3:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L211:
	str r0, [fp, #-8]
	str r1, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	beq .L214
	b .L219
.L214:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	mov r0, r5
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
	b .L216
.L215:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	add r6, r4, r5
	mov r0, r6
	mov r1, #0
	bl func3
	mov r4, r0
	mov r0, r4
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
	b .L216
.L216:
	mov r0, #0
	add sp, sp, #8
	pop {r4, r5, r6, fp, lr}
	bx lr
.L219:
	b .L215

	.global func4
	.type func4 , %function
func4:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #12
.L220:
	str r0, [fp, #-12]
	str r1, [fp, #-8]
	str r2, [fp, #-4]
	ldr r4, [fp, #-12]
	cmp r4, #0
	bne .L224
	b .L228
.L224:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #12
	pop {r4, fp, lr}
	bx lr
	b .L226
.L225:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #12
	pop {r4, fp, lr}
	bx lr
	b .L226
.L226:
	mov r0, #0
	add sp, sp, #12
	pop {r4, fp, lr}
	bx lr
.L228:
	b .L225

	.global func5
	.type func5 , %function
func5:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L230:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, =0
	sub r6, r5, r4
	mov r0, r6
	add sp, sp, #4
	pop {r4, r5, r6, fp, lr}
	bx lr

	.global func6
	.type func6 , %function
func6:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L232:
	str r0, [fp, #-8]
	str r1, [fp, #-4]
	ldr r4, [fp, #-8]
	cmp r4, #0
	bne .L238
	b .L240
.L235:
	mov r0, #1
	add sp, sp, #8
	pop {r4, fp, lr}
	bx lr
	b .L237
.L236:
	mov r0, #0
	add sp, sp, #8
	pop {r4, fp, lr}
	bx lr
	b .L237
.L237:
	mov r0, #0
	add sp, sp, #8
	pop {r4, fp, lr}
	bx lr
.L238:
	ldr r4, [fp, #-4]
	cmp r4, #0
	bne .L235
	b .L243
.L240:
	b .L236
.L243:
	b .L236

	.global func7
	.type func7 , %function
func7:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L245:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	moveq r4, #1
	movne r4, #0
	cmp r4, #0
	bne .L247
	b .L252
.L247:
	mov r0, #1
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
	b .L249
.L248:
	mov r0, #0
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
	b .L249
.L249:
	mov r0, #0
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
.L252:
	b .L248

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	mov fp, sp
	sub sp, sp, #76
.L254:
	bl getint
	mov r4, r0
	str r4, [fp, #-64]
	bl getint
	mov r4, r0
	str r4, [fp, #-60]
	bl getint
	mov r4, r0
	str r4, [fp, #-56]
	bl getint
	mov r4, r0
	str r4, [fp, #-52]
	ldr r4, =0
	str r4, [fp, #-8]
	b .L262
.L261:
	bl getint
	mov r4, r0
	ldr r5, [fp, #-8]
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	str r4, [r6]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L262
.L262:
	ldr r4, [fp, #-8]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L261
	b .L266
.L263:
	ldr r4, [fp, #-64]
	mov r0, r4
	bl func7
	mov r4, r0
	ldr r5, [fp, #-60]
	mov r0, r5
	bl func5
	mov r5, r0
	mov r0, r4
	mov r1, r5
	bl func6
	mov r4, r0
	ldr r5, [fp, #-56]
	mov r0, r4
	mov r1, r5
	bl func2
	mov r4, r0
	ldr r5, [fp, #-52]
	mov r0, r4
	mov r1, r5
	bl func3
	mov r4, r0
	mov r0, r4
	bl func5
	mov r4, r0
	str r4, [fp, #-72]
	mov r4, #0
	mov r5, #-48
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	str r4, [fp, #-68]
	mov r4, #1
	mov r5, #-48
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	mov r0, r4
	bl func5
	mov r4, r0
	mov r5, #2
	mov r6, #-48
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	add r6, fp, r5
	ldr r5, [r6]
	mov r6, #3
	mov r7, #-48
	mov r8, #4
	mul r9, r6, r8
	add r6, r7, r9
	add r7, fp, r6
	ldr r6, [r7]
	mov r0, r6
	bl func7
	mov r6, r0
	mov r0, r5
	mov r1, r6
	bl func6
	mov r5, r0
	mov r6, #4
	mov r7, #-48
	mov r9, #4
	mul r8, r6, r9
	add r6, r7, r8
	add r7, fp, r6
	ldr r6, [r7]
	mov r7, #5
	mov r8, #-48
	mov r9, #4
	mul r10, r7, r9
	add r7, r8, r10
	add r8, fp, r7
	ldr r7, [r8]
	mov r0, r7
	bl func7
	mov r7, r0
	mov r0, r6
	mov r1, r7
	bl func2
	mov r6, r0
	mov r0, r4
	mov r1, r5
	mov r2, r6
	bl func4
	mov r4, r0
	mov r5, #6
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mov r0, r4
	mov r1, r5
	bl func3
	mov r4, r0
	mov r5, #7
	mov r6, #-48
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	add r6, fp, r5
	ldr r5, [r6]
	mov r0, r4
	mov r1, r5
	bl func2
	mov r4, r0
	mov r5, #8
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mov r6, #9
	mov r7, #-48
	mov r8, #4
	mul r9, r6, r8
	add r6, r7, r9
	add r7, fp, r6
	ldr r6, [r7]
	mov r0, r6
	bl func7
	mov r6, r0
	mov r0, r5
	mov r1, r6
	bl func3
	mov r5, r0
	ldr r6, [fp, #-64]
	mov r0, r4
	mov r1, r5
	mov r2, r6
	bl func1
	mov r4, r0
	ldr r5, [fp, #-72]
	mov r0, r5
	ldr r5, [fp, #-68]
	mov r1, r5
	mov r2, r4
	bl func4
	mov r4, r0
	ldr r5, [fp, #-60]
	ldr r6, [fp, #-56]
	mov r0, r6
	bl func7
	mov r6, r0
	ldr r7, [fp, #-52]
	mov r0, r6
	mov r1, r7
	bl func3
	mov r6, r0
	mov r0, r5
	mov r1, r6
	bl func2
	mov r5, r0
	mov r0, r4
	mov r1, r5
	bl func3
	mov r4, r0
	mov r5, #0
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mov r6, #1
	mov r7, #-48
	mov r9, #4
	mul r8, r6, r9
	add r6, r7, r8
	add r7, fp, r6
	ldr r6, [r7]
	mov r0, r4
	mov r1, r5
	mov r2, r6
	bl func1
	mov r4, r0
	mov r5, #2
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mov r0, r4
	mov r1, r5
	bl func2
	mov r4, r0
	mov r5, #3
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	str r5, [fp, #-76]
	mov r5, #4
	mov r6, #-48
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	add r6, fp, r5
	ldr r5, [r6]
	mov r6, #5
	mov r7, #-48
	mov r8, #4
	mul r9, r6, r8
	add r6, r7, r9
	add r7, fp, r6
	ldr r6, [r7]
	mov r0, r6
	bl func5
	mov r6, r0
	mov r0, r5
	mov r1, r6
	bl func3
	mov r5, r0
	mov r6, #6
	mov r7, #-48
	mov r8, #4
	mul r9, r6, r8
	add r6, r7, r9
	add r7, fp, r6
	ldr r6, [r7]
	mov r0, r6
	bl func5
	mov r6, r0
	mov r0, r5
	mov r1, r6
	bl func2
	mov r5, r0
	mov r6, #7
	mov r7, #-48
	mov r8, #4
	mul r9, r6, r8
	add r6, r7, r9
	add r7, fp, r6
	ldr r6, [r7]
	mov r7, #8
	mov r8, #-48
	mov r9, #4
	mul r10, r7, r9
	add r7, r8, r10
	add r8, fp, r7
	ldr r7, [r8]
	mov r0, r7
	bl func7
	mov r7, r0
	mov r0, r5
	mov r1, r6
	mov r2, r7
	bl func1
	mov r5, r0
	mov r6, #9
	mov r7, #-48
	mov r8, #4
	mul r9, r6, r8
	add r6, r7, r9
	add r7, fp, r6
	ldr r6, [r7]
	mov r0, r6
	bl func5
	mov r6, r0
	mov r0, r5
	mov r1, r6
	bl func2
	mov r5, r0
	ldr r6, [fp, #-64]
	mov r0, r5
	mov r1, r6
	bl func3
	mov r5, r0
	mov r0, r4
	ldr r4, [fp, #-76]
	mov r1, r4
	mov r2, r5
	bl func1
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #76
	pop {r4, r5, r6, r7, r8, r9, r10, fp, lr}
	bx lr
	b .F0
.LTORG
.F0:
.L266:
	b .L263

