	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global n
	.align 4
	.size n, 4
n:
	.word 0
	.comm array, 440, 4
	.text
	.global init
	.type init , %function
init:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #8
.L192:
	str r0, [fp, #-8]
	ldr r4, =1
	str r4, [fp, #-4]
	b .L196
.L195:
	ldr r4, =0
	sub r5, r4, #1
	ldr r4, [fp, #-4]
	ldr r6, addr_array0
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	str r5, [r6]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	b .L196
.L196:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	ldr r6, [fp, #-8]
	mul r7, r5, r6
	add r5, r7, #1
	cmp r4, r5
	movle r4, #1
	movgt r4, #0
	ble .L195
	b .L200
.L197:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L198:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L199:
	add sp, sp, #8
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L200:
	b .L197

	.global findfa
	.type findfa , %function
findfa:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L201:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, addr_array0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	ldr r5, [fp, #-4]
	cmp r4, r5
	beq .L203
	b .L209
.L203:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .L205
.L204:
	ldr r4, [fp, #-4]
	ldr r5, addr_array0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	mov r0, r4
	bl findfa
	mov r4, r0
	ldr r5, [fp, #-4]
	ldr r6, addr_array0
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	str r4, [r6]
	ldr r4, [fp, #-4]
	ldr r5, addr_array0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	mov r0, r4
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .L205
.L205:
	mov r0, #0
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L209:
	b .L204

	.global mmerge
	.type mmerge , %function
mmerge:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #16
.L212:
	str r0, [fp, #-16]
	str r1, [fp, #-12]
	ldr r4, [fp, #-16]
	mov r0, r4
	bl findfa
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-12]
	mov r0, r4
	bl findfa
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	cmp r4, r5
	bne .L217
	b .L221
.L217:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	ldr r6, addr_array0
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	str r4, [r6]
	b .L218
.L218:
	add sp, sp, #16
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L219:
	add sp, sp, #16
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L220:
	add sp, sp, #16
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L221:
	b .L218

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #36
.L222:
	ldr r4, =1
	str r4, [fp, #-36]
	b .L228
.L227:
	ldr r4, [fp, #-36]
	sub r5, r4, #1
	str r5, [fp, #-36]
	ldr r4, =4
	ldr r5, addr_n0
	str r4, [r5]
	ldr r4, =10
	str r4, [fp, #-32]
	ldr r4, =0
	str r4, [fp, #-20]
	ldr r4, =0
	str r4, [fp, #-16]
	ldr r4, addr_n0
	ldr r5, [r4]
	mov r0, r5
	bl init
	ldr r4, addr_n0
	ldr r5, [r4]
	ldr r4, addr_n0
	ldr r6, [r4]
	mul r4, r5, r6
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L237
.L228:
	ldr r4, [fp, #-36]
	cmp r4, #0
	bne .L227
	b .L231
.L229:
	mov r0, #0
	add sp, sp, #36
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L231:
	b .L229
.L236:
	bl getint
	mov r4, r0
	str r4, [fp, #-28]
	bl getint
	mov r4, r0
	str r4, [fp, #-24]
	ldr r4, [fp, #-16]
	cmp r4, #0
	moveq r4, #1
	movne r4, #0
	cmp r4, #0
	bne .L242
	b .L246
.L237:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-32]
	cmp r4, r5
	movlt r4, #1
	movge r4, #0
	blt .L236
	b .L241
.L238:
	ldr r4, [fp, #-16]
	cmp r4, #0
	moveq r4, #1
	movne r4, #0
	cmp r4, #0
	bne .L315
	b .L319
.L241:
	b .L238
.L242:
	ldr r4, addr_n0
	ldr r5, [r4]
	ldr r4, [fp, #-28]
	sub r6, r4, #1
	mul r4, r5, r6
	ldr r5, [fp, #-24]
	add r6, r4, r5
	str r6, [fp, #-8]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	ldr r6, addr_array0
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	str r4, [r6]
	ldr r4, [fp, #-28]
	cmp r4, #1
	beq .L249
	b .L253
.L243:
	ldr r4, [fp, #-20]
	add r5, r4, #1
	str r5, [fp, #-20]
	b .L237
.L246:
	b .L243
.L249:
	mov r4, #0
	ldr r5, addr_array0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, =0
	str r4, [r5]
	ldr r4, [fp, #-8]
	mov r0, r4
	mov r1, #0
	bl mmerge
	b .L250
.L250:
	ldr r4, [fp, #-28]
	ldr r5, addr_n0
	ldr r6, [r5]
	cmp r4, r6
	beq .L254
	b .L258
.L253:
	b .L250
.L254:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-12]
	ldr r6, addr_array0
	mov r7, #4
	mul r8, r5, r7
	add r5, r6, r8
	mov r6, r5
	str r4, [r6]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	mov r0, r4
	mov r1, r5
	bl mmerge
	b .L255
.L255:
	ldr r4, [fp, #-24]
	ldr r5, addr_n0
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L261
	b .L264
.L258:
	b .L255
.L259:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	add r6, r5, #1
	mov r0, r4
	mov r1, r6
	bl mmerge
	b .L260
.L260:
	ldr r4, [fp, #-24]
	cmp r4, #1
	movgt r4, #1
	movle r4, #0
	bgt .L271
	b .L274
.L261:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	ldr r4, addr_array0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, =0
	sub r6, r4, #1
	cmp r5, r6
	bne .L259
	b .L268
.L264:
	b .L260
.L268:
	b .L260
.L269:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	sub r6, r5, #1
	mov r0, r4
	mov r1, r6
	bl mmerge
	b .L270
	b .F0
.LTORG
addr_array0:
	.word array
addr_n0:
	.word n
.F0:
.L270:
	ldr r4, [fp, #-28]
	ldr r5, addr_n1
	ldr r6, [r5]
	cmp r4, r6
	movlt r4, #1
	movge r4, #0
	blt .L281
	b .L284
.L271:
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	ldr r4, addr_array1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, =0
	sub r6, r4, #1
	cmp r5, r6
	bne .L269
	b .L278
.L274:
	b .L270
.L278:
	b .L270
.L279:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	ldr r6, addr_n1
	ldr r7, [r6]
	add r6, r5, r7
	mov r0, r4
	mov r1, r6
	bl mmerge
	b .L280
.L280:
	ldr r4, [fp, #-28]
	cmp r4, #1
	movgt r4, #1
	movle r4, #0
	bgt .L291
	b .L294
.L281:
	ldr r4, [fp, #-8]
	ldr r5, addr_n1
	ldr r6, [r5]
	add r5, r4, r6
	ldr r4, addr_array1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, =0
	sub r6, r4, #1
	cmp r5, r6
	bne .L279
	b .L288
.L284:
	b .L280
.L288:
	b .L280
.L289:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-8]
	ldr r6, addr_n1
	ldr r7, [r6]
	sub r6, r5, r7
	mov r0, r4
	mov r1, r6
	bl mmerge
	b .L290
.L290:
	mov r4, #0
	ldr r5, addr_array1
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	ldr r5, =0
	sub r6, r5, #1
	cmp r4, r6
	bne .L302
	b .L306
.L291:
	ldr r4, [fp, #-8]
	ldr r5, addr_n1
	ldr r6, [r5]
	sub r5, r4, r6
	ldr r4, addr_array1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, =0
	sub r6, r4, #1
	cmp r5, r6
	bne .L289
	b .L298
.L294:
	b .L290
.L298:
	b .L290
.L299:
	ldr r4, =1
	str r4, [fp, #-16]
	ldr r4, [fp, #-20]
	add r5, r4, #1
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	mov r0, #10
	bl putch
	b .L300
.L300:
	b .L243
.L301:
	mov r0, #0
	bl findfa
	mov r4, r0
	ldr r5, [fp, #-12]
	mov r0, r5
	bl findfa
	mov r5, r0
	cmp r4, r5
	beq .L299
	b .L313
.L302:
	ldr r4, [fp, #-12]
	ldr r5, addr_array1
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	ldr r5, =0
	sub r6, r5, #1
	cmp r4, r6
	bne .L301
	b .L310
.L306:
	b .L300
.L310:
	b .L300
.L313:
	b .L300
.L315:
	ldr r4, =0
	sub r5, r4, #1
	mov r0, r5
	bl putint
	mov r0, #10
	bl putch
	b .L316
.L316:
	b .L228
.L319:
	b .L316

addr_array1:
	.word array
addr_n1:
	.word n
