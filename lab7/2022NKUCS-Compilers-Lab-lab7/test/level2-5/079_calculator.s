	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global intt
	.align 4
	.size intt, 4
intt:
	.word 0
	.global chat
	.align 4
	.size chat, 4
chat:
	.word 0
	.global i
	.align 4
	.size i, 4
i:
	.word 0
	.global ii
	.align 4
	.size ii, 4
ii:
	.word 1
	.global c
	.align 4
	.size c, 4
c:
	.word 0
	.comm ints, 40000, 4
	.comm chas, 40000, 4
	.comm get, 40000, 4
	.comm get2, 40000, 4
	.text
	.global isdigit
	.type isdigit , %function
isdigit:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L417:
	str r0, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #48
	movge r4, #1
	movlt r4, #0
	bge .L421
	b .L424
.L419:
	mov r0, #1
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
	b .L420
.L420:
	mov r0, #0
	add sp, sp, #4
	pop {r4, fp, lr}
	bx lr
.L421:
	ldr r4, [fp, #-4]
	cmp r4, #57
	movle r4, #1
	movgt r4, #0
	ble .L419
	b .L427
.L424:
	b .L420
.L427:
	b .L420

	.global power
	.type power , %function
power:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #12
.L428:
	str r0, [fp, #-12]
	str r1, [fp, #-8]
	ldr r4, =1
	str r4, [fp, #-4]
	b .L433
.L432:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-12]
	mul r6, r4, r5
	str r6, [fp, #-4]
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	str r5, [fp, #-8]
	b .L433
.L433:
	ldr r4, [fp, #-8]
	cmp r4, #0
	bne .L432
	b .L437
.L434:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #12
	pop {r4, r5, r6, fp, lr}
	bx lr
.L437:
	b .L434

	.global getstr
	.type getstr , %function
getstr:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #12
.L438:
	str r0, [fp, #-12]
	bl getch
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, =0
	str r4, [fp, #-4]
	b .L443
.L442:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	ldr r6, [fp, #-4]
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	str r4, [r6]
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	bl getch
	mov r4, r0
	str r4, [fp, #-8]
	b .L443
.L443:
	ldr r4, [fp, #-8]
	cmp r4, #13
	bne .L445
	b .L448
.L444:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #12
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L445:
	ldr r4, [fp, #-8]
	cmp r4, #10
	bne .L442
	b .L451
.L448:
	b .L444
.L451:
	b .L444

	.global intpush
	.type intpush , %function
intpush:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L453:
	str r0, [fp, #-4]
	ldr r4, addr_intt0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_intt0
	str r4, [r5]
	ldr r4, [fp, #-4]
	ldr r5, addr_intt0
	ldr r6, [r5]
	ldr r5, addr_ints0
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	str r4, [r5]
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr

	.global chapush
	.type chapush , %function
chapush:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L455:
	str r0, [fp, #-4]
	ldr r4, addr_chat0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_chat0
	str r4, [r5]
	ldr r4, [fp, #-4]
	ldr r5, addr_chat0
	ldr r6, [r5]
	ldr r5, addr_chas0
	mov r7, #4
	mul r8, r6, r7
	add r6, r5, r8
	mov r5, r6
	str r4, [r5]
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr

	.global intpop
	.type intpop , %function
intpop:
	push {r4, r5, r6, r7, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L457:
	ldr r4, addr_intt0
	ldr r5, [r4]
	sub r4, r5, #1
	ldr r5, addr_intt0
	str r4, [r5]
	ldr r4, addr_intt0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_ints0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	mov r0, r4
	add sp, sp, #0
	pop {r4, r5, r6, r7, fp, lr}
	bx lr

	.global chapop
	.type chapop , %function
chapop:
	push {r4, r5, r6, r7, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L459:
	ldr r4, addr_chat0
	ldr r5, [r4]
	sub r4, r5, #1
	ldr r5, addr_chat0
	str r4, [r5]
	ldr r4, addr_chat0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_chas0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	mov r0, r4
	add sp, sp, #0
	pop {r4, r5, r6, r7, fp, lr}
	bx lr

	.global intadd
	.type intadd , %function
intadd:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #4
.L461:
	str r0, [fp, #-4]
	ldr r4, addr_intt0
	ldr r5, [r4]
	ldr r4, addr_ints0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, =10
	mul r6, r5, r4
	ldr r4, addr_intt0
	ldr r5, [r4]
	ldr r4, addr_ints0
	mov r7, #4
	mul r8, r5, r7
	add r5, r4, r8
	mov r4, r5
	str r6, [r4]
	ldr r4, addr_intt0
	ldr r5, [r4]
	ldr r4, addr_ints0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, [fp, #-4]
	add r6, r5, r4
	ldr r4, addr_intt0
	ldr r5, [r4]
	ldr r4, addr_ints0
	mov r8, #4
	mul r7, r5, r8
	add r5, r4, r7
	mov r4, r5
	str r6, [r4]
	add sp, sp, #4
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr

	.global find
	.type find , %function
find:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L465:
	bl chapop
	mov r4, r0
	ldr r5, addr_c0
	str r4, [r5]
	ldr r4, addr_ii0
	ldr r5, [r4]
	ldr r4, addr_get20
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, =32
	str r5, [r4]
	ldr r4, addr_c0
	ldr r5, [r4]
	ldr r4, addr_ii0
	ldr r6, [r4]
	add r4, r6, #1
	ldr r6, addr_get20
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	str r5, [r6]
	ldr r4, addr_ii0
	ldr r5, [r4]
	add r4, r5, #2
	ldr r5, addr_ii0
	str r4, [r5]
	ldr r4, addr_chat0
	ldr r5, [r4]
	cmp r5, #0
	beq .L466
	b .L470
.L466:
	mov r0, #0
	add sp, sp, #0
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .L467
.L467:
	mov r0, #1
	add sp, sp, #0
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L470:
	b .L467

	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #20
.L471:
	ldr r4, =0
	ldr r5, addr_intt0
	str r4, [r5]
	ldr r4, =0
	ldr r5, addr_chat0
	str r4, [r5]
	mov r4, #0
	ldr r5, addr_get0
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	mov r0, r5
	bl getstr
	mov r4, r0
	str r4, [fp, #-20]
	b .L474
.L473:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, addr_get0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	mov r0, r5
	bl isdigit
	mov r4, r0
	cmp r4, #1
	beq .L479
	b .L485
.L474:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, [fp, #-20]
	cmp r5, r4
	movlt r4, #1
	movge r4, #0
	blt .L473
	b .L478
.L475:
	b .L702
.L478:
	b .L475
.L479:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, addr_get0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	ldr r4, addr_ii0
	ldr r6, [r4]
	ldr r4, addr_get20
	mov r8, #4
	mul r7, r6, r8
	add r6, r4, r7
	mov r4, r6
	str r5, [r4]
	ldr r4, addr_ii0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_ii0
	str r4, [r5]
	b .L481
.L480:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, addr_get0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #40
	beq .L487
	b .L492
.L481:
	ldr r4, addr_i0
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_i0
	str r4, [r5]
	b .L474
.L485:
	b .L480
.L487:
	mov r0, #40
	bl chapush
	b .L488
.L488:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, addr_get0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L493
	b .L498
.L492:
	b .L488
.L493:
	mov r0, #94
	bl chapush
	b .L494
.L494:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, addr_get0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #41
	beq .L499
	b .L504
.L498:
	b .L494
.L499:
	bl chapop
	mov r4, r0
	ldr r5, addr_c0
	str r4, [r5]
	b .L506
.L500:
	ldr r4, addr_i0
	ldr r5, [r4]
	ldr r4, addr_get0
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #43
	beq .L511
	b .L516
.L504:
	b .L500
.L505:
	ldr r4, addr_ii0
	ldr r5, [r4]
	ldr r4, addr_get20
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, =32
	str r5, [r4]
	ldr r4, addr_c0
	ldr r5, [r4]
	ldr r4, addr_ii0
	ldr r6, [r4]
	add r4, r6, #1
	ldr r6, addr_get20
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	mov r6, r4
	str r5, [r6]
	ldr r4, addr_ii0
	ldr r5, [r4]
	add r4, r5, #2
	ldr r5, addr_ii0
	str r4, [r5]
	bl chapop
	mov r4, r0
	ldr r5, addr_c0
	str r4, [r5]
	b .L506
.L506:
	ldr r4, addr_c0
	ldr r5, [r4]
	cmp r5, #40
	bne .L505
	b .L510
	b .F0
.LTORG
addr_ints0:
	.word ints
addr_intt0:
	.word intt
addr_chas0:
	.word chas
addr_chat0:
	.word chat
addr_i0:
	.word i
addr_ii0:
	.word ii
addr_c0:
	.word c
addr_get0:
	.word get
addr_get20:
	.word get2
.F0:
.L507:
	b .L500
.L510:
	b .L507
.L511:
	b .L518
.L512:
	ldr r4, addr_i1
	ldr r5, [r4]
	ldr r4, addr_get1
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #45
	beq .L555
	b .L560
.L516:
	b .L512
.L517:
	bl find
	mov r4, r0
	cmp r4, #0
	beq .L549
	b .L553
.L518:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #43
	beq .L517
	b .L528
.L519:
	mov r0, #43
	bl chapush
	b .L512
.L520:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L517
	b .L548
.L521:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L517
	b .L544
.L522:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L517
	b .L540
.L523:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L517
	b .L536
.L524:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #45
	beq .L517
	b .L532
.L528:
	b .L524
.L532:
	b .L523
.L536:
	b .L522
.L540:
	b .L521
.L544:
	b .L520
.L548:
	b .L519
.L549:
	b .L519
.L550:
	b .L518
.L553:
	b .L550
.L554:
	b .L550
.L555:
	b .L562
.L556:
	ldr r4, addr_i1
	ldr r5, [r4]
	ldr r4, addr_get1
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L599
	b .L604
.L560:
	b .L556
.L561:
	bl find
	mov r4, r0
	cmp r4, #0
	beq .L593
	b .L597
.L562:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #43
	beq .L561
	b .L572
.L563:
	mov r0, #45
	bl chapush
	b .L556
.L564:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L561
	b .L592
.L565:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L561
	b .L588
.L566:
	ldr r4, addr_chat1
	ldr r5, [r4]
	ldr r4, addr_chas1
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L561
	b .L584
	b .F1
.LTORG
addr_ints1:
	.word ints
addr_intt1:
	.word intt
addr_chas1:
	.word chas
addr_chat1:
	.word chat
addr_i1:
	.word i
addr_ii1:
	.word ii
addr_c1:
	.word c
addr_get1:
	.word get
addr_get21:
	.word get2
.F1:
.L567:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L561
	b .L580
.L568:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #45
	beq .L561
	b .L576
.L572:
	b .L568
.L576:
	b .L567
.L580:
	b .L566
.L584:
	b .L565
.L588:
	b .L564
.L592:
	b .L563
.L593:
	b .L563
.L594:
	b .L562
.L597:
	b .L594
.L598:
	b .L594
.L599:
	b .L606
.L600:
	ldr r4, addr_i2
	ldr r5, [r4]
	ldr r4, addr_get2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L633
	b .L638
.L604:
	b .L600
.L605:
	bl find
	mov r4, r0
	cmp r4, #0
	beq .L627
	b .L631
.L606:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L605
	b .L614
.L607:
	mov r0, #42
	bl chapush
	b .L600
.L608:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L605
	b .L626
.L609:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L605
	b .L622
.L610:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L605
	b .L618
.L614:
	b .L610
.L618:
	b .L609
.L622:
	b .L608
.L626:
	b .L607
.L627:
	b .L607
.L628:
	b .L606
.L631:
	b .L628
.L632:
	b .L628
.L633:
	b .L640
.L634:
	ldr r4, addr_i2
	ldr r5, [r4]
	ldr r4, addr_get2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L667
	b .L672
.L638:
	b .L634
.L639:
	bl find
	mov r4, r0
	cmp r4, #0
	beq .L661
	b .L665
.L640:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L639
	b .L648
.L641:
	mov r0, #47
	bl chapush
	b .L634
.L642:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L639
	b .L660
.L643:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L639
	b .L656
.L644:
	ldr r4, addr_chat2
	ldr r5, [r4]
	ldr r4, addr_chas2
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L639
	b .L652
	b .F2
.LTORG
addr_ints2:
	.word ints
addr_intt2:
	.word intt
addr_chas2:
	.word chas
addr_chat2:
	.word chat
addr_i2:
	.word i
addr_ii2:
	.word ii
addr_c2:
	.word c
addr_get2:
	.word get
addr_get22:
	.word get2
.F2:
.L648:
	b .L644
.L652:
	b .L643
.L656:
	b .L642
.L660:
	b .L641
.L661:
	b .L641
.L662:
	b .L640
.L665:
	b .L662
.L666:
	b .L662
.L667:
	b .L674
.L668:
	ldr r4, addr_ii3
	ldr r5, [r4]
	ldr r4, addr_get23
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, =32
	str r5, [r4]
	ldr r4, addr_ii3
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_ii3
	str r4, [r5]
	b .L481
.L672:
	b .L668
.L673:
	bl find
	mov r4, r0
	cmp r4, #0
	beq .L695
	b .L699
.L674:
	ldr r4, addr_chat3
	ldr r5, [r4]
	ldr r4, addr_chas3
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L673
	b .L682
.L675:
	mov r0, #37
	bl chapush
	b .L668
.L676:
	ldr r4, addr_chat3
	ldr r5, [r4]
	ldr r4, addr_chas3
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L673
	b .L694
.L677:
	ldr r4, addr_chat3
	ldr r5, [r4]
	ldr r4, addr_chas3
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L673
	b .L690
.L678:
	ldr r4, addr_chat3
	ldr r5, [r4]
	ldr r4, addr_chas3
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L673
	b .L686
.L682:
	b .L678
.L686:
	b .L677
.L690:
	b .L676
.L694:
	b .L675
.L695:
	b .L675
.L696:
	b .L674
.L699:
	b .L696
.L700:
	b .L696
.L701:
	bl chapop
	mov r4, r0
	str r4, [fp, #-16]
	ldr r4, addr_ii3
	ldr r5, [r4]
	ldr r4, addr_get23
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, =32
	str r5, [r4]
	ldr r4, [fp, #-16]
	ldr r5, addr_ii3
	ldr r6, [r5]
	add r5, r6, #1
	ldr r6, addr_get23
	mov r8, #4
	mul r7, r5, r8
	add r5, r6, r7
	mov r6, r5
	str r4, [r6]
	ldr r4, addr_ii3
	ldr r5, [r4]
	add r4, r5, #2
	ldr r5, addr_ii3
	str r4, [r5]
	b .L702
.L702:
	ldr r4, addr_chat3
	ldr r5, [r4]
	cmp r5, #0
	movgt r4, #1
	movle r4, #0
	bgt .L701
	b .L706
.L703:
	ldr r4, addr_ii3
	ldr r5, [r4]
	ldr r4, addr_get23
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, =64
	str r5, [r4]
	ldr r4, =1
	ldr r5, addr_i3
	str r4, [r5]
	b .L709
.L706:
	b .L703
.L708:
	ldr r4, addr_i3
	ldr r5, [r4]
	ldr r4, addr_get23
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #43
	beq .L715
	b .L726
.L709:
	ldr r4, addr_i3
	ldr r5, [r4]
	ldr r4, addr_get23
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #64
	bne .L708
	b .L714
.L710:
	mov r4, #1
	ldr r5, addr_ints3
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	mov r0, r4
	bl putint
	mov r0, #0
	add sp, sp, #20
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
	b .F3
.LTORG
addr_ints3:
	.word ints
addr_intt3:
	.word intt
addr_chas3:
	.word chas
addr_chat3:
	.word chat
addr_i3:
	.word i
addr_ii3:
	.word ii
addr_c3:
	.word c
addr_get3:
	.word get
addr_get23:
	.word get2
.F3:
.L714:
	b .L710
.L715:
	bl intpop
	mov r4, r0
	str r4, [fp, #-12]
	bl intpop
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #43
	beq .L750
	b .L755
.L716:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #32
	bne .L786
	b .L791
.L717:
	ldr r4, addr_i4
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_i4
	str r4, [r5]
	b .L709
.L718:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L715
	b .L746
.L719:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L715
	b .L742
.L720:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r7, #4
	mul r6, r5, r7
	add r5, r4, r6
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L715
	b .L738
.L721:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L715
	b .L734
.L722:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #45
	beq .L715
	b .L730
.L726:
	b .L722
.L730:
	b .L721
.L734:
	b .L720
.L738:
	b .L719
.L742:
	b .L718
.L746:
	b .L716
.L750:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	add r6, r4, r5
	str r6, [fp, #-4]
	b .L751
.L751:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #45
	beq .L756
	b .L761
.L755:
	b .L751
.L756:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	sub r6, r4, r5
	str r6, [fp, #-4]
	b .L757
.L757:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #42
	beq .L762
	b .L767
.L761:
	b .L757
.L762:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	mul r6, r4, r5
	str r6, [fp, #-4]
	b .L763
.L763:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #47
	beq .L768
	b .L773
.L767:
	b .L763
.L768:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	sdiv r6, r4, r5
	str r6, [fp, #-4]
	b .L769
.L769:
	ldr r4, addr_i4
	ldr r5, [r4]
	ldr r4, addr_get24
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #37
	beq .L774
	b .L779
	b .F4
.LTORG
addr_ints4:
	.word ints
addr_intt4:
	.word intt
addr_chas4:
	.word chas
addr_chat4:
	.word chat
addr_i4:
	.word i
addr_ii4:
	.word ii
addr_c4:
	.word c
addr_get4:
	.word get
addr_get24:
	.word get2
.F4:
.L773:
	b .L769
.L774:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	sdiv r6, r4, r5
	mul r5, r6, r5
	sub r6, r4, r5
	str r6, [fp, #-4]
	b .L775
.L775:
	ldr r4, addr_i5
	ldr r5, [r4]
	ldr r4, addr_get25
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	cmp r5, #94
	beq .L780
	b .L785
.L779:
	b .L775
.L780:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	mov r0, r4
	mov r1, r5
	bl power
	mov r4, r0
	str r4, [fp, #-4]
	b .L781
.L781:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl intpush
	b .L717
.L785:
	b .L781
.L786:
	ldr r4, addr_i5
	ldr r5, [r4]
	ldr r4, addr_get25
	mov r6, #4
	mul r7, r5, r6
	add r5, r4, r7
	mov r4, r5
	ldr r5, [r4]
	sub r4, r5, #48
	mov r0, r4
	bl intpush
	ldr r4, =1
	ldr r5, addr_ii5
	str r4, [r5]
	b .L794
.L787:
	b .L717
.L791:
	b .L787
.L793:
	ldr r4, addr_i5
	ldr r5, [r4]
	ldr r4, addr_ii5
	ldr r6, [r4]
	add r4, r5, r6
	ldr r5, addr_get25
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	sub r5, r4, #48
	mov r0, r5
	bl intadd
	ldr r4, addr_ii5
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_ii5
	str r4, [r5]
	b .L794
.L794:
	ldr r4, addr_i5
	ldr r5, [r4]
	ldr r4, addr_ii5
	ldr r6, [r4]
	add r4, r5, r6
	ldr r5, addr_get25
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	mov r5, r4
	ldr r4, [r5]
	cmp r4, #32
	bne .L793
	b .L799
.L795:
	ldr r4, addr_i5
	ldr r5, [r4]
	ldr r4, addr_ii5
	ldr r6, [r4]
	add r4, r5, r6
	sub r5, r4, #1
	ldr r4, addr_i5
	str r5, [r4]
	b .L787
.L799:
	b .L795

addr_ints5:
	.word ints
addr_intt5:
	.word intt
addr_chas5:
	.word chas
addr_chat5:
	.word chat
addr_i5:
	.word i
addr_ii5:
	.word ii
addr_c5:
	.word c
addr_get5:
	.word get
addr_get25:
	.word get2
