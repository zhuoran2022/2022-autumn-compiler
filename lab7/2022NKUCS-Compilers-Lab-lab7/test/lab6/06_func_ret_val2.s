	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global func
	.type func , %function
func:
	push {r4, r5, r6, fp, lr}
	mov fp, sp
	sub sp, sp, #12
.L10:
	str r0, [fp, #-12]
	str r1, [fp, #-8]
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	add r6, r4, r5
	str r6, [fp, #-4]

	.global main
	.type main , %function
main:
	push {r4, fp, lr}
	mov fp, sp
	sub sp, sp, #0
.L14:
	mov r0, #1
	mov r1, #2
	bl func
	mov r4, r0
	mov r0, #0
	add sp, sp, #0
	pop {r4, fp, lr}
	bx lr

