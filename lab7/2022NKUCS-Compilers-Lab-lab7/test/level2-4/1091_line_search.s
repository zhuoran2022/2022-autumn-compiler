	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global main
	.type main , %function
main:
	push {r4, r5, r6, r7, r8, fp, lr}
	mov fp, sp
	sub sp, sp, #76
.L62:
	ldr r4, =0
	str r4, [fp, #-72]
	ldr r4, =0
	str r4, [fp, #-76]
	b .L67
.L66:
	ldr r4, [fp, #-76]
	add r5, r4, #1
	ldr r4, [fp, #-76]
	mov r6, #-68
	mov r7, #4
	mul r8, r4, r7
	add r4, r6, r8
	add r6, fp, r4
	str r5, [r6]
	ldr r4, [fp, #-76]
	add r5, r4, #1
	str r5, [fp, #-76]
	b .L67
.L67:
	ldr r4, [fp, #-76]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L66
	b .L71
.L68:
	ldr r4, =10
	str r4, [fp, #-12]
	bl getint
	mov r4, r0
	str r4, [fp, #-28]
	ldr r4, [fp, #-12]
	sub r5, r4, #1
	str r5, [fp, #-24]
	ldr r4, =0
	str r4, [fp, #-20]
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-20]
	add r6, r4, r5
	ldr r4, =2
	sdiv r5, r6, r4
	str r5, [fp, #-16]
	ldr r4, =0
	str r4, [fp, #-8]
	ldr r4, =0
	str r4, [fp, #-76]
	ldr r4, =0
	str r4, [fp, #-4]
	b .L80
.L71:
	b .L68
.L79:
	ldr r4, [fp, #-76]
	mov r5, #-68
	mov r6, #4
	mul r7, r4, r6
	add r4, r5, r7
	add r5, fp, r4
	ldr r4, [r5]
	ldr r5, [fp, #-28]
	cmp r4, r5
	beq .L89
	b .L94
.L80:
	ldr r4, [fp, #-76]
	cmp r4, #10
	movlt r4, #1
	movge r4, #0
	blt .L82
	b .L85
.L81:
	ldr r4, [fp, #-8]
	cmp r4, #1
	beq .L95
	b .L100
.L82:
	ldr r4, [fp, #-8]
	cmp r4, #0
	beq .L79
	b .L88
.L85:
	b .L81
.L88:
	b .L81
.L89:
	ldr r4, =1
	str r4, [fp, #-8]
	ldr r4, [fp, #-76]
	str r4, [fp, #-4]
	b .L90
.L90:
	ldr r4, [fp, #-76]
	add r5, r4, #1
	str r5, [fp, #-76]
	b .L80
.L94:
	b .L90
.L95:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	b .L97
.L96:
	ldr r4, =0
	str r4, [fp, #-28]
	ldr r4, [fp, #-28]
	mov r0, r4
	bl putint
	b .L97
.L97:
	ldr r4, =10
	str r4, [fp, #-28]
	ldr r4, [fp, #-28]
	mov r0, r4
	bl putch
	mov r0, #0
	add sp, sp, #76
	pop {r4, r5, r6, r7, r8, fp, lr}
	bx lr
.L100:
	b .L96

