#include <string.h>
#include <unistd.h>
#include <iostream>
#include "Ast.h"
#include "LinearScan.h"
#include "MachineCode.h"
#include "Unit.h"
using namespace std;

Ast ast;
Unit unit;
MachineUnit mUnit;
extern FILE* yyin;
extern FILE* yyout;

int yyparse();

char outfile[256] = "a.out";
bool dump_tokens; 
bool dump_ast;
bool dump_ir;
bool dump_asm;

int main(int argc, char* argv[]) {
    int opt;
    while ((opt = getopt(argc, argv, "Siato:O::")) != -1) {
        switch (opt) {
            case 'o':
                strcpy(outfile, optarg);
                break;
            case 'a':
                dump_ast = true;
                break;
            case 't':
                dump_tokens = true;
                break;
            case 'i':
                dump_ir = true;
                break;
            case 'O':
            case 'S':
                dump_asm = true;
                break; 
            default:
                // fprintf(stderr, "Usage: %s [-o outfile] infile\n", argv[0]);
                // exit(EXIT_FAILURE);
                dump_asm = true;

                break;
        }
    }
    if (optind >= argc) {
        fprintf(stderr, "no input file\n");
        exit(EXIT_FAILURE);
    }
    if (!(yyin = fopen(argv[optind], "r"))) {
        fprintf(stderr, "%s: No such file or directory\nno input file\n",
                argv[optind]);
        exit(EXIT_FAILURE);
    }
    if (!(yyout = fopen(outfile, "w"))) {
        fprintf(stderr, "%s: fail to open output file\n", outfile);
        exit(EXIT_FAILURE);
    }

    std::vector<Type*> vec;
    std::vector<SymbolEntry*> vec1;
    vec.push_back(TypeSystem::intType);
    Type* funcType = new FunctionType(TypeSystem::voidType, vec,vec1);
    SymbolEntry *se;
    se = new IdentifierSymbolEntry(funcType, "putint", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se))->sysy = true;
    std::string str = funcType->toStr();
    identifiers->install("putint", se);

    std::vector<Type*> vec2;
    std::vector<SymbolEntry*> vec3;
    vec2.push_back(TypeSystem::intType);
    Type* funcType1 = new FunctionType(TypeSystem::voidType, vec2,vec3);
    SymbolEntry *se1;
    se1 = new IdentifierSymbolEntry(funcType1, "putch", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se1))->sysy = true;
    std::string str1 = funcType1->toStr();
    identifiers->install("putch", se1);

    std::vector<Type*> vec4;
    std::vector<SymbolEntry*> vec5;
    vec4.push_back(TypeSystem::intType);
    ArrayType* arr = new ArrayType(TypeSystem::intType, -1);
    vec4.push_back(arr);
    Type* funcType3 = new FunctionType(TypeSystem::voidType, vec4,vec5);
    SymbolEntry *se3;
    se3 = new IdentifierSymbolEntry(funcType3, "putarray", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se3))->sysy = true;
    std::string str3 = funcType3->toStr();
    identifiers->install("putarray", se3);


    std::vector<Type*> vec6;
    std::vector<SymbolEntry*> vec7;
    ArrayType* arr1 = new ArrayType(TypeSystem::intType, -1);
    vec6.push_back(arr1);
    Type* funcType4 = new FunctionType(TypeSystem::intType, vec6,vec7);
    SymbolEntry *se4;
    se4 = new IdentifierSymbolEntry(funcType4, "getarray", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se4))->sysy = true;
    std::string str4 = funcType4->toStr();
    identifiers->install("getarray", se4);

    std::vector<Type*> vec8;
    std::vector<SymbolEntry*> vec9;
    Type* funcType5 = new FunctionType(TypeSystem::intType, vec8,vec9);
    SymbolEntry *se5;
    se5 = new IdentifierSymbolEntry(funcType5, "getint", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se5))->sysy = true;
    std::string str5 = funcType5->toStr();
    identifiers->install("getint", se5);

    std::vector<Type*> vec10;
    std::vector<SymbolEntry*> vec11;
    Type* funcType6 = new FunctionType(TypeSystem::intType, vec10,vec11);
    SymbolEntry *se6;
    se6 = new IdentifierSymbolEntry(funcType6, "getch", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se6))->sysy = true;
    std::string str6 = funcType6->toStr();
    identifiers->install("getint", se6);


    yyparse();
    if (dump_ast)
        ast.output();
    ast.typeCheck();
    ast.genCode(&unit);
    if (dump_ir)
        unit.output();
    unit.genMachineCode(&mUnit);
    LinearScan linearScan(&mUnit);
    linearScan.allocateRegisters();
    if (dump_asm)
        mUnit.output();
    return 0;////
}
