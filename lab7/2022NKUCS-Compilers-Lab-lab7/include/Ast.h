#ifndef __AST_H__
#define __AST_H__

#include "Unit.h"
#include <fstream>
#include "Operand.h"
#include "Type.h"
#include <stack>

class SymbolEntry;
class Unit;
class Function;
class BasicBlock;
class Instruction;
class IRBuilder;
extern Unit unit;

class Node
{
private:
    static int counter;
    int seq;
    Node *next;

protected:
    std::vector<Instruction *> true_list;
    std::vector<Instruction *> false_list;
    static IRBuilder *builder;
    void backPatch(std::vector<Instruction *> &list, BasicBlock *bb);
    std::vector<Instruction *> merge(std::vector<Instruction *> &list1,
                                     std::vector<Instruction *> &list2);

public:
    Node();
    int getSeq() const { return seq; };
    static void setIRBuilder(IRBuilder *ib) { builder = ib; };
    virtual void output(int level) = 0;
    void setNext(Node *node);
    Node *getNext() { return next; }
    void typeCheck(){};
    virtual void genCode() = 0;
    std::vector<Instruction *> &trueList() { return true_list; }
    std::vector<Instruction *> &falseList() { return false_list; }
};

class ExprNode : public Node
{
private:
    
protected:
    int kind;
    enum
    {
        EXPR,
        INITVALUELISTEXPR,
        IMPLICTCASTEXPR,
        UNARYEXPR
    };
    Type *type;
    SymbolEntry *symbolEntry;
    //std::vector<int> vec;
    Operand *dst; // The result of the subtree is stored into dst.
public:
    ExprNode(SymbolEntry *symbolEntry, int kind = EXPR)
        : kind(kind), symbolEntry(symbolEntry){};
    Operand *getOperand() { return dst; };
    void output(int level);
    virtual int getValue() { return -1; };
    bool isExpr() const { return kind == EXPR; };
    bool isInitValueListExpr() const { return kind == INITVALUELISTEXPR; };
    bool isImplictCastExpr() const { return kind == IMPLICTCASTEXPR; };
    bool isUnaryExpr() const { return kind == UNARYEXPR; };
    SymbolEntry *getSymbolEntry() { return symbolEntry; };
    // virtual void typeCheck() {};
    void genCode();
    virtual Type *getType() { return type; };
    void setType(Type *t) { type = t; }
    Type *getOriginType() { return type; };
    Type *funcarray();
    //std::vector<int> getvector() { return vec; }
    //void setVec();
};

class BinaryExpr : public ExprNode
{
private:
    int op;
    ExprNode *expr1, *expr2;

public:
    enum
    {
        ADD,
        SUB,
        MUL,
        DIV,
        MOD,
        AND,
        OR,
        LESS,
        LESSEQUAL,
        GREATER,
        GREATEREQUAL,
        EQUAL,
        NOTEQUAL
    };
    BinaryExpr(SymbolEntry *se, int op, ExprNode *expr1, ExprNode *expr2);
    void output(int level);
    int getValue();
    void typeCheck();
    void genCode();
};

class UnaryExpr : public ExprNode
{
private:
    int op;
    ExprNode *expr;

public:
    enum
    {
        NOT,
        SUB
    };
    UnaryExpr(SymbolEntry *se, int op, ExprNode *expr);
    void output(int level);
    int getValue();
    void typeCheck();
    void genCode();
    int getOp() const { return op; };
    void setType(Type *type) { this->type = type; }
};

/*class CallExpr : public ExprNode {
   private:
    ExprNode* param;

   public:
    CallExpr(SymbolEntry* se, ExprNode* param = nullptr);
    void output(int level);
    void typeCheck();
    void genCode();
};*/

class Constant : public ExprNode
{
public:
    Constant(SymbolEntry *se) : ExprNode(se)
    {
        dst = new Operand(se);
        type = TypeSystem::intType;
    };
    void output(int level);
    int getValue();
    void typeCheck();
    void genCode();
};

class Id : public ExprNode
{
private:
    ExprNode *arrIdx;
    bool left = false;

public:
    Id(SymbolEntry *se, ExprNode *arrIdx = nullptr)
        : ExprNode(se), arrIdx(arrIdx)
    {
        if (se)
        {
            type = se->getType();
            if (type->isInt())
            {
                SymbolEntry *temp = new TemporarySymbolEntry(
                    TypeSystem::intType, SymbolTable::getLabel());
                dst = new Operand(temp);
            }
            else if (type->isArray())
            {
                SymbolEntry *temp = new TemporarySymbolEntry(
                    new PointerType(((ArrayType *)type)->getElementType()),
                    SymbolTable::getLabel());
                dst = new Operand(temp);
            }
        }
    };
    void output(int level);
    void typeCheck();
    void genCode();
    int getValue();
    ExprNode *getArrIdx() { return arrIdx; };
    Type *getType();
    bool isLeft() const { return left; };
    void setLeft() { left = true; }
};

class ImplicitValueInitExpr : public ExprNode
{
public:
    ImplicitValueInitExpr(SymbolEntry *se) : ExprNode(se){};
    void output(int level);
};

class InitValueListExpr : public ExprNode
{
private:
    ExprNode *expr;
    int childCnt;

public:
    InitValueListExpr(SymbolEntry *se, ExprNode *expr = nullptr)
        : ExprNode(se, INITVALUELISTEXPR), expr(expr)
    {
        type = se->getType();
        childCnt = 0;
    };
    void output(int level);
    ExprNode *getExpr() const { return expr; };
    void addExpr(ExprNode *expr);
    bool isEmpty() { return childCnt == 0; };
    bool isFull();
    void typeCheck();
    void genCode();
    void fill();
};

// 仅用于int2bool
class ImplictCastExpr : public ExprNode
{
private:
    ExprNode *expr;

public:
    ImplictCastExpr(ExprNode *expr)
        : ExprNode(nullptr, IMPLICTCASTEXPR), expr(expr)
    {
        type = TypeSystem::boolType;
        dst = new Operand(
            new TemporarySymbolEntry(type, SymbolTable::getLabel()));
    };
    void output(int level);
    ExprNode *getExpr() const { return expr; };
    void typeCheck();
    void genCode();
};
class StmtNode : public Node
{
private:
    int kind;

protected:
    enum
    {
        IF,
        IFELSE,
        WHILE,
        COMPOUND,
        RETURN
    };

public:
    StmtNode(int kind = -1) : kind(kind){};
    bool isIf() const { return kind == IF; };
    virtual void typeCheck() = 0;
};
class CompoundStmt : public StmtNode
{
private:
    StmtNode *stmt;

public:
    CompoundStmt(StmtNode *stmt = nullptr) : stmt(stmt){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class SeqNode : public StmtNode
{
private:
    StmtNode *stmt1, *stmt2;

public:
    SeqNode(StmtNode *stmt1, StmtNode *stmt2) : stmt1(stmt1), stmt2(stmt2){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class DeclStmt : public StmtNode
{
    /*   private:
        Id* id;
        ExprNode* expr;

       public:
        DeclStmt(Id* id, ExprNode* expr = nullptr) : id(id) {
            if (expr) {
                this->expr = expr;
                if (expr->isInitValueListExpr())
                    ((InitValueListExpr*)(this->expr))->fill();
            }
        };
        void output(int level);
        void typeCheck();
        void genCode();
        Id* getId() { return id; };*/
};

class VarDeclStmt : public DeclStmt
{
private:
    StmtNode *defs;

public:
    VarDeclStmt(StmtNode *defs) : defs(defs){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class ConstDeclStmt : public DeclStmt
{
private:
    StmtNode *defs;

public:
    ConstDeclStmt(StmtNode *defs) : defs(defs){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class Def : public StmtNode
{
private:
    Id *id;
    ExprNode *expr;

public:
    // Def(Id* id):id(id),expr(0){};//int a;
    // Def(Id* id,ExprNode* expr):id(id),expr(expr){};//int a = 3;
    Def(Id *id, ExprNode *expr = nullptr) : id(id)
    {
        if (expr)
        {
            this->expr = expr;
            if (expr->isInitValueListExpr())
                ((InitValueListExpr *)(this->expr))->fill();
        }
    };
    void output(int level);
    void typeCheck();
    void genCode();
    Id *getId() { return id; };
};

class Defs : public StmtNode
{
private:
    StmtNode *def;
    StmtNode *defs;

public:
    Defs(StmtNode *def, StmtNode *defs) : def(def), defs(defs){};
    Defs(StmtNode *def) : def(def), defs(0){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class EmptyStmt : public StmtNode
{
public:
    EmptyStmt(){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class IfStmt : public StmtNode
{
private:
    ExprNode *cond;
    StmtNode *thenStmt;

public:
    IfStmt(ExprNode *cond, StmtNode *thenStmt)
        : cond(cond), thenStmt(thenStmt)
    {
        if (cond->getType()->isInt() && cond->getType()->getSize() == 32)
        {
            ImplictCastExpr *temp = new ImplictCastExpr(cond);
            this->cond = temp;
        }
    };
    void output(int level);
    void typeCheck();
    void genCode();
};

class IfElseStmt : public StmtNode
{
private:
    ExprNode *cond;
    StmtNode *thenStmt;
    StmtNode *elseStmt;

public:
    IfElseStmt(ExprNode *cond, StmtNode *thenStmt, StmtNode *elseStmt)
        : cond(cond), thenStmt(thenStmt), elseStmt(elseStmt)
    {
        if (cond->getType()->isInt() && cond->getType()->getSize() == 32)
        {
            ImplictCastExpr *temp = new ImplictCastExpr(cond);
            this->cond = temp;
        }
    };
    void output(int level);
    void typeCheck();
    void genCode();
};

class WhileStmt : public StmtNode
{
private:
    ExprNode *cond;
    StmtNode *stmt;
    BasicBlock *condBlock;
    BasicBlock *endBlock;

public:
    // WhileStmt(ExprNode *cond, StmtNode *stmt) : cond(cond), stmt(stmt){};
    WhileStmt(ExprNode *cond, StmtNode *stmt = nullptr) : cond(cond), stmt(stmt)
    {
        if (cond->getType()->isInt() && cond->getType()->getSize() == 32)
        {
            ImplictCastExpr *temp = new ImplictCastExpr(cond);
            this->cond = temp;
        }
    };
    void output(int level);
    void typeCheck();
    void genCode();
    BasicBlock *getCondBlock() { return this->condBlock; }
    BasicBlock *getEndBlock() { return this->endBlock; }
};

class BreakStmt : public StmtNode
{
public:
    void output(int level);
    void typeCheck();
    void genCode();
};

class ContinueStmt : public StmtNode
{
public:
    void output(int level);
    void typeCheck();
    void genCode();
};

class ReturnStmt : public StmtNode
{
private:
    ExprNode *retValue;
    Type *retType;

public:
    ReturnStmt(ExprNode *retValue = nullptr) : retValue(retValue){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class AssignStmt : public StmtNode
{
private:
    ExprNode *lval;
    ExprNode *expr;

public:
    AssignStmt(ExprNode *lval, ExprNode *expr);
    void output(int level);
    void typeCheck();
    void genCode();
};

class ExprStmt : public StmtNode
{
private:
    ExprNode *expr;

public:
    ExprStmt(ExprNode *expr) : expr(expr){};
    void output(int level);
    void typeCheck();
    void genCode();
};

class FunctionDef : public StmtNode
{
private:
    SymbolEntry *se;
    // 参数的定义 next连接
    DeclStmt *decl;
    StmtNode *stmt;

public:
    FunctionDef(SymbolEntry *se, DeclStmt *decl, StmtNode *stmt)
        : se(se), decl(decl), stmt(stmt){};
    void output(int level);
    void typeCheck();
    void genCode();
    SymbolEntry *getSymbolEntry() { return se; };
};

class FuncCallExpr : public ExprNode
{
private:
    SymbolEntry *Funcname;
    std::vector<ExprNode *> params; // 实参链表
public:
    FuncCallExpr(SymbolEntry *funcname, SymbolEntry *se) : ExprNode(funcname), Funcname(funcname)
    {
        printf("check isSysy1\n");
        printf("parser.y--IdEntryname3 = %s\n", ((IdentifierSymbolEntry *)funcname)->getname().c_str());
        // if(se==nullptr)
        // {
        //     printf("funccallexpr se == nullptr\n");
        //     dst=nullptr;
        // }
        // else{
        //     dst=new Operand(se);
        // }
        dst = nullptr;
        // FuncName=new Operand(se);
        // dst = new Operand(se);
        // params=new std::vector<Operand*>;
        if (symbolEntry)
        {
            Type *type = symbolEntry->getType();
            type->toStr();
            printf("test callexpr type\n");
            this->type = ((FunctionType *)type)->getRetType();
            if (this->type != TypeSystem::voidType)
            {
                SymbolEntry *se =
                    new TemporarySymbolEntry(this->type, SymbolTable::getLabel());
                dst = new Operand(se);
            }
            // std::vector<Type*> params = ((FunctionType*)type)->getParamsType();
            printf("check isSysy2\n");
            // ExprNode* temp = param;
            //  for (auto it = params.begin(); it != params.end(); it++) {
            //      if (temp == nullptr) {
            //          fprintf(stderr, "too few arguments to function %s %s\n",
            //                  symbolEntry->toStr().c_str(), type->toStr().c_str());
            //          break;
            //      } else if ((*it)->getKind() != temp->getType()->getKind())
            //          fprintf(stderr, "parameter's type %s can't convert to %s\n",
            //                  temp->getType()->toStr().c_str(),
            //                  (*it)->toStr().c_str());
            //      temp = (ExprNode*)(temp->getNext());
            //  }
            //  if (temp != nullptr) {
            //      fprintf(stderr, "too many arguments to function %s %s\n",
            //              symbolEntry->toStr().c_str(), type->toStr().c_str());
            //  }
        }
        printf("check isSysy\n");
        printf("IdEntryname = %s\n", ((IdentifierSymbolEntry *)funcname)->getname().c_str());
        if (((IdentifierSymbolEntry *)funcname)->isSysy())
        {
            // std::string s = ((FunctionType*)funcname->getType())->toStr().c_str();
            // printf("funccall funcname s = %s\n", s);

            FunctionType *ftype = (FunctionType *)(funcname->getType());
            std::string str = ftype->toStr();
            // std::string name = str.substr(0, str.find('('));
            // std::string param = str.substr(str.find('('));
            printf("funccall funcname str = %s\n", str.c_str());
            unit.insertDeclaration(funcname);
            printf("sysy--FuncCallExpr::FuncCallExpr\n");
        }
    };

    void addParam(ExprNode *param)
    {
        if (param != NULL)
        {
            if (param->getType() == nullptr)
            {
                param->setType(TypeSystem::intType);
            }
            params.push_back(param);
        }
    }
    void output(int level);
    void typeCheck(); // 进行类型转换
    void genCode();   // 中间代码生成
    void printme() { printf("print funccallexpr!\n"); };
};

class Ast
{
private:
    Node *root;

public:
    Ast() { root = nullptr; }
    void setRoot(Node *n) { root = n; }
    void output();
    void typeCheck();
    void genCode(Unit *unit);
};

static std::stack<WhileStmt *> whileStack;
static std::stack<InitValueListExpr*> stk;

#endif
