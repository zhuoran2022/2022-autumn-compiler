#ifndef __TYPE_H__
#define __TYPE_H__
#include <vector>
#include <string>

class Type
{
private:
protected:
    enum {INT, VOID, FUNC, PTR, ARRAY, STRING,BOOL};
public:
    int size;
    int kind;
    Type(int kind, int size = 0) : kind(kind), size(size) {};
    virtual ~Type() {};
    virtual std::string toStr() = 0;
    bool isInt() const {return kind == INT;};
    bool isVoid() const {return kind == VOID;};
    bool isBool() const {return kind == BOOL;}
    bool isFunc() const {return kind == FUNC;};
    bool isPtr() const { return kind == PTR; };
    bool isArray() const { return kind == ARRAY; };
    bool isString() const { return kind == STRING; };
    int getSize() const { return size;};
    int getKind() const {return kind;}
    //void setSize(int s) const {size = s;};
};

class IntType : public Type
{
private:
    //int size;
public:
    //IntType(int size) : Type(Type::INT), size(size){};
    IntType(int size) : Type(Type::INT, size){printf("from type.h size = %d\n", size);};
    std::string toStr();
};

class VoidType : public Type
{
public:
    VoidType() : Type(Type::VOID){};
    std::string toStr();
};

class PointerType : public Type
{
private:
    Type *valueType;
public:
    PointerType(Type* valueType) : Type(Type::PTR) {this->valueType = valueType;};
    std::string toStr();
    Type* getType() const { return valueType; };
};

class TypeSystem
{
private:
    static IntType commonInt;
    static IntType commonBool;
    static VoidType commonVoid;
public:
    static Type *intType;
    static Type *voidType;
    static Type *boolType;
    static Type* getMaxType(Type* type1, Type* type2);
    static bool needCast(Type* type1, Type* type2);
};


class FunctionType : public Type
{
private:
    Type *returnType;
    std::vector<Type*> paramsType;
public:
    FunctionType(Type* returnType, std::vector<Type*> paramsType) : 
    Type(Type::FUNC), returnType(returnType), paramsType(paramsType){};
    Type* getRetType() {return returnType;};
    std::vector<Type*> getParamsType(){return paramsType;};
    void addParamType(Type* type){paramsType.push_back(type);}
    std::string toStr();
};

#endif
