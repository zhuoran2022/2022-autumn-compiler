#include "Unit.h"
extern FILE* yyout;

void Unit::insertFunc(Function *f)
{
    func_list.push_back(f);
}

void Unit::removeFunc(Function *func)
{
    func_list.erase(std::find(func_list.begin(), func_list.end(), func));
}

void Unit::insertGlobal(SymbolEntry* se) {
    global_list.push_back(se);
}

// void Unit::insertDeclaration(SymbolEntry* se){
//     declare_list.push_back(se);
// }

void Unit::output() const
{
    for (auto &glob : global_list)
    {
        fprintf(yyout, "%s = global %s %d, align 4\n", glob->toStr().c_str(),
                    glob->getType()->toStr().c_str(),
                    ((IdentifierSymbolEntry*)glob)->value);
    }
    printf("after global_list output\n");
    for (auto &func : func_list)
        func->output();
    printf("after func_list output\n");
    for (auto se : declare_list) {
        FunctionType* type = (FunctionType*)(se->getType());
        std::string str = type->toStr();
        std::string name = str.substr(0, str.find('('));
        std::string param = str.substr(str.find('('));
        printf("unit-declare-output str = %s\n", param.c_str());
        fprintf(yyout, "declare %s %s%s\n", type->getRetType()->toStr().c_str(),
                se->toStr().c_str(), param.c_str());
    }
    printf("after declare_list output\n");
}

Unit::~Unit()
{
    for(auto &func:func_list)
        delete func;
}

void Unit::insertDeclaration(SymbolEntry* se) {
    printf("Unit::insertDeclaration\n");
    auto it = std::find(declare_list.begin(), declare_list.end(), se);
    // for(int i = 0; i < declare_list.size(); i ++){
    //     if(declare_list[i] == se){
    //         return;
    //     }
    // }
    //if(&declare_list == nullptr){printf("!!\n");}
    if (it == declare_list.end()) {
        declare_list.push_back(se);
    }
}