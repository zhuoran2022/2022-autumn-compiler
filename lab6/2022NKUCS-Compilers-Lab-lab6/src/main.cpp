#include <iostream>
#include <string.h>
#include <unistd.h>
#include "Ast.h"
#include "Unit.h"
#include "SymbolTable.h"
using namespace std;

Ast ast;
Unit unit;
extern FILE *yyin;
extern FILE *yyout;

int yyparse();

char outfile[256] = "a.out";
bool dump_tokens;
bool dump_ast;
bool dump_ir;

int main(int argc, char *argv[])
{
    int opt;
    while ((opt = getopt(argc, argv, "iato:")) != -1)
    {
        switch (opt)
        {
        case 'o':
            strcpy(outfile, optarg);
            break;
        case 'a':
            dump_ast = true;
            break;
        case 't':
            dump_tokens = true;
            break;
        case 'i':
            dump_ir = true;
            break;
        default:
            fprintf(stderr, "Usage: %s [-o outfile] infile\n", argv[0]);
            exit(EXIT_FAILURE);
            break;
        }
    }
    if (optind >= argc)
    {
        fprintf(stderr, "no input file\n");
        exit(EXIT_FAILURE);
    }
    if (!(yyin = fopen(argv[optind], "r")))
    {
        fprintf(stderr, "%s: No such file or directory\nno input file\n", argv[optind]);
        exit(EXIT_FAILURE);
    }
    if (!(yyout = fopen(outfile, "w")))
    {
        fprintf(stderr, "%s: fail to open output file\n", outfile);
        exit(EXIT_FAILURE);
    }
    //printf("filename = %s\n", argv[4]);

    std::vector<Type*> vec;
    Type* funcType = new FunctionType(TypeSystem::intType, vec);
    SymbolEntry *se1;
    se1 = new IdentifierSymbolEntry(funcType, "getint", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se1))->sysy = true;
    identifiers->install("getint", se1);

    // std::vector<Type*> vec2;
    // Type* funcType2 = new FunctionType(TypeSystem::intType, vec2);
    // SymbolEntry *se2;
    // se2 = new IdentifierSymbolEntry(funcType2, "getch", identifiers->getLevel());
    // identifiers->install("getch", se2);

    std::vector<Type*> vec3;
    vec3.push_back(TypeSystem::intType);
    Type* funcType3 = new FunctionType(TypeSystem::voidType, vec3);
    SymbolEntry *se3;
    se3 = new IdentifierSymbolEntry(funcType3, "putint", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se3))->sysy = true;
    //FunctionType* type = (FunctionType*)(se3->getType());
    std::string str = funcType3->toStr();
    printf("main-putint-output str = %s\n", str.c_str());

    printf("main--IdEntryname = %s\n", ((IdentifierSymbolEntry*)se3)->getname().c_str());

    identifiers->install("putint", se3);

    // std::vector<Type*> vec4;
    // Type* funcType4 = new FunctionType(TypeSystem::intType, vec4);
    // SymbolEntry *se4;
    // se4 = new IdentifierSymbolEntry(funcType4, "putch", identifiers->getLevel());
    // identifiers->install("putch", se4);
    std::vector<Type*> vec4;
    vec4.push_back(TypeSystem::intType);
    Type* funcType4 = new FunctionType(TypeSystem::voidType, vec4);
    SymbolEntry *se4;
    se4 = new IdentifierSymbolEntry(funcType4, "putch", identifiers->getLevel());
    (dynamic_cast<IdentifierSymbolEntry*>(se4))->sysy = true;
    std::string str4 = funcType4->toStr();
    identifiers->install("putch", se4);

    yyparse();
    if(dump_ast)
        ast.output();
    ast.typeCheck();
    ast.genCode(&unit);
    if(dump_ir){
        unit.output();//write the .ll
        printf("main--after unit.output()\n");
    }
    return 0;
}
