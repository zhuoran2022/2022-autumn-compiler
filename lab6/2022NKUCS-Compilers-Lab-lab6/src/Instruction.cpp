#include "Instruction.h"
#include "BasicBlock.h"
#include <iostream>
#include "Function.h"
#include "Type.h"
extern FILE* yyout;


Instruction::Instruction(unsigned instType, BasicBlock *insert_bb)
{
    printf("Instruction initial %d\n", instType);
    prev = next = this;
    opcode = -1;
    this->instType = instType;
    if (insert_bb != nullptr)
    {
        insert_bb->insertBack(this);//here is where an instruction inserted
        parent = insert_bb;
    }
}

Instruction::~Instruction()
{
    parent->remove(this);
}

BasicBlock *Instruction::getParent()
{
    return parent;
}

void Instruction::setParent(BasicBlock *bb)
{
    parent = bb;
}

void Instruction::setNext(Instruction *inst)
{
    next = inst;
}

void Instruction::setPrev(Instruction *inst)
{
    prev = inst;
}

Instruction *Instruction::getNext()
{
    return next;
}

Instruction *Instruction::getPrev()
{
    return prev;
}

BinaryInstruction::BinaryInstruction(unsigned opcode, Operand *dst, Operand *src1, Operand *src2, BasicBlock *insert_bb) : Instruction(BINARY, insert_bb)
{
    printf("BinaryInstruction initial\n");
    this->opcode = opcode;
    operands.push_back(dst);
    operands.push_back(src1);
    operands.push_back(src2);
    dst->setDef(this);
    src1->addUse(this);
    src2->addUse(this);
}

BinaryInstruction::~BinaryInstruction()
{
    operands[0]->setDef(nullptr);
    if(operands[0]->usersNum() == 0)
        delete operands[0];
    operands[1]->removeUse(this);
    operands[2]->removeUse(this);
}

void BinaryInstruction::output() const
{
    printf("BinaryInstruction::output()\n");
    std::string s1, s2, s3, op, type;
    s1 = operands[0]->toStr();
    s2 = operands[1]->toStr();
    s3 = operands[2]->toStr();
    type = operands[0]->getType()->toStr();
    switch (opcode)
    {
    case ADD:
        op = "add";
        break;
    case SUB:
        op = "sub";
        break;
    case MUL:
        op = "mul";
        break;
    case DIV:
        op = "sdiv";
        break;
    case MOD:
        op = "srem";
        break;
    default:
        break;
    }
    fprintf(yyout, "  %s = %s %s %s, %s\n", s1.c_str(), op.c_str(), type.c_str(), s2.c_str(), s3.c_str());
}

CmpInstruction::CmpInstruction(unsigned opcode, Operand *dst, Operand *src1, Operand *src2, BasicBlock *insert_bb): Instruction(CMP, insert_bb){
    printf("CmpInstruction initial\n");
    this->opcode = opcode;
    operands.push_back(dst);
    operands.push_back(src1);
    operands.push_back(src2);
    dst->setDef(this);
    src1->addUse(this);
    src2->addUse(this);
}

CmpInstruction::~CmpInstruction()
{
    operands[0]->setDef(nullptr);
    if(operands[0]->usersNum() == 0)
        delete operands[0];
    operands[1]->removeUse(this);
    operands[2]->removeUse(this);
}

void CmpInstruction::output() const
{
    printf("CmpInstruction::output()\n");
    std::string s1, s2, s3, op, type;
    s1 = operands[0]->toStr();
    s2 = operands[1]->toStr();
    s3 = operands[2]->toStr();
    type = operands[1]->getType()->toStr();
    switch (opcode)
    {
    case E:
        op = "eq";
        break;
    case NE:
        op = "ne";
        break;
    case L:
        op = "slt";
        break;
    case LE:
        op = "sle";
        break;
    case G:
        op = "sgt";
        break;
    case GE:
        op = "sge";
        break;
    default:
        op = "";
        break;
    }

    fprintf(yyout, "  %s = icmp %s %s %s, %s\n", s1.c_str(), op.c_str(), type.c_str(), s2.c_str(), s3.c_str());
}

UncondBrInstruction::UncondBrInstruction(BasicBlock *to, BasicBlock *insert_bb) : Instruction(UNCOND, insert_bb)
{
    printf("UncondInstruction initial\n");
    if(to == nullptr)
    {
        printf("uncondbrinstruction get to == nullptr\n");
    }
    branch = to;
}

void UncondBrInstruction::output() const
{
    printf("UncondBrInstruction::output()\n");
    fprintf(yyout, "  br label %%B%d\n", branch->getNo());
}

void UncondBrInstruction::setBranch(BasicBlock *bb)
{
    branch = bb;
    if(bb == nullptr)
    {
        printf("uncondbrinstruction get bb == nullptr in setBrach\n");
    }
}

BasicBlock *UncondBrInstruction::getBranch()
{
    return branch;
}

CondBrInstruction::CondBrInstruction(BasicBlock*true_branch, BasicBlock*false_branch, Operand *cond, BasicBlock *insert_bb) : Instruction(COND, insert_bb){
    printf("CondBrInstruction initial\n");
    this->true_branch = true_branch;
    this->false_branch = false_branch;
    cond->addUse(this);
    operands.push_back(cond);
    std::string type = operands[0]->getType()->toStr();
    printf("i1problem type = %s\n", type.c_str());
}

CondBrInstruction::~CondBrInstruction()
{
    operands[0]->removeUse(this);
}

void CondBrInstruction::output() const
{
    printf("CondBrInstruction::output()\n");
    std::string cond, type;
    cond = operands[0]->toStr();
    type = operands[0]->getType()->toStr();
    int true_label = true_branch->getNo();
    int false_label = false_branch->getNo();
    fprintf(yyout, "  br %s %s, label %%B%d, label %%B%d\n", type.c_str(), cond.c_str(), true_label, false_label);
}

void CondBrInstruction::setFalseBranch(BasicBlock *bb)
{
    false_branch = bb;
}

BasicBlock *CondBrInstruction::getFalseBranch()
{
    return false_branch;
}

void CondBrInstruction::setTrueBranch(BasicBlock *bb)
{
    true_branch = bb;
}

BasicBlock *CondBrInstruction::getTrueBranch()
{
    return true_branch;
}

RetInstruction::RetInstruction(Operand *src, BasicBlock *insert_bb) : Instruction(RET, insert_bb)
{
    printf("RetInstruction initial\n");
    if(src != nullptr)
    {
        operands.push_back(src);
        src->addUse(this);
    }
}

RetInstruction::~RetInstruction()
{
    if(!operands.empty())
        operands[0]->removeUse(this);
}

void RetInstruction::output() const
{
    printf("RetInstruction::output()\n");
    if(operands.empty())
    {
        fprintf(yyout, "  ret void\n");
    }
    else
    {
        std::string ret, type;
        ret = operands[0]->toStr();
        type = operands[0]->getType()->toStr();
        fprintf(yyout, "  ret %s %s\n", type.c_str(), ret.c_str());
    }
}

AllocaInstruction::AllocaInstruction(Operand *dst, SymbolEntry *se, BasicBlock *insert_bb) : Instruction(ALLOCA, insert_bb)
{
    printf("AllocaInstruction initial\n");
    operands.push_back(dst);
    dst->setDef(this);
    this->se = se;
}

AllocaInstruction::~AllocaInstruction()
{
    operands[0]->setDef(nullptr);
    if(operands[0]->usersNum() == 0)
        delete operands[0];
}

void AllocaInstruction::output() const
{
    std::string dst, type;
    // if(operands == nullptr)
    // {
    //     printf("allocainstruction::output find operands[0] == nullptr\n");
    // }
    dst = operands[0]->toStr();
    type = se->getType()->toStr();
    fprintf(yyout, "  %s = alloca %s, align 4\n", dst.c_str(), type.c_str());
    printf("AllocaInstruction::output()\n");
}

LoadInstruction::LoadInstruction(Operand *dst, Operand *src_addr, BasicBlock *insert_bb) : Instruction(LOAD, insert_bb)
{
    printf("LoadInstruction initial\n");
    operands.push_back(dst);
    operands.push_back(src_addr);
    dst->setDef(this);
    src_addr->addUse(this);
}

LoadInstruction::~LoadInstruction()
{
    operands[0]->setDef(nullptr);
    if(operands[0]->usersNum() == 0)
        delete operands[0];
    operands[1]->removeUse(this);
}

void LoadInstruction::output() const
{
    printf("LoadInstruction::output()\n");
    std::string dst = operands[0]->toStr();
    std::string src = operands[1]->toStr();
    std::string src_type;
    std::string dst_type;
    dst_type = operands[0]->getType()->toStr();
    src_type = operands[1]->getType()->toStr();
    fprintf(yyout, "  %s = load %s, %s %s, align 4\n", dst.c_str(), dst_type.c_str(), src_type.c_str(), src.c_str());
}

StoreInstruction::StoreInstruction(Operand *dst_addr, Operand *src, BasicBlock *insert_bb) : Instruction(STORE, insert_bb)
{
    operands.push_back(dst_addr);
    operands.push_back(src);
    dst_addr->addUse(this);
    src->addUse(this);
    printf("StoreInstruction initial\n");
}

StoreInstruction::~StoreInstruction()
{
    operands[0]->removeUse(this);
    operands[1]->removeUse(this);
}

void StoreInstruction::output() const
{
    printf("StoreInstruction::output()\n");
    std::string dst = operands[0]->toStr();
    std::string src = operands[1]->toStr();
    std::string dst_type = operands[0]->getType()->toStr();
    std::string src_type = operands[1]->getType()->toStr();
    printf("StoreInstruction::output() end\n");

    fprintf(yyout, "  store %s %s, %s %s, align 4\n", src_type.c_str(), src.c_str(), dst_type.c_str(), dst.c_str());
}

ZextInstruction::ZextInstruction(Operand* dst,
                                 Operand* src,
                                 BasicBlock* insert_bb)
    : Instruction(ZEXT, insert_bb) {
    operands.push_back(dst);
    operands.push_back(src);
    dst->setDef(this);
    src->addUse(this);
}

ZextInstruction::~ZextInstruction()
{
    operands[0]->setDef(nullptr);
    if(operands[0]->usersNum() == 0)
        delete operands[0];
    operands[1]->removeUse(this);
}

void ZextInstruction::output() const {
    std::string dst = operands[0]->toStr();
    std::string dst_type = operands[0]->getType()->toStr();
    std::string src = operands[1]->toStr();
    std::string src_type = operands[1]->getType()->toStr();
    fprintf(yyout, "  %s = zext %s %s to %s\n", dst.c_str(),
            src_type.c_str(), src.c_str(),dst_type.c_str());
}

XorInstruction::XorInstruction(Operand* dst,
                               Operand* src,
                               BasicBlock* insert_bb)
    : Instruction(XOR, insert_bb) {
    operands.push_back(dst);
    operands.push_back(src);
    dst->setDef(this);
    src->addUse(this);
}

void XorInstruction::output() const {
    Operand* dst = operands[0];
    Operand* src = operands[1];
    fprintf(yyout, "  %s = xor %s %s, true\n", dst->toStr().c_str(),
            src->getType()->toStr().c_str(), src->toStr().c_str());
}

CallInstruction::CallInstruction(SymbolEntry *funcname,Operand *dst, std::vector<Operand*> params,BasicBlock *insert_bb):Instruction(CALL, insert_bb)
{
    //params
    this->funcname=funcname;

    for(auto it=params.begin();it!=params.end();it++)
    {
        PARAMS.push_back((*it));
    }

    operands.push_back(dst);

    for(auto it=params.begin();it!=params.end();it++)
    {
        operands.push_back((*it));
    }
    dst->setDef(this);
    for(auto it=params.begin();it!=params.end();it++)
    {
        (*it)->addUse(this);
    }
}

CallInstruction::~CallInstruction()
{
    
}

void CallInstruction::output() const
{
    
    // %7 = call i32 @f(i32 %5, i32 %6)
    printf("callinstruction output\n");
    std::string FUNCname=funcname->toStr();
    std::string s1, s2, type,funcrettype;
    s1 = operands[0]->toStr();
    funcrettype = funcname->getType()->toStr();
    printf("funcrettype = %s\n", funcrettype.c_str());
    //if(funcrettype=="i32()" || funcrettype=="i32(i32)")
    if(funcrettype[0] == 'i' && funcrettype[1] == '3' && funcrettype[2] == '2')
    {
        funcrettype="i32";
        fprintf(yyout, "  %s = call %s %s(", s1.c_str(), funcrettype.c_str(), FUNCname.c_str());
        
    }
    if(funcrettype=="void()" || funcrettype=="void(i32)")
    {
        funcrettype="void";
        fprintf(yyout, "  call %s %s(",  funcrettype.c_str(), FUNCname.c_str());
        
    }
    
    if((operands).size()==1)
    {
        fprintf(yyout, ")\n");
        
    }
    else{
    if((operands).size()==2)
    {
         s2=operands[1]->toStr();
         operands[1]->getType();//->toStr();
        if(operands[1]->getType() == nullptr){
            printf("operands[1]->getType() == nullptr\n");
        }
        operands[1]->getSymEntry()->setType(TypeSystem::intType);//it shouldn't be added here
        type=operands[1]->getType()->toStr();
        printf("callinstruction output breakpoint\n");
        fprintf(yyout, " %s %s)\n", type.c_str(),s2.c_str());
    }
    else{
         s2=operands[1]->toStr();
        type=operands[1]->getType()->toStr();
        fprintf(yyout, " %s %s ", type.c_str(),s2.c_str());

        for(auto it=operands.begin()+2;it!=operands.end();++it)
        {
             s2=(*it)->toStr();
        type=(*it)->getType()->toStr();
        fprintf(yyout, ", %s %s ", type.c_str(),s2.c_str());

        }
        fprintf(yyout, ")\n");

    }

    }
    
    
}


//-------------------函数形参赋值---------------
FuncStoreInstruction::FuncStoreInstruction(Operand *dst_addr, std::string src, int paramno,BasicBlock *insert_bb) : Instruction(STORE, insert_bb)
{
    operands.push_back(dst_addr);
    //operands.push_back(src);
    this->src=src;
    dst_addr->addUse(this);
    this->paramno=paramno;
   // src->addUse(this);
}


FuncStoreInstruction::~FuncStoreInstruction()
{
    operands[0]->removeUse(this);
   // operands[1]->removeUse(this);
}

void FuncStoreInstruction::output() const
{
    std::string dst = operands[0]->toStr();
   // std::string src = operands[1]->toStr();
    std::string dst_type = operands[0]->getType()->toStr();
  //  std::string src_type = operands[1]->getType()->toStr();
  std::string src_type =TypeSystem::intType->toStr();

    fprintf(yyout, "  store %s %s, %s %s, align 4\n", src_type.c_str(), src.c_str(), dst_type.c_str(), dst.c_str());
}


//------------------------------------------------