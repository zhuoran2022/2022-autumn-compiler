#include "Type.h"
#include <sstream>

IntType TypeSystem::commonInt = IntType(32);
IntType TypeSystem::commonBool = IntType(1);
VoidType TypeSystem::commonVoid = VoidType();

Type* TypeSystem::intType = &commonInt;
Type* TypeSystem::voidType = &commonVoid;
Type* TypeSystem::boolType = &commonBool;

Type* TypeSystem::getMaxType(Type* type1, Type* type2){
    if(type1->isInt() || type2->isInt()) return intType;
    else return boolType;
}

bool TypeSystem::needCast(Type* src, Type* target) {
    if(src->isInt() && target->isInt()) {
        return false;
    }
    return true;
}

std::string IntType::toStr()
{
    //printf("IntType::toStr\n");
    std::ostringstream buffer;
    buffer << "i" << size;
    return buffer.str();
}

std::string VoidType::toStr()
{
    printf("VoidType::toStr\n");
    return "void";
}

/*std::string FunctionType::toStr()
{
    printf("FunctionType::toStr\n");
    // std::ostringstream buffer;
    // buffer << returnType->toStr() << "()";
    // return buffer.str();
    std::ostringstream buffer;
    buffer << returnType->toStr() << "(";
    for (auto it = paramsType.begin(); it != paramsType.end(); it++) {
        buffer << (*it)->toStr();
        if (it + 1 != paramsType.end())
            buffer << ", ";
    }
    buffer << ')';
    return buffer.str();
}*/

std::string PointerType::toStr()
{
    printf("PointerType::toStr\n");
    std::ostringstream buffer;
    buffer << valueType->toStr() << "*";
    return buffer.str();
}

std::string FunctionType::toStr()
{
    printf("FunctionType::toStr\n");
    // std::ostringstream buffer;
    // buffer << returnType->toStr() << "()";
    // return buffer.str();
    std::ostringstream buffer;
    buffer << returnType->toStr() << "(";
    //buffer<<"1";
    for (auto it = paramsType.begin(); it != paramsType.end(); it++) {
        buffer << (*it)->toStr();
        // if (it + 1 != paramsType.end())
        //     buffer << ", ";

        printf("functiontype::tostr = %s\n", ((*it)->toStr()).c_str());
        if (it + 1 != paramsType.end())
        {
            //buffer << ", ";
            printf("functiontypeLLtostr = ,\n");
        }
    }
    buffer << ')';
    return buffer.str();
}
