/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* "%code top" blocks.  */
#line 499 "src/parser.y" /* yacc.c:316  */

    #include <iostream>
    #include <assert.h>
    #include "parser.h"
    extern Ast ast;
    int yylex();
    int yyerror( char const * );

#line 72 "src/parser.cpp" /* yacc.c:316  */



/* Copy the first part of user declarations.  */
#line 514 "src/parser.y" /* yacc.c:339  */

FunctionDef *func;
FuncCallExpr *call;
std::vector<FuncCallExpr*> callBeforeExp;
ReturnStmt *ret=nullptr;

#line 84 "src/parser.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.h".  */
#ifndef YY_YY_INCLUDE_PARSER_H_INCLUDED
# define YY_YY_INCLUDE_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 508 "src/parser.y" /* yacc.c:355  */

    #include "Ast.h"
    #include "SymbolTable.h"
    #include "Type.h"

#line 120 "src/parser.cpp" /* yacc.c:355  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ID = 258,
    INTEGER = 259,
    IF = 260,
    ELSE = 261,
    WHILE = 262,
    CONST = 263,
    INT = 264,
    VOID = 265,
    LPAREN = 266,
    RPAREN = 267,
    LBRACE = 268,
    RBRACE = 269,
    SEMICOLON = 270,
    COMMA = 271,
    LSBRACE = 272,
    RSBRACE = 273,
    OR = 274,
    AND = 275,
    EQUAL = 276,
    NOTEQUAL = 277,
    ADD = 278,
    SUB = 279,
    MUL = 280,
    DIV = 281,
    MOD = 282,
    LESS = 283,
    MORE = 284,
    NOMORE = 285,
    NOLESS = 286,
    ASSIGN = 287,
    NOT = 288,
    RETURN = 289,
    THEN = 290
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 521 "src/parser.y" /* yacc.c:355  */

    int itype;
    char* strtype;
    StmtNode* stmttype;
    ExprNode* exprtype;
    Type* type;

#line 176 "src/parser.cpp" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_INCLUDE_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 193 "src/parser.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  59
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   146

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  36
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  41
/* YYNRULES -- Number of rules.  */
#define YYNRULES  81
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  135

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   290

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   554,   554,   559,   560,   565,   566,   567,   568,   569,
     570,   571,   572,   575,   592,   598,   597,   608,   607,   618,
     621,   627,   633,   637,   644,   648,   652,   656,   663,   670,
     678,   678,   714,   716,   716,   724,   724,   732,   756,   759,
     769,   776,   778,   784,   792,   794,   800,   806,   814,   816,
     823,   831,   838,   847,   849,   855,   864,   866,   875,   877,
     885,   888,   904,   908,   915,   922,   929,   933,   940,   944,
     951,   970,   988,  1091,  1102,  1090,  1121,  1123,  1128,  1136,
    1151,  1153
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ID", "INTEGER", "IF", "ELSE", "WHILE",
  "CONST", "INT", "VOID", "LPAREN", "RPAREN", "LBRACE", "RBRACE",
  "SEMICOLON", "COMMA", "LSBRACE", "RSBRACE", "OR", "AND", "EQUAL",
  "NOTEQUAL", "ADD", "SUB", "MUL", "DIV", "MOD", "LESS", "MORE", "NOMORE",
  "NOLESS", "ASSIGN", "NOT", "RETURN", "THEN", "$accept", "Stmts", "Stmt",
  "AssignStmt", "BlockStmt", "IfStmt", "ReturnStmt", "DeclStmt", "FuncDef",
  "VarDeclStmt", "ConstDeclStmt", "VarDefs", "VarDef", "ConstDef",
  "ConstDefs", "UselessStmt", "WhileStmt", "Exp", "AddExp", "UnaryExp",
  "Cond", "LOrExp", "PrimaryExp", "LVal", "RelExp", "LAndExp", "EqExp",
  "MulExp", "FuncFParams", "FuncFParam", "FuncRParams", "FuncRParam",
  "Type", "Program", "$@1", "$@2", "$@3", "$@4", "$@5", "$@6", "$@7", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290
};
# endif

#define YYPACT_NINF -46

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-46)))

#define YYTABLE_NINF -74

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      97,    15,   -46,    34,    40,    26,   -46,   -46,    70,    43,
     -46,    70,    70,    70,   113,    97,   -46,   -46,   -46,   -46,
     -46,   -46,   -46,   -46,   -46,   -46,   -46,    44,   -18,   -46,
     -46,    28,    17,    55,    71,   -46,    70,    70,    67,    65,
     -46,    97,    64,   -46,   -46,   -46,   -46,    68,   -46,   -46,
      70,    70,    70,    70,    70,    70,    -2,    69,    63,   -46,
      -9,   -18,    74,    73,     3,    62,    18,    76,    57,    79,
      75,   -46,    14,   -46,   -46,    17,    17,    82,   -46,   -46,
     -46,    70,    87,   -46,   106,     4,    70,    97,    70,    70,
      70,    70,    70,    70,    70,    70,    97,    70,    67,   -46,
     -46,   -46,   -46,    26,    81,   -46,   -46,   -46,   -46,   -46,
     105,    62,   -18,   -18,   -18,   -18,    18,     3,     3,   -46,
     -46,   -46,    98,   -46,   115,    70,    97,    26,   110,   -46,
     -46,   -46,   -46,   112,   -46
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    13,    39,     0,     0,     0,    60,    61,     0,    15,
      81,     0,     0,     0,     0,     2,     3,     5,     6,     7,
       9,    10,    11,    62,    63,    12,     8,     0,    24,    44,
      26,    38,    41,     0,     0,    30,     0,     0,     0,     0,
      38,     0,     0,    27,    28,    29,    23,     0,     4,    80,
       0,     0,     0,     0,     0,     0,    70,     0,    67,     1,
      33,    48,     0,    25,    53,    58,    56,     0,     0,    69,
       0,    40,     0,    18,    22,    42,    43,     0,    45,    46,
      47,     0,     0,    65,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    64,
      16,    14,    71,    76,    70,    66,    31,    35,    37,    34,
      19,    59,    49,    50,    51,    52,    57,    54,    55,    21,
      72,    68,    74,    77,     0,     0,     0,     0,     0,    79,
      36,    20,    78,     0,    75
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -46,    86,   -11,   -46,    -1,   -46,   -46,   -46,   -46,   -46,
     -46,    45,   -46,   -46,    35,   -46,   -46,    -6,   -26,     1,
     101,   -46,   -46,     0,   -45,    46,    42,     2,   -46,    12,
     -46,    16,    -4,   -46,   -46,   -46,   -46,   -46,   -46,   -46,
     -46
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    57,    58,    69,    70,    25,    26,    27,    28,    29,
      62,    63,    30,    40,    64,    65,    66,    32,   122,   123,
      85,   109,    33,    34,    41,    42,    60,    86,   125,    82,
     128
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      31,    38,    39,   -32,    48,    50,    51,   -32,    47,   -73,
      61,    61,    43,    44,    45,    31,   106,     1,     2,     3,
     107,     4,     5,     6,     7,     8,    35,     9,   100,    10,
      81,    89,    90,    91,    92,     6,     7,    11,    12,    94,
      95,    31,    53,    54,    55,    36,    77,    13,    14,   117,
     118,    37,    75,    76,    78,    79,    80,   -17,    56,    49,
      52,    48,    61,   112,   113,   114,   115,    61,    61,    61,
      68,    59,    31,     1,     2,   102,   110,    71,    73,    84,
     108,     8,    93,    74,    83,   119,    87,    31,    96,    97,
      99,   120,    88,    11,    12,    98,    31,   101,   103,   124,
       1,     2,     3,    13,     4,     5,     6,     7,     8,   104,
       9,   126,    10,    81,   127,   131,     1,     2,   129,   108,
      11,    12,   133,   124,     8,     9,    31,    72,    46,   105,
      13,    14,   134,   121,   111,   116,    11,    12,    67,   132,
       0,   130,     0,     0,     0,     0,    13
};

static const yytype_int16 yycheck[] =
{
       0,     5,     8,    12,    15,    23,    24,    16,    14,    11,
      36,    37,    11,    12,    13,    15,    12,     3,     4,     5,
      16,     7,     8,     9,    10,    11,    11,    13,    14,    15,
      32,    28,    29,    30,    31,     9,    10,    23,    24,    21,
      22,    41,    25,    26,    27,    11,    52,    33,    34,    94,
      95,    11,    50,    51,    53,    54,    55,    14,     3,    15,
      32,    72,    88,    89,    90,    91,    92,    93,    94,    95,
       3,     0,    72,     3,     4,    81,    87,    12,    14,    16,
      86,    11,    20,    15,    15,    96,    12,    87,    12,    32,
      15,    97,    19,    23,    24,    16,    96,    15,    11,   103,
       3,     4,     5,    33,     7,     8,     9,    10,    11,     3,
      13,     6,    15,    32,    16,   126,     3,     4,     3,   125,
      23,    24,    12,   127,    11,    13,   126,    41,    15,    84,
      33,    34,   133,    98,    88,    93,    23,    24,    37,   127,
      -1,   125,    -1,    -1,    -1,    -1,    33
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     7,     8,     9,    10,    11,    13,
      15,    23,    24,    33,    34,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    51,    52,    53,    54,    55,
      58,    59,    63,    68,    69,    11,    11,    11,    68,    53,
      59,    70,    71,    55,    55,    55,    15,    53,    38,    15,
      23,    24,    32,    25,    26,    27,     3,    47,    48,     0,
      72,    54,    56,    57,    60,    61,    62,    56,     3,    49,
      50,    12,    37,    14,    15,    63,    63,    53,    55,    55,
      55,    32,    75,    15,    16,    66,    73,    12,    19,    28,
      29,    30,    31,    20,    21,    22,    12,    32,    16,    15,
      14,    15,    53,    11,     3,    47,    12,    16,    53,    67,
      38,    61,    54,    54,    54,    54,    62,    60,    60,    38,
      53,    50,    64,    65,    68,    74,     6,    16,    76,     3,
      67,    38,    65,    12,    40
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    36,    69,    37,    37,    38,    38,    38,    38,    38,
      38,    38,    38,    59,    39,    70,    40,    71,    40,    41,
      41,    52,    42,    42,    53,    56,    55,    55,    55,    55,
      72,    55,    66,    73,    66,    74,    66,    67,    58,    58,
      58,    54,    54,    54,    63,    63,    63,    63,    60,    60,
      60,    60,    60,    62,    62,    62,    61,    61,    57,    57,
      68,    68,    43,    43,    46,    45,    47,    47,    50,    50,
      48,    48,    49,    75,    76,    44,    64,    64,    64,    65,
      51,    51
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     0,     4,     0,     3,     5,
       7,     5,     3,     2,     1,     1,     1,     2,     2,     2,
       0,     5,     0,     0,     2,     0,     4,     1,     1,     1,
       3,     1,     3,     3,     1,     3,     3,     3,     1,     3,
       3,     3,     3,     1,     3,     3,     1,     3,     1,     3,
       1,     1,     1,     1,     4,     3,     3,     1,     3,     1,
       1,     3,     3,     0,     0,     8,     0,     1,     3,     2,
       2,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 554 "src/parser.y" /* yacc.c:1646  */
    {
        ast.setRoot((yyvsp[0].stmttype));
    }
#line 1375 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 3:
#line 559 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1381 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 560 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new SeqNode((yyvsp[-1].stmttype), (yyvsp[0].stmttype));
    }
#line 1389 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 565 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1395 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 6:
#line 566 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1401 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 7:
#line 567 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1407 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 8:
#line 568 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1413 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 569 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1419 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 570 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1425 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 571 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=(yyvsp[0].stmttype);}
#line 1431 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 12:
#line 572 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype) = (yyvsp[0].stmttype);}
#line 1437 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 13:
#line 575 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se;
        se = identifiers->lookup((yyvsp[0].strtype));
        if(se == nullptr)
        {
            fprintf(stderr, "identifier \"%s\" is undefined\n", (char*)(yyvsp[0].strtype));
            delete [](char*)(yyvsp[0].strtype);
            assert(se != nullptr);
        }
        (yyval.exprtype) = new Id(se);
        delete [](yyvsp[0].strtype);

        (dynamic_cast<Id *>(yyval.exprtype))->getType()->isVoid();
    }
#line 1456 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 592 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new AssignStmt((yyvsp[-3].exprtype), (yyvsp[-1].exprtype));
    }
#line 1464 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 598 "src/parser.y" /* yacc.c:1646  */
    {identifiers = new SymbolTable(identifiers);}
#line 1470 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 16:
#line 600 "src/parser.y" /* yacc.c:1646  */
    {
            (yyval.stmttype) = new CompoundStmt((yyvsp[-1].stmttype));
            SymbolTable *top = identifiers;
            identifiers = identifiers->getPrev();
            delete top;
        }
#line 1481 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 17:
#line 608 "src/parser.y" /* yacc.c:1646  */
    {identifiers = new SymbolTable(identifiers);}
#line 1487 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 18:
#line 610 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new CompoundStmt(nullptr);
        SymbolTable *top = identifiers;//now top is identifiers
        identifiers = identifiers->getPrev();//now identifiers is before identifiers
        delete top;//why delete new ones?create,and delete,why?
    }
#line 1498 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 19:
#line 618 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new IfStmt((yyvsp[-2].exprtype), (yyvsp[0].stmttype));
    }
#line 1506 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 20:
#line 621 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new IfElseStmt((yyvsp[-4].exprtype), (yyvsp[-2].stmttype), (yyvsp[0].stmttype));
    }
#line 1514 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 21:
#line 627 "src/parser.y" /* yacc.c:1646  */
    {
         (yyval.stmttype) = new WhileStmt((yyvsp[-2].exprtype), (yyvsp[0].stmttype));
    }
#line 1522 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 22:
#line 633 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new ReturnStmt((yyvsp[-1].exprtype));
    }
#line 1530 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 23:
#line 638 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype) = new ReturnStmt(nullptr);
    }
#line 1538 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 24:
#line 644 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1544 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 648 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1550 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 26:
#line 652 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.exprtype)=(yyvsp[0].exprtype);
    }
#line 1558 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 656 "src/parser.y" /* yacc.c:1646  */
    {
        // SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        // $$ = new UnaryExpr(se, UnaryExpr::ADD, $2);
        // printf("unaryexpr op = %d\n", (dynamic_cast<UnaryExpr *>$$)->op);
        (yyval.exprtype) = (yyvsp[0].exprtype);
    }
#line 1569 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 28:
#line 663 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        printf("SUB UnaryExp\n");
        (yyval.exprtype) = new UnaryExpr(se, UnaryExpr::SUB, (yyvsp[0].exprtype));
        printf("unaryexpr op = %d\n", (dynamic_cast<UnaryExpr *>(yyval.exprtype))->op);
    }
#line 1580 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 671 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new UnaryExpr(se, UnaryExpr::NOT, (yyvsp[0].exprtype));
        printf("unaryexpr op = %d\n", (dynamic_cast<UnaryExpr *>(yyval.exprtype))->op);
    }
#line 1591 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 678 "src/parser.y" /* yacc.c:1646  */
    {
        //函数调用
        //FuncRParams为函数实参
        SymbolEntry *se;
        se = identifiers->lookup((yyvsp[-1].strtype));
        printf("parser.y--funccall ID = %s\n", (yyvsp[-1].strtype));
        printf("parser.y--IdEntryname = %s\n", ((IdentifierSymbolEntry*)se)->getname().c_str());

        //此处为在构建语法树的过程中进行的类型检查，若函数未声明则输出相应提示信息并退出程序
        if(se==nullptr)
        {
            fprintf(stderr,"Function undeclared!\n");
            exit(EXIT_FAILURE);
        }
        //assert(se!=nullptr);

        //检查是否返回值为void
        Type* idType=se->getType();
        if(!idType->isFunc())
        {
            fprintf(stderr,"expect a function!");
            exit(EXIT_FAILURE);
        }

        Type* retType=dynamic_cast<FunctionType*>(se->getType())->getRetType();
        SymbolEntry *tmp = new TemporarySymbolEntry(retType, SymbolTable::getLabel());
        call=new FuncCallExpr(se,tmp); 
        printf("parser.y--IdEntryname2 = %s\n", ((IdentifierSymbolEntry*)se)->getname().c_str());
        printf("funccallexpr1\n"); 
        //delete []$1;  
        //在解析参数时，call可能会被参数里面的call覆盖
    }
#line 1628 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 709 "src/parser.y" /* yacc.c:1646  */
    { (yyval.exprtype) = call;}
#line 1634 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 714 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype)=nullptr; }
#line 1640 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 33:
#line 716 "src/parser.y" /* yacc.c:1646  */
    {callBeforeExp.push_back(call);
    }
#line 1647 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 719 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.exprtype)=(yyvsp[0].exprtype);
        
    }
#line 1656 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 35:
#line 724 "src/parser.y" /* yacc.c:1646  */
    {callBeforeExp.push_back(call);}
#line 1662 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 725 "src/parser.y" /* yacc.c:1646  */
    {
       (yyval.exprtype)=(yyvsp[0].exprtype);
    }
#line 1670 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 37:
#line 733 "src/parser.y" /* yacc.c:1646  */
    {
        //不用创建新的类，只用每次插入就行

        //ExprNode *expr=$1;
        
        //创建新的变量
        //SymbolEntry *tmp = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        //SymbolEntry *se = expr->getSymPtr();

        //Operand* dst = expr->getOperand();//在call指令时在dst中写入调用
        if((yyvsp[0].exprtype)!=NULL)
        {
            call=*(callBeforeExp.rbegin());
            callBeforeExp.pop_back();
            call->addParam((yyvsp[0].exprtype));
            
        }

    }
#line 1694 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 38:
#line 756 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.exprtype) = (yyvsp[0].exprtype);
    }
#line 1702 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 39:
#line 759 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, (yyvsp[0].itype));
        // int t = (dynamic_cast<ConstantSymbolEntry *>(se))->getValue();
        // printf("constantsymbolentry!!!%d\n", t);
        (yyval.exprtype) = new Constant(se);
        // SymbolEntry *se2 = $$->getSymPtr();
        // t = (dynamic_cast<ConstantSymbolEntry *>(se2))->getValue();
        // printf("constantsymbolentry2!!!%d\n", t);
        printf("consttype size = %d\n", (dynamic_cast<Constant *>((yyval.exprtype)))->getType()->getSize());
    }
#line 1717 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 40:
#line 769 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.exprtype) = (yyvsp[-1].exprtype);
    }
#line 1725 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 776 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1731 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 42:
#line 779 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::ADD, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1740 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 785 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::SUB, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1749 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 44:
#line 792 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1755 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 795 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::MUL, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1764 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 46:
#line 801 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::DIV, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1773 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 47:
#line 807 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::MOD, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1782 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 48:
#line 814 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1788 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 49:
#line 817 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::LESS, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1798 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 50:
#line 824 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        printf("BinaryExpr::MORE\n");
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::MORE, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1809 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 832 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::NOMORE, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1819 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 52:
#line 839 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::NOLESS, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1829 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 847 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype)=(yyvsp[0].exprtype);}
#line 1835 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 54:
#line 849 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::EQUAL, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1845 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 55:
#line 856 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::NOTEQUAL, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1855 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 56:
#line 864 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1861 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 57:
#line 867 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::AND, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1871 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 875 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype) = (yyvsp[0].exprtype);}
#line 1877 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 59:
#line 878 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        (yyval.exprtype) = new BinaryExpr(se, BinaryExpr::OR, (yyvsp[-2].exprtype), (yyvsp[0].exprtype));
    }
#line 1887 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 60:
#line 885 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.type) = TypeSystem::intType;
    }
#line 1895 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 61:
#line 888 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.type) = TypeSystem::voidType;
    }
#line 1903 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 62:
#line 904 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=(yyvsp[0].stmttype);
    }
#line 1911 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 908 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=(yyvsp[0].stmttype);
    }
#line 1919 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 64:
#line 915 "src/parser.y" /* yacc.c:1646  */
    {
         (yyval.stmttype)=new ConstDeclStmt((yyvsp[-1].stmttype));
    }
#line 1927 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 922 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=new VarDeclStmt((yyvsp[-1].stmttype));
    }
#line 1935 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 929 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=new Defs((yyvsp[-2].stmttype),(yyvsp[0].stmttype));
    }
#line 1943 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 933 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=new Defs((yyvsp[0].stmttype));
    }
#line 1951 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 68:
#line 940 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=new Defs((yyvsp[-2].stmttype),(yyvsp[0].stmttype));
    }
#line 1959 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 944 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.stmttype)=new Defs((yyvsp[0].stmttype));
    }
#line 1967 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 951 "src/parser.y" /* yacc.c:1646  */
    {
        if(identifiers->isRedefine((yyvsp[0].strtype))) {
            fprintf(stderr, "identifier %s redefine\n", (yyvsp[0].strtype));
            exit(EXIT_FAILURE);
        }
        SymbolEntry *se;
        se = new IdentifierSymbolEntry(TypeSystem::intType ,(yyvsp[0].strtype), identifiers->getLevel());
        dynamic_cast<IdentifierSymbolEntry*>(se)->setvar();
        dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
        identifiers->install((yyvsp[0].strtype), se);
        //se->getType()->setType(32);
        se->getType()->size = 32;
        printf("check getsize\n");
        int s = se->getType()->getSize();
        printf("size = %d\n", s);
        (yyval.stmttype) = new Def(new Id(se));
        delete [](yyvsp[0].strtype);
    }
#line 1990 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 71:
#line 970 "src/parser.y" /* yacc.c:1646  */
    {
        if(identifiers->isRedefine((yyvsp[-2].strtype))) {
            fprintf(stderr, "identifier %s redefine\n", (yyvsp[-2].strtype));
            exit(EXIT_FAILURE);
        }
        SymbolEntry *se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, (yyvsp[-2].strtype), identifiers->getLevel());
        dynamic_cast<IdentifierSymbolEntry*>(se)->setvar();
        dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
        se->getType()->size = 32;
        identifiers->install((yyvsp[-2].strtype), se);
        (yyval.stmttype)=new Def(new Id(se),(yyvsp[0].exprtype));
        delete[](yyvsp[-2].strtype);
    }
#line 2009 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 988 "src/parser.y" /* yacc.c:1646  */
    {
        if(identifiers->isRedefine((yyvsp[-2].strtype))) {
            fprintf(stderr, "identifier %s redefine\n", (yyvsp[-2].strtype));
            exit(EXIT_FAILURE);
        }
        SymbolEntry *se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, (yyvsp[-2].strtype), identifiers->getLevel());
        dynamic_cast<IdentifierSymbolEntry*>(se)->setcon();
        dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
        identifiers->install((yyvsp[-2].strtype), se);
        (yyval.stmttype)=new Def(new Id(se),(yyvsp[0].exprtype));
        delete[](yyvsp[-2].strtype);
    }
#line 2027 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 1091 "src/parser.y" /* yacc.c:1646  */
    {
        Type *funcType;
        funcType = new FunctionType((yyvsp[-1].type),{});
        SymbolEntry *se = new IdentifierSymbolEntry(funcType, (yyvsp[0].strtype), identifiers->getLevel());
        identifiers->install((yyvsp[0].strtype), se);
        identifiers = new SymbolTable(identifiers);
        //$$ = new FunctionDef(se);
        //func=$$;
        func = new FunctionDef(se);
    }
#line 2042 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 74:
#line 1102 "src/parser.y" /* yacc.c:1646  */
    {
        


    }
#line 2052 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 1109 "src/parser.y" /* yacc.c:1646  */
    {
        func->setStmtNode((yyvsp[0].stmttype));
        identifiers = identifiers->getPrev();
        func->setRetStmt(ret);
        ret=nullptr;
        (yyval.stmttype)=func;
        delete [](yyvsp[-6].strtype);
    }
#line 2065 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 76:
#line 1121 "src/parser.y" /* yacc.c:1646  */
    {(yyval.exprtype)=nullptr;}
#line 2071 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 77:
#line 1124 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.exprtype)=(yyvsp[0].exprtype);
    }
#line 2079 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 78:
#line 1129 "src/parser.y" /* yacc.c:1646  */
    {
        (yyval.exprtype)=(yyvsp[0].exprtype);
    }
#line 2087 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 79:
#line 1137 "src/parser.y" /* yacc.c:1646  */
    {
        SymbolEntry *se;
        se = new IdentifierSymbolEntry((yyvsp[-1].type) ,(yyvsp[0].strtype), identifiers->getLevel());
        identifiers->install((yyvsp[0].strtype), se);
        func->addParam(new Id(se));
        SymbolEntry* se1=func->getSymPtr();
        FunctionType* type=dynamic_cast<FunctionType*>(se1->getType());
        type->addParamType((yyvsp[-1].type));
        delete [](yyvsp[0].strtype);
    }
#line 2102 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 80:
#line 1151 "src/parser.y" /* yacc.c:1646  */
    {}
#line 2108 "src/parser.cpp" /* yacc.c:1646  */
    break;

  case 81:
#line 1153 "src/parser.y" /* yacc.c:1646  */
    {(yyval.stmttype)=new EmptyStmt();}
#line 2114 "src/parser.cpp" /* yacc.c:1646  */
    break;


#line 2118 "src/parser.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1155 "src/parser.y" /* yacc.c:1906  */


int yyerror(char const* message)
{
    std::cerr<<message<<std::endl;
    return -1;
}
