#include "SymbolTable.h"
#include <iostream>
#include <sstream>

SymbolEntry::SymbolEntry(Type *type, int kind) 
{
    this->type = type;
    this->kind = kind;
}

ConstantSymbolEntry::ConstantSymbolEntry(Type *type, int value) : SymbolEntry(type, SymbolEntry::CONSTANT)
{
    this->value = value;
}

std::string ConstantSymbolEntry::toStr()
{
    std::ostringstream buffer;
    buffer << value;
    return buffer.str();
}

IdentifierSymbolEntry::IdentifierSymbolEntry(Type *type, std::string name, int scope) : SymbolEntry(type, SymbolEntry::VARIABLE), name(name)
{
    this->scope = scope;
    addr = nullptr;
    this->sysy = false;
}

// IdentifierSymbolEntry::IdentifierSymbolEntry(Type *type, std::string name, int scope, int sysy) : SymbolEntry(type, SymbolEntry::VARIABLE), name(name), sysy(sysy)
// {
//     this->scope = scope;
//     addr = nullptr;
//     //this->sysy = sysy;
// }

std::string IdentifierSymbolEntry::toStr()
{
    return "@" + name;
}

TemporarySymbolEntry::TemporarySymbolEntry(Type *type, int label) : SymbolEntry(type, SymbolEntry::TEMPORARY)
{
    this->label = label;
}

std::string TemporarySymbolEntry::toStr()
{
    std::ostringstream buffer;
    buffer << "%t" << label;
    return buffer.str();
}

SymbolTable::SymbolTable()
{
    prev = nullptr;
    level = 0;
}

SymbolTable::SymbolTable(SymbolTable *prev)
{
    this->prev = prev;
    this->level = prev->level + 1;
}

/*
    Description: lookup the symbol entry of an identifier in the symbol table
    Parameters: 
        name: identifier name
    Return: pointer to the symbol entry of the identifier

    hint:
    1. The symbol table is a stack. The top of the stack contains symbol entries in the current scope.
    2. Search the entry in the current symbol table at first.
    3. If it's not in the current table, search it in previous ones(along the 'prev' link).
    4. If you find the entry, return it.
    5. If you can't find it in all symbol tables, return nullptr.
*/
// SymbolEntry* SymbolTable::lookup(std::string name)
// {
//     // Todo
//     SymbolTable* symboltable = identifiers;
//     while(symboltable != nullptr){
//         SymbolEntry* res = symboltable->symbolTable[name];
//         if(res != nullptr){
//             return res;
//         }
//         symboltable = symboltable->prev;
//     }
//     return nullptr;
// }

SymbolEntry* SymbolTable::lookup(std::string name)
{
    //使用当前符号表
    SymbolTable* temp=identifiers;
    //逐层搜索符号表至全局符号表
    while(temp!=globals)
    {
        //搜索map
        if(temp->symbolTable.find(name)==temp->symbolTable.end())
        {
            //这一层没有，向上一层搜索
            temp=temp->getPrev();
        }
        else{
            //找到了
            return temp->symbolTable[name];
        }

    }
    //搜索全局符号表
    if(temp->symbolTable.find(name)==temp->symbolTable.end())
        {
            //这一层没有，向上一层搜索
            temp=temp->getPrev();
        }
        else{
            //找到了
            return temp->symbolTable[name];
        }
    return nullptr;
    return nullptr;
}
// bool SymbolTable::lookupLocal(std::string name)
// {
//     //使用当前符号表
//         SymbolTable* temp=identifiers;
//         //搜索map
//         if(temp->symbolTable.find(name)==temp->symbolTable.end())
//         {
//             //这一层没有，向上一层搜索
//             return false;
//         }
//         return true;
// }

// install the entry into current symbol table.
void SymbolTable::install(std::string name, SymbolEntry* entry)
{
    symbolTable[name] = entry;
}

bool SymbolTable::isRedefine(std::string name) {
    if(symbolTable.find(name)!=symbolTable.end()){
        return true;
    }
    else {
        return false;
    }
}

int SymbolTable::counter = 0;
static SymbolTable t;
SymbolTable *identifiers = &t;
SymbolTable *globals = &t;
