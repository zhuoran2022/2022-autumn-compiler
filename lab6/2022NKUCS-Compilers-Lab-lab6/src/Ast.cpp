#include "Ast.h"
#include "SymbolTable.h"
#include "Unit.h"
#include "Instruction.h"
#include "IRBuilder.h"
#include <string>
#include "Type.h"

extern FILE *yyout;
int Node::counter = 0;
IRBuilder* Node::builder = nullptr;
extern Unit unit;
Type* returnType = nullptr;
bool funcReturned = false;
int genBr = 0;

Node::Node()
{
    seq = counter++;
}

void Node::backPatch(std::vector<Instruction*> &list, BasicBlock*bb)
{
    for(auto &inst:list)
    {
        if(inst->isCond())
        {
            dynamic_cast<CondBrInstruction*>(inst)->setTrueBranch(bb);
            printf("backPatch isCond\n");
        }
        else if(inst->isUncond())
        {
            dynamic_cast<UncondBrInstruction*>(inst)->setBranch(bb);
            printf("backPatch isUnCond\n");
        }
    }
}

std::vector<Instruction*> Node::merge(std::vector<Instruction*> &list1, std::vector<Instruction*> &list2)
{
    std::vector<Instruction*> res(list1);
    res.insert(res.end(), list2.begin(), list2.end());
    return res;
}

UnaryExpr::UnaryExpr(SymbolEntry* se, int op, ExprNode* expr)
    : ExprNode(se, UNARYEXPR), op(op), expr(expr) {
    std::string op_str;// = op == UnaryExpr::NOT ? "!" : "-";
    switch(op)
    {
        case UnaryExpr::NOT:
            op_str = "!";
            break;
        case UnaryExpr::ADD:
            op_str = "+";
        case UnaryExpr::SUB:
            op_str = "-";
    }
    printf("UnaryExpr::UnaryExpr()\n");
    //below should be considerd later
    // if (expr->getType()->isVoid()) {
    //     fprintf(stderr,
    //             "invalid operand of type \'void\' to unary \'opeartor%s\'\n",
    //             op_str.c_str());
    // }
    if (expr->getType()->isVoid()) 
    {
        fprintf(stderr,"invalid operand of type \'void\' to unary \'opeartor%s\'\n",op_str.c_str());
    }
    if (op == UnaryExpr::NOT) {
        type = TypeSystem::intType;
        dst = new Operand(se);
        if (expr->isUnaryExpr()) {
            UnaryExpr* ue = (UnaryExpr*)expr;
            if (ue->getOp() == UnaryExpr::NOT) {
                if (ue->getType() == TypeSystem::intType)
                    ue->setType(TypeSystem::boolType);
                // type = TypeSystem::intType;
            }
        }
        // if (expr->getType()->isInt() && expr->getType()->getSize() == 32) {
        //     ImplictCastExpr* temp = new ImplictCastExpr(expr);
        //     this->expr = temp;
        // }
    } else if (op == UnaryExpr::SUB) {
        type = TypeSystem::intType;
        dst = new Operand(se);
        if (expr->isUnaryExpr()) {
            UnaryExpr* ue = (UnaryExpr*)expr;
            if (ue->getOp() == UnaryExpr::NOT)
                if (ue->getType() == TypeSystem::intType)
                    ue->setType(TypeSystem::boolType);
        }
    }
    else if(op == UnaryExpr::ADD)
    {
        type = TypeSystem::intType;
        dst = new Operand(se);
        if (expr->isUnaryExpr()) {
            UnaryExpr* ue = (UnaryExpr*)expr;
            if (ue->getOp() == UnaryExpr::NOT)
                if (ue->getType() == TypeSystem::intType)
                    ue->setType(TypeSystem::boolType);
        }
    }
};

void Ast::genCode(Unit *unit)
{
    IRBuilder *builder = new IRBuilder(unit);
    Node::setIRBuilder(builder);
    root->genCode();//?
    printf("Ast::genCode()\n");
}

void FunctionDef::genCode()
{
    //printf("functiondef--gencode()\n");
    printf("FuncDef::genCode()\n");
    Unit *unit = builder->getUnit();
    Function *func = new Function(unit, se);
    BasicBlock *entry = func->getEntry();
    // set the insert point to the entry basicblock of this function.
    builder->setInsertBB(entry);
    //添加为形参变量申请空间的语句
    //---------------------------------------
    int i=0;
    for(auto it=params.begin();it!=params.end();it++,++i)
    {
        Id* id=(*it);
        IdentifierSymbolEntry *se = dynamic_cast<IdentifierSymbolEntry *>(id->getSymPtr());
        Function *func = builder->getInsertBB()->getParent();
        BasicBlock *entry = func->getEntry();
        Instruction *alloca;
        Operand *addr;
        SymbolEntry *addr_se;
        Type *type;
        type = new PointerType(se->getType());
        addr_se = new TemporarySymbolEntry(type, SymbolTable::getLabel());
        addr = new Operand(addr_se);
        alloca = new AllocaInstruction(addr, se); 
        std::string temp="%"+std::to_string(i); 
        //给函数形参分配空间
        //对于中间代码是ALLOCA指令以及STORE指令，先分配空间再存值
        //对于目标代码是压栈指令，将特殊约定的寄存器压栈，超过4个不仅是压栈，还要将栈正向偏移的存入栈的负偏移
       new FuncStoreInstruction(addr,temp,i,entry);                 
       entry->insertFront(alloca);    // allocate instructions should be inserted into the begin of the entry block.
        se->setAddr(addr);  
        
    }

    stmt->genCode();
    
    /**
     * Construct control flow graph. You need do set successors and predecessors for each basic block.
     * Todo
    */

    // 获取该块的最后一条指令
    Instruction* last = (entry)->rbegin();
    int k = 0;
    // 遍历Function中所有的BasicBlock，在各个BasicBlock之间建立控制流关系
    for (auto block = func->begin(); block != func->end(); block++) {
        // 获取该块的最后一条指令
        Instruction* last = (*block)->rbegin();
        // 对于有条件的跳转指令，需要对其true分支和false分支都设置控制流关系
        if (last->isCond()) {
            //printf("1-1\n");
            printf("1-1 %d\n", k++);
            BasicBlock *trueBlock = dynamic_cast<CondBrInstruction*>(last)->getTrueBranch();//if BinaryExpr::genCode() update cond->truelist(), then backpatch for the truelist to set TrueBranch, and here we get the true TrueBranch, that's why lost 3 lines of code in BinaryExpr::genCode() cause the problem
            BasicBlock *falseBlock = dynamic_cast<CondBrInstruction*>(last)->getFalseBranch();
            if (trueBlock->empty()) {
                new RetInstruction(nullptr, trueBlock);

            } else if (falseBlock->empty()) {
                new RetInstruction(nullptr, falseBlock);
            }
            (*block)->addSucc(trueBlock);
            (*block)->addSucc(falseBlock);
            trueBlock->addPred(*block);
            falseBlock->addPred(*block);
        } 
        // 对于无条件的跳转指令，只需要对其目标基本块设置控制流关系即可
        else if (last->isUncond()) {
            printf("2-2 %d\n", k++);
            BasicBlock* dstBlock = dynamic_cast<UncondBrInstruction*>(last)->getBranch();
            if(*block == nullptr)
            {
                printf("block == nullptr\n");
            }
            (*block)->addSucc(dstBlock);
            if(dstBlock == nullptr)
            {
                printf("dstBlock == nullptr\n");
            }
            else
            {
            dstBlock->addPred(*block);
            if (dstBlock->empty()) 
            {
                if (((FunctionType*)(se->getType()))->getRetType() == TypeSystem::intType)
                    new RetInstruction(new Operand(new ConstantSymbolEntry(TypeSystem::intType, 0)),dstBlock);
                else if (((FunctionType*)(se->getType()))->getRetType() == TypeSystem::voidType)
                    new RetInstruction(nullptr, dstBlock);
            }
            }
        }
    }

}

BinaryExpr::BinaryExpr(SymbolEntry* se,
                       int op,
                       ExprNode* expr1,
                       ExprNode* expr2)
    : ExprNode(se), op(op), expr1(expr1), expr2(expr2) {
    dst = new Operand(se);
    std::string type1 = dst->getType()->toStr();
    printf("i1problem ast.cpp:186 type = %s\n", type1.c_str());
    std::string op_str;
    switch (op) {
        case ADD:
            op_str = "+";
            break;
        case SUB:
            op_str = "-";
            break;
        case MUL:
            op_str = "*";
            break;
        case DIV:
            op_str = "/";
            break;
        case MOD:
            op_str = "%";
            break;
        case AND:
            op_str = "&&";
            break;
        case OR:
            op_str = "||";
            break;
        case LESS:
            op_str = "<";
            break;
        case NOMORE:
            op_str = "<=";
            break;
        case MORE:
            op_str = ">";
            break;
        case NOLESS:
            op_str = ">=";
            break;
        case EQUAL:
            op_str = "==";
            break;
        case NOTEQUAL:
            op_str = "!=";
            break;
    }
    printf("op = %d\n", op);
    printf("BinaryExpr::AND = %d, BinaryExpr::NOTEQUAL = %d\n", BinaryExpr::AND, BinaryExpr::NOTEQUAL);
    if (expr1->getType()->isVoid() || expr2->getType()->isVoid()) 
    {
        fprintf(stderr,"invalid operand of type \'void\' to binary \'opeartor%s\'\n",op_str.c_str());
    }
    if (op >= BinaryExpr::AND && op <= BinaryExpr::NOTEQUAL) {
        printf("11111\n");
        type = TypeSystem::boolType;
        if (op == BinaryExpr::AND || op == BinaryExpr::OR) {
            printf("22222\n");
            if (expr1->getType()->isInt() &&
                expr1->getType()->getSize() == 32) {
                printf("33333\n");
                ImplictCastExpr* temp = new ImplictCastExpr(expr1);
                this->expr1 = temp;
            }
            if (expr2->getType()->isInt() &&
                expr2->getType()->getSize() == 32) {
                printf("44444\n");
                ImplictCastExpr* temp = new ImplictCastExpr(expr2);
                this->expr2 = temp;
            }
        }
    } else{
        printf("55555\n");
        type = TypeSystem::intType;
    }
    //std::string type = dst->getType()->toStr();
    printf("i1problem ast.cpp:186 type = %s\n", type->toStr().c_str());
    // std::string type1 = dst->getType()->toStr();
    // printf("i1problem ast.cpp:186 type = %s\n", type1.c_str());
};

void BinaryExpr::genCode()
{

    printf("BinaryExpr::genCode\n");
    BasicBlock *bb = builder->getInsertBB();
    Function *func = bb->getParent();
    if (op == AND)
    {
        genBr = 1;
        BasicBlock *trueBB = new BasicBlock(func);  // if the result of lhs is true, jump to the trueBB.
        expr1->genCode();
        printf("before backPatch in AND\n");
        backPatch(expr1->trueList(), trueBB);
        builder->setInsertBB(trueBB);               // set the insert point to the trueBB so that intructions generated by expr2 will be inserted into it.
        expr2->genCode();
        true_list = expr2->trueList();
        false_list = merge(expr1->falseList(), expr2->falseList());
    }
    else if(op == OR)
    {
        // Todo
        genBr = 1;
        BasicBlock *trueBB = new BasicBlock(func);  // if the result of lhs is true, jump to the trueBB.
        expr1->genCode();
        printf("before backPatch in OR\n");
        backPatch(expr1->falseList(), trueBB);//it should be falseList!
        builder->setInsertBB(trueBB);               // set the insert point to the trueBB so that intructions generated by expr2 will be inserted into it.
        expr2->genCode();
        true_list = merge(expr1->trueList(), expr2->trueList());
        false_list = expr2->falseList();
    }
    else if(op >= LESS && op <= NOTEQUAL)
    {
        // Todo
        genBr--;
        expr1->genCode();
        expr2->genCode();
        Operand *src1 = expr1->getOperand();
        Operand *src2 = expr2->getOperand();
        genBr++;

        //bool->int
        printf("bool->int1");
        if(src1->getType()==TypeSystem::boolType)
        {
            printf("bool->int2");
            BasicBlock *bb = builder->getInsertBB();
            SymbolEntry*se=new TemporarySymbolEntry(TypeSystem::intType,SymbolTable::getLabel()); 
            Operand *src1_new=new Operand(se);
            new ZextInstruction(src1_new, src1, bb);
            src1 =src1_new;
        }
        if(src2->getType()==TypeSystem::boolType)
        {
            printf("bool->int3");
            BasicBlock *bb = builder->getInsertBB();
            SymbolEntry*se=new TemporarySymbolEntry(TypeSystem::intType,SymbolTable::getLabel()); 
            Operand *src2_new=new Operand(se);
            new ZextInstruction(src2_new, src2, bb);
            src2 =src2_new;
        }

        int opcode;
        switch (op)
        {
        case LESS:
            opcode = CmpInstruction::L;
            break;
        case NOLESS:
            opcode = CmpInstruction::GE;
            break;
        case MORE:
            opcode = CmpInstruction::G;
            break;
        case NOMORE:
            opcode = CmpInstruction::LE;
            break;
        case EQUAL:
            opcode = CmpInstruction::E;
            break;
        case NOTEQUAL:
            opcode = CmpInstruction::NE;
            break;
        }
        std::string type = dst->getType()->toStr();
        printf("i1problem ast.cpp type = %s\n", type.c_str());
        new CmpInstruction(opcode, dst, src1, src2, builder->getInsertBB());
        if(genBr>0)
        {
        BasicBlock *truebb, *falsebb, *tempbb;
        //cost an afternoon because of the loss of below code
        truebb = new BasicBlock(func);
        falsebb = new BasicBlock(func);
        //truebb = nullptr;
        //falsebb = nullptr;
        tempbb = new BasicBlock(func);

        true_list.push_back(new CondBrInstruction(truebb, tempbb, dst, bb));//insert condBr into bb; true_list is owned by class Node
        false_list.push_back(new UncondBrInstruction(falsebb, tempbb));//don't insert to bb, because binaryexpr is used by condBr, uncondBr don't use it
        //true_list.push_back(new CondBrInstruction(truebb, falsebb, dst, bb));
        }
    }
    else if(op >= ADD && op <= MOD)
    {
        expr1->genCode();
        expr2->genCode();
        Operand *src1 = expr1->getOperand();
        Operand *src2 = expr2->getOperand();
        int opcode;
        switch (op)
        {
        case ADD:
            opcode = BinaryInstruction::ADD;
            break;
        case SUB:
            opcode = BinaryInstruction::SUB;
            break;
        case MUL:
            opcode = BinaryInstruction::MUL;
            break;
        case DIV:
            opcode = BinaryInstruction::DIV;
            break;
        case MOD:
            opcode = BinaryInstruction::MOD;
            break;
        }
        new BinaryInstruction(opcode, dst, src1, src2, bb);
    }
}

void Constant::genCode()
{
    // we don't need to generate code.
    printf("Constant--gencode()\n");
}

void Id::genCode()
{
    printf("Id::genCode()\n");
    BasicBlock *bb = builder->getInsertBB();
    Operand *addr = dynamic_cast<IdentifierSymbolEntry*>(symbolEntry)->getAddr();
    new LoadInstruction(dst, addr, bb);//why load? Because load the Id we need from mem.
}

void IfStmt::genCode()
{
    printf("IfStmt::genCode()\n");
    Function *func;
    BasicBlock *then_bb, *end_bb;

    func = builder->getInsertBB()->getParent();
    then_bb = new BasicBlock(func);
    end_bb = new BasicBlock(func);

    genBr = 1;

    printf("ifstmt cond->genCode\n");
    cond->genCode();
    printf("!!ifstmt cond->genCode end\n");
    backPatch(cond->trueList(), then_bb);
    printf("backPatch1 end\n");
    backPatch(cond->falseList(), end_bb);
    printf("backPatch2 end\n");

    builder->setInsertBB(then_bb);
    thenStmt->genCode();
    then_bb = builder->getInsertBB();
    new UncondBrInstruction(end_bb, then_bb);//why uncond?

    builder->setInsertBB(end_bb);
}

void IfElseStmt::genCode()
{
    // Todo
    printf("IfElseStmt::genCode()\n");
    Function *func;
    BasicBlock *then_bb, *else_bb, *end_bb;

    func = builder->getInsertBB()->getParent();
    then_bb = new BasicBlock(func);
    else_bb = new BasicBlock(func);
    end_bb = new BasicBlock(func);
    
    genBr = 1;

    printf("cond->genCode() begin;\n");
    cond->genCode();
    printf("cond->genCode();\n");
    backPatch(cond->trueList(), then_bb);//cond->truelist is added a condBrInstruction in BinaryExpr::genCode() called by cond->genCode() above
    backPatch(cond->falseList(), else_bb);

    builder->setInsertBB(then_bb);
    thenStmt->genCode();
    then_bb = builder->getInsertBB();
    new UncondBrInstruction(end_bb, then_bb);//why uncond?

    builder->setInsertBB(else_bb);
    elseStmt->genCode();
    else_bb = builder->getInsertBB();
    new UncondBrInstruction(end_bb, else_bb);

    builder->setInsertBB(end_bb);
}

void CompoundStmt::genCode()
{
    // Todo
    printf("compoundstmt--gencode()\n");
    if(stmt!=nullptr)
    {
        stmt->genCode();
    }

}

void SeqNode::genCode()
{
    // Todo
    printf("seqnode--gencode()\n");
    stmt1->genCode();
    stmt2->genCode();
}


void VarDeclStmt::genCode()
{
    printf("vardeclstmt--gencode()\n");
    defs->genCode();
    
}

void Defs::genCode()
{
    printf("defs--gencode()\n");
    def->genCode();
    if(defs != nullptr){
        defs->genCode();
    }
}

void Def::genCode()
{
    printf("def::genCode()\n");
    IdentifierSymbolEntry *se = dynamic_cast<IdentifierSymbolEntry *>(id->getSymPtr());
    if(se->isGlobal())
    {
        if(expr != nullptr)
        {
            //printf("GLOBAL--expr!=nullptr\n");
            SymbolEntry* expr_se = expr->getSymPtr();
            int t = (dynamic_cast<ConstantSymbolEntry *>(expr_se))->getValue();
            //se->setValue(t);
            //printf("constantsymbolentry in genCode!!!%d\n", t);
            se->value = t;
        }
        Operand *addr;
        SymbolEntry *addr_se;
        addr_se = new IdentifierSymbolEntry(*se);//why you can new IdentifierSymbolEntry this way?
        addr_se->setType(new PointerType(se->getType()));
        addr = new Operand(addr_se);
        se->setAddr(addr);
        unit.insertGlobal(se);

    }
    else if(se->isLocal())
    {
        Function *func = builder->getInsertBB()->getParent();
        BasicBlock *entry = func->getEntry();
        Instruction *alloca;
        Operand *addr;
        SymbolEntry *addr_se;
        Type *type;
        type = new PointerType(se->getType());
        addr_se = new TemporarySymbolEntry(type, SymbolTable::getLabel());
        addr = new Operand(addr_se);
        alloca = new AllocaInstruction(addr, se);                   // allocate space for local id in function stack.
        entry->insertFront(alloca);                                 // allocate instructions should be inserted into the begin of the entry block.
        se->setAddr(addr);                                          // set the addr operand in symbol entry so that we can use it in subsequent code generation.

        if(expr != nullptr)
        {
            BasicBlock* bb = builder->getInsertBB();
            expr->genCode();
            Operand* src = expr->getOperand();
            printf("where new storeinstruction\n");
            src->getType()->toStr();
            new StoreInstruction(addr, src, bb);
        }
    }
}

void ConstDeclStmt::genCode()
{
    printf("constdeclstmt--gencode()\n");
    defs->genCode();
}

void ReturnStmt::genCode()
{
    // Todo
    printf("ReturnStmt::genCode()\n");
    BasicBlock *bb = builder->getInsertBB();
    if(retValue!=nullptr)
    {
    retValue->genCode();
    Operand *src = retValue->getOperand();
    new RetInstruction(src, bb);
    }
    else{//void -- return
        new RetInstruction(nullptr,bb);
        
    }
}

void AssignStmt::genCode()
{
    printf("AssignStmt::genCode()\n");
    BasicBlock *bb = builder->getInsertBB();
    expr->genCode();
    Operand *addr = dynamic_cast<IdentifierSymbolEntry*>(lval->getSymPtr())->getAddr();
    Operand *src = expr->getOperand();
    if(src == nullptr){
        printf("src == nullptr\n");
    }
    /***
     * We haven't implemented array yet, the lval can only be ID. So we just store the result of the `expr` to the addr of the id.
     * If you want to implement array, you have to caculate the address first and then store the result into it.
     */
    ((FuncCallExpr*)expr)->printme();
    printf("where new storeinstruction633\n");
            src->getType()->toStr();
    new StoreInstruction(addr, src, bb);
    printf("AssignStmt::genCode() break point\n");
}

void UnaryExpr::genCode()
{
    printf("unaryexpr--gencode()\n");
    printf("unaryexpr op = %d\n", op);
    expr->genCode();
    if(op == UnaryExpr::ADD)
    {
        printf("unary in add\n");
        // Operand* src2;
        // BasicBlock* bb = builder->getInsertBB();
        // Operand* src1 = new Operand(new ConstantSymbolEntry(TypeSystem::intType, 0));
        // if (expr->getType()->getSize() == 1) {
        //     src2 = new Operand(new TemporarySymbolEntry(
        //         TypeSystem::intType, SymbolTable::getLabel()));
        //     new ZextInstruction(src2, expr->getOperand(), bb);
        // } else
        //     src2 = expr->getOperand();
        // new BinaryInstruction(BinaryInstruction::ADD, dst, src1, src2, bb);
    }
    else if(op == UnaryExpr::SUB)
    {
        printf("unary in sub\n");
        Operand* src2;
        BasicBlock* bb = builder->getInsertBB();
        Operand* src1 =
            new Operand(new ConstantSymbolEntry(TypeSystem::intType, 0));
        expr->getType()->getSize();
        printf("unary in sub break point\n");
            if (expr->getType()->getSize() == 1) {
                src2 = new Operand(new TemporarySymbolEntry(
                    TypeSystem::intType, SymbolTable::getLabel()));
                new ZextInstruction(src2, expr->getOperand(), bb);
            } else
            src2 = expr->getOperand();
        new BinaryInstruction(BinaryInstruction::SUB, dst, src1, src2, bb);
    printf("UnaryExpr::genCode\n");
    }
    else if(op == UnaryExpr::NOT)
    {
        
        BasicBlock* bb = builder->getInsertBB();
        Operand* src = expr->getOperand();
        expr->getType();//->getSize();
        printf("unary in not\n");
        if (expr->getType()->getSize() == 32) {
            printf("unary not size == 32\n");
            Operand* temp = new Operand(new TemporarySymbolEntry(
                TypeSystem::boolType, SymbolTable::getLabel()));
            new CmpInstruction(
                CmpInstruction::NE, temp, src,
                new Operand(new ConstantSymbolEntry(TypeSystem::intType, 0)),
                bb);
            src = temp;
        }
        new XorInstruction(dst, src, bb);
    }
}

int UnaryExpr::getValue() {
    int value;
    switch (op) {
        case NOT:
            value = !(expr->getValue());
            break;
        case SUB:
            value = -(expr->getValue());
            break;
    }
    return value;
}

void FuncCallParamsnode::genCode(){
    //Todo
    printf("FuncCallParamsnode--gencode()\n");
}


/*FuncCallExpr::FuncCallExpr(SymbolEntry *se,Id*id,FuncCallParamsnode* params)
 : ExprNode(se), funcId(id), params(params)
{
    if (symbolEntry) {
        Type* type = symbolEntry->getType();
        this->type = ((FunctionType*)type)->getRetType();
        if (this->type != TypeSystem::voidType) {
            SymbolEntry* se =
                new TemporarySymbolEntry(this->type, SymbolTable::getLabel());
            dst = new Operand(se);
        }
        std::vector<Type*> params = ((FunctionType*)type)->getParamsType();
        //ExprNode* temp = param;
        // for (auto it = params.begin(); it != params.end(); it++) {
        //     if (temp == nullptr) {
        //         fprintf(stderr, "too few arguments to function %s %s\n",
        //                 symbolEntry->toStr().c_str(), type->toStr().c_str());
        //         break;
        //     } else if ((*it)->getKind() != temp->getType()->getKind())
        //         fprintf(stderr, "parameter's type %s can't convert to %s\n",
        //                 temp->getType()->toStr().c_str(),
        //                 (*it)->toStr().c_str());
        //     temp = (ExprNode*)(temp->getNext());
        // }
        // if (temp != nullptr) {
        //     fprintf(stderr, "too many arguments to function %s %s\n",
        //             symbolEntry->toStr().c_str(), type->toStr().c_str());
        // }
    }
    //if (((IdentifierSymbolEntry*)se)->isSysy()) {
        unit.insertDeclaration(se);
    printf("FuncCallExpr::FuncCallExpr\n");
    //}
}*/

void FuncCallExpr::genCode(){
    //Todo
    printf("FuncCallExpr--gencode()\n");
    FunctionType* ff=dynamic_cast<FunctionType*>(Funcname->getType());
    std::vector<Type*> fparams=ff->getParamsType();
    std::vector<ExprNode*>::iterator it2=params.begin();
    std::vector<Type*>::iterator it=fparams.begin();
    

    for(;it!=fparams.end()&&it2!=params.end();++it,++it2)
    {
       if((*it)->getKind()!=(*it2)->getType()->getKind())
        {
            fprintf(stderr,"function undefinition!\n");
            exit(EXIT_FAILURE);
        }
    }

    if(it!=fparams.end()||it2!=params.end())
    {
      fprintf(stderr,"Function parameters do not match!\n");
        exit(EXIT_FAILURE);
    }


    BasicBlock *bb = builder->getInsertBB();

    Function* parent =bb->getParent();
    parent->setUnleaf();
    
    std::vector<Operand*> src;
    for(auto it=params.begin();it!=params.end();++it)
    {
       (*it)->genCode();
       src.push_back((*it)->getOperand());
    }
    new CallInstruction(Funcname,dst,src,bb);//传入实参链表和函数名即可    new CallInstruction(Funcname,dst,src,bb);//传入实参链表和函数名即可
    // if (((IdentifierSymbolEntry*)se)->isSysy()) {
    //     unit.insertDeclaration(se);
    // }
}


void FuncParam::genCode(){
    //Todo
    printf("FuncParam--gencode()\n");
}


void FuncParamsnode::genCode(){
    //Todo
    printf("FuncParamsnode--gencode()\n");
}

void EmptyStmt::genCode(){
    //Todo
    printf("EmptyStmt--gencode()\n");
}

void WhileStmt::genCode()
{
    printf("WHileStmt--gencode()\n");

    // 将当前的whileStmt压栈
    //whileStack.push(this);
    Function* func = builder->getInsertBB()->getParent();
    BasicBlock* stmt_bb, *cond_bb, *end_bb, *bb = builder->getInsertBB();
    stmt_bb = new BasicBlock(func);
    cond_bb = new BasicBlock(func);
    end_bb = new BasicBlock(func);

    this->condBlock = cond_bb;
    this->endBlock = end_bb;

    // 先从当前的bb跳转到cond_bb进行条件判断
    new UncondBrInstruction(cond_bb, bb);

    // 调整插入点到cond_bb，对条件判断部分生成中间代码
    builder->setInsertBB(cond_bb);
    genBr = 1;
    cond->genCode();

    backPatch(cond->trueList(), stmt_bb);
    backPatch(cond->falseList(), end_bb);

    // 调整插入点到stmt_bb，对循环体部分生成中间代码
    builder->setInsertBB(stmt_bb);
    stmt->genCode();
    // 循环体完成之后，增加一句无条件跳转到cond_bb
    stmt_bb = builder->getInsertBB();
    new UncondBrInstruction(cond_bb, stmt_bb);
    // 重新调整插入点到end_bb
    builder->setInsertBB(end_bb);
    // 将当前的whileStmt出栈
    //whileStack.pop();
}

void Ast::typeCheck()
{
    if(root != nullptr)
        root->typeCheck();
}



void FunctionDef::typeCheck()
{
     // 获取函数的返回值类型
    returnType = ((FunctionType*)se->getType())->getRetType();
    // 判断函数是否返回
    funcReturned = false;
    stmt->typeCheck();
    // 非void类型的函数需要有返回值
    if(!funcReturned && !returnType->isVoid()){
        fprintf(stderr, "expected a %s type to return, but no returned value found\n", returnType->toStr().c_str());
        exit(EXIT_FAILURE);
    }
    // 如果void类型没写return需要补上
    if(!funcReturned && returnType->isVoid()) {
        this->ret = new ReturnStmt(nullptr);
    }
    returnType = nullptr;
}


void BinaryExpr::typeCheck()
{
    //Todo
}
void FuncParamsnode::typeCheck(){
    //Todo
}

void FuncCallParamsnode::typeCheck(){
    //Todo
}

void WhileStmt::typeCheck()
{
    // Todo
}

void Constant::typeCheck()
{
    // Todo
    printf("%s","Constant::typeCheck\n");
}

void Id::typeCheck()
{
    // Todo
     printf("%s","Id::typeCheck\n");
}

void IfStmt::typeCheck()
{
    // Todo
    printf("%s","IfStmt::typeCheck\n");
    cond->typeCheck();
    if(thenStmt!=nullptr) {
        thenStmt->typeCheck();
    }
    else {
        thenStmt = new EmptyStmt();
    }
}

void IfElseStmt::typeCheck()
{
    // Todo
    printf("%s","IfElseStmt::typeCheck\n");
    cond->typeCheck();
    if(thenStmt!=nullptr){
        if(elseStmt!=nullptr){
            elseStmt->typeCheck();
        }
        thenStmt->typeCheck();
    }
    else{
        fprintf(stderr,"ifelse type wrong");
        exit(EXIT_FAILURE);
    }
}

void CompoundStmt::typeCheck()
{
    // Todo
    printf("%s","CompoundStmt::typeCheck\n");
    if(stmt!=nullptr){
        stmt->typeCheck();
    }
}

void SeqNode::typeCheck()
{
    // Todo
   stmt1->typeCheck();
   stmt2->typeCheck();

}

void FuncCallExpr::typeCheck()
{
    // Todo
    
}
// void DeclStmt::typeCheck()
// {
//     // Todo
// }

void VarDeclStmt::typeCheck()
{
    //Todo
    
}

void ConstDeclStmt::typeCheck()
{
    //Todo
}

void Defs::typeCheck()
{
    //Todo
}

void Def::typeCheck()
{
    //Todo
}

void ReturnStmt::typeCheck()
{
    // Todo
    if(returnType == nullptr){//not in a fuction
        fprintf(stderr, "return statement outside functions\n");
        exit(EXIT_FAILURE);
    }
    else if(returnType->isVoid() && retValue!=nullptr){//returned a value in void()
        fprintf(stderr, "value returned in a void() function\n");
        exit(EXIT_FAILURE);
    }
    else if(!returnType->isVoid() && retValue==nullptr){//expected returned value, but returned nothing
        fprintf(stderr, "expected a %s type to return, but returned nothing\n", returnType->toStr().c_str());
        exit(EXIT_FAILURE);
    }
    if(!returnType->isVoid()){
        retValue->typeCheck();
    }
    this->retType = returnType;
    funcReturned = true;
}

void AssignStmt::typeCheck()
{
    // Todo
    lval->typeCheck();
    expr->typeCheck();

    if(expr->getType()->isFunc() && ((FunctionType*)(expr->getType()))->getRetType()->isVoid()){//返回值为void的函数做运算数
        fprintf(stderr, "expected a return value, but functionType %s returns nothing\n", expr->getType()->toStr().c_str());
        exit(EXIT_FAILURE);
    }
}

void UnaryExpr::typeCheck() {
    //return false;
    //Todo
}

void FuncParam::typeCheck(){
    //Todo
}

void EmptyStmt::typeCheck(){
    //Todo
}

void BinaryExpr::output(int level)
{
    std::string op_str;
    switch(op)
    {
        case ADD:
            op_str = "add";
            break;
        case SUB:
            op_str = "sub";
            break;
        case AND:
            op_str = "and";
            break;
        case OR:
            op_str = "or";
            break;
        case LESS:
            op_str = "less";
            break;
    }
    fprintf(yyout, "%*cBinaryExpr\top: %s\n", level, ' ', op_str.c_str());
    expr1->output(level + 4);
    expr2->output(level + 4);
}

void Ast::output()
{
    fprintf(yyout, "program\n");
    if(root != nullptr)
        root->output(4);
}

void Constant::output(int level)
{
    std::string type, value;
    type = symbolEntry->getType()->toStr();
    value = symbolEntry->toStr();
    fprintf(yyout, "%*cIntegerLiteral\tvalue: %s\ttype: %s\n", level, ' ',
            value.c_str(), type.c_str());
}

void Id::output(int level)
{
    std::string name, type;
    int scope;
    name = symbolEntry->toStr();
    type = symbolEntry->getType()->toStr();
    scope = dynamic_cast<IdentifierSymbolEntry*>(symbolEntry)->getScope();
    fprintf(yyout, "%*cId\tname: %s\tscope: %d\ttype: %s\n", level, ' ',
            name.c_str(), scope, type.c_str());
}

void CompoundStmt::output(int level)
{
    fprintf(yyout, "%*cCompoundStmt\n", level, ' ');
    stmt->output(level + 4);
}

void SeqNode::output(int level)
{
    stmt1->output(level);
    stmt2->output(level);
}

// void DeclStmt::output(int level)
// {
//     fprintf(yyout, "%*cDeclStmt\n", level, ' ');
//     id->output(level + 4);
// }

void Def::output(int level)
{
    fprintf(yyout, "%*cDefStmt\n", level, ' ');
    id->output(level+4);
    if(expr!=0)
    {
        expr->output(level+4);
    }
}

void Defs::output(int level)
{
    def->output(level);
    if(defs!=0)
    {
        defs->output(level);
    }
}

void ConstDeclStmt::output(int level)
{
    fprintf(yyout, "%*cConstDeclStmt\n", level, ' ');
    defs->output(level+4);
}

void VarDeclStmt::output(int level)
{
    fprintf(yyout, "%*cVarDeclStmt\n", level, ' ');
    defs->output(level+4);
}

void IfStmt::output(int level)
{
    fprintf(yyout, "%*cIfStmt\n", level, ' ');
    cond->output(level + 4);
    thenStmt->output(level + 4);
}

void IfElseStmt::output(int level)
{
    fprintf(yyout, "%*cIfElseStmt\n", level, ' ');
    cond->output(level + 4);
    thenStmt->output(level + 4);
    elseStmt->output(level + 4);
}

void ReturnStmt::output(int level)
{
    fprintf(yyout, "%*cReturnStmt\n", level, ' ');
    retValue->output(level + 4);
}

void EmptyStmt::output(int level)
{
    fprintf(yyout,"%*cEmptyStmt\n",level,' ');
}

void AssignStmt::output(int level)
{
    fprintf(yyout, "%*cAssignStmt\n", level, ' ');
    lval->output(level + 4);
    expr->output(level + 4);
}

void FunctionDef::output(int level)
{
    // std::string name, type;
    // name = se->toStr();
    // type = se->getType()->toStr();
    // fprintf(yyout, "%*cFunctionDefine function name: %s, type: %s\n", level, ' ', 
    //         name.c_str(), type.c_str());
    // stmt->output(level + 4);

    printf("FunctionDef\n");
    std::string name, type;
    name = se->toStr();
    type = se->getType()->toStr();
    fprintf(yyout, "%*cFunctionDefine function name: %s, type: %s\n", level, ' ', 
            name.c_str(), type.c_str());
    if(params.size()!=0)
    {
        for(auto it=params.begin();it!=params.end();it++)
        {
            (*it)->output(level+4);
        }
    }
    stmt->output(level + 4);
}

void UnaryExpr::output(int level) {
    std::string op_str;
    switch (op) {
        case NOT:
            op_str = "not";
            break;
        case SUB:
            op_str = "minus";
            break;
    }
    fprintf(yyout, "%*cUnaryExpr\top: %s\ttype: %s\n", level, ' ',
            op_str.c_str(), type->toStr().c_str());
    expr->output(level + 4);
}

void FuncParamsnode::output(int level)
{
    fprintf(yyout, "%*cFuncParamsnode\n", level, ' '); 
    for(std::vector<Id*>::iterator i=paramsList.begin(); i!= paramsList.end();i++)
    {
        (*i)->output(level+4);
    }
}

Id* FuncParam::getid()
{
    return id;
}

void FuncParamsnode::append(Id*next)
{
    paramsList.push_back(next);
}

void FuncParam::output(int level)
{
    fprintf(yyout, "%*cFuncParam\n", level,' ');
}

std::vector<Type *> FuncParamsnode::getParamsType()
{
    std::vector<Type *> typelist;
    for(std::vector<Id*>::iterator i=paramsList.begin(); i!= paramsList.end();i++)
    {
        typelist.push_back(TypeSystem::intType);//加入浮点数，数组，指针之后要修改一下这个位置
    }
    return typelist;
}

void FuncCallExpr::output(int level)
{
    // std::string name,type;
    // SymbolEntry*se=funcId->getSymbolEntry();//在符号表中找到函数id对应的入口地址 
    // name=se->toStr();
    // type=se->getType()->toStr();
    // int scope=dynamic_cast<IdentifierSymbolEntry*>(se)->getScope();
    // fprintf(yyout,"%*cFunccallNode\tfuncName:%s\t funcType:%s\tscope: %d\n",
    //         level,' ', name.c_str(),type.c_str(),scope);
    // if(params!=nullptr)
    //     params->output(level+4); 
    // else
    //     fprintf(yyout,"%*cFunccallParamsNode NULL\n", level+4,' ') ;

    printf("funccallexpr::output\n");

    fprintf(yyout, "%*cFuncCallExpr\top: func\n", level, ' ');
    //expr1->output(level + 4);

    std::string name,type;
    int scope;
    name = symbolEntry->toStr();//获得当前ID的名字
    type = symbolEntry->getType()->toStr();
    scope = dynamic_cast<IdentifierSymbolEntry*>(symbolEntry)->getScope();
    fprintf(yyout, "%*cFunction\tname: %s\tscope: %d\ttype: %s \n", level, ' ',
            name.c_str(), scope, type.c_str());

    //if(expr1!=nullptr){expr1->output(level + 4);}
    return;
}

void FuncCallParamsnode::append(ExprNode* next)
{
    paramsList.push_back(next);
}

void FuncCallParamsnode::output(int level)
{
    fprintf(yyout, "%*cFuncCallParamsnode\n", level, ' '); 
    for(std::vector<ExprNode*>::iterator i=paramsList.begin(); i!= paramsList.end();i++)
    {
        (*i)->output(level+4);
    }
}

void ImplictCastExpr::output(int level) {
    fprintf(yyout, "%*cImplictCastExpr\ttype: %s to %s\n", level, ' ',
            expr->getType()->toStr().c_str(), type->toStr().c_str());
    this->expr->output(level + 4);
}

void ImplictCastExpr::genCode() {
    expr->genCode();
    BasicBlock* bb = builder->getInsertBB();
    Function* func = bb->getParent();
    BasicBlock* trueBB = new BasicBlock(func);
    BasicBlock* tempbb = new BasicBlock(func);
    BasicBlock* falseBB = new BasicBlock(func);

    new CmpInstruction(
        CmpInstruction::NE, this->dst, this->expr->getOperand(),
        new Operand(new ConstantSymbolEntry(TypeSystem::intType, 0)), bb);
    this->trueList().push_back(
        new CondBrInstruction(trueBB, tempbb, this->dst, bb));
    this->falseList().push_back(new UncondBrInstruction(falseBB, tempbb));
}

void WhileStmt::output(int level)
{
    fprintf(yyout, "%*cWhileStmt\n", level, ' ');
    cond->output(level+4);
    stmt->output(level+4);
}