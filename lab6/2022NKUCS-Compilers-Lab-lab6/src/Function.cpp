#include "Function.h"
#include "Unit.h"
#include "Type.h"
#include <list>

extern FILE* yyout;

Function::Function(Unit *u, SymbolEntry *s)
{
    u->insertFunc(this);
    entry = new BasicBlock(this);
    sym_ptr = s;
    parent = u;
}

Function::~Function()
{
    auto delete_list = block_list;
    for (auto &i : delete_list)
        delete i;
   // parent->removeFunc(this);
}

// remove the basicblock bb from its block_list.
void Function::remove(BasicBlock *bb)
{
    block_list.erase(std::find(block_list.begin(), block_list.end(), bb));
}

void Function::output() const
{
    printf("Function::output()\n");
    FunctionType* funcType = dynamic_cast<FunctionType*>(sym_ptr->getType());
    Type *retType = funcType->getRetType();
    printf("check func param: %s\n", sym_ptr->toStr().c_str());
    //fprintf(yyout, "define %s %s() {\n", retType->toStr().c_str(), sym_ptr->toStr().c_str());
    //fprintf(yyout, "define %s %s(", retType->toStr().c_str(), sym_ptr->toStr().c_str());
    
    std::string params;
    std::vector<Type*> paramsType=funcType->getParamsType();
    if(paramsType.size()>=1){
    if(paramsType.size()==1)
    {
        auto it =paramsType.begin();
        params+=(*it)->toStr();
    }
    else{
        auto it =paramsType.begin();
        params+=(*it)->toStr();
    for(auto it =paramsType.begin()+1;it!=paramsType.end();++it)
    {
        params+=","+(*it)->toStr();
    }
    }
    fprintf(yyout, "define %s %s(%s) {\n", retType->toStr().c_str(), sym_ptr->toStr().c_str(),params.c_str());
    }
    else{
    fprintf(yyout, "define %s %s() {\n", retType->toStr().c_str(), sym_ptr->toStr().c_str());

    }

    std::set<BasicBlock *> v;
    std::list<BasicBlock *> q;
    //entry->output();
    //BasicBlock* entry2 = block_list[1];//my add
    q.push_back(entry);
    v.insert(entry);
    while (!q.empty())
    {
        auto bb = q.front();
        q.pop_front();
        printf("Function::output()--bb->output() in while\n");
        bb->output();
        printf("after bb->output()\n");
        for (auto succ = bb->succ_begin(); succ != bb->succ_end(); succ++)
        {
            printf("Function::output()--for\n");//haven't been in
            if (v.find(*succ) == v.end())
            {
                v.insert(*succ);
                q.push_back(*succ);
            }
        }
    }
    
    fprintf(yyout, "}\n");
}
