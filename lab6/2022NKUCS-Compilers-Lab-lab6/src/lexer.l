%option noyywrap
%{
    #define YY_NO_UNPUT
    #define YY_NO_INPUT

    #include "parser.h"
    #include <ostream>
    #include <fstream>
    using namespace std;

    extern FILE *yyin; 
    extern FILE *yyout;
    extern bool dump_tokens;

    void DEBUG_FOR_LAB4(std::string s){
        std::string DEBUG_INFO = "[DEBUG LAB4]: \t" + s + "\n";
        fputs(DEBUG_INFO.c_str(), yyout);
    }
     int HtoD(char*s)//16进制到10进制，返回10进制
    {
        char *str=new char[yyleng+1];
        strcpy(str,s);
        strcat(str,"\0");
        int i = 0;
        sscanf(str, "%x", &i);
        //printf("%d\n", i);
        return i;
    }

    int OtoD(char*s)
    {
        char *str=new char[yyleng+1];
        strcpy(str,s);
        strcat(str,"\0");
        int i = 0;
        sscanf(str, "%o", &i);
        //printf("%d\n", i);
        return i;
    }
%}

HEXADECIMAL ("0x"|"0X")([1-9a-fA-F][0-9a-fA-F]*|0)
OCTAL (0+)[1-7][0-7]*
DECIMIAL ([1-9][0-9]*|0)
ID [[:alpha:]_][[:alpha:][:digit:]_]*
EOL (\r\n|\n|\r)
WHITE [\t ]
blockcommentBegin "/*"
blockcommentElement .|\n
blockcommentEnd "*/"
%x BLOCKCOMMENT

linecommentBegin "//"
linecommentElement .
linecommentEnd \n
%x LINECOMMENT
%%
{blockcommentBegin} {BEGIN BLOCKCOMMENT;}
<BLOCKCOMMENT>{blockcommentElement} {}
<BLOCKCOMMENT>{blockcommentEnd} {BEGIN INITIAL;}
{linecommentBegin} {BEGIN LINECOMMENT;}
<LINECOMMENT>{linecommentElement} {}
<LINECOMMENT>{linecommentEnd} {BEGIN INITIAL;}

"int" {
    /*
    * Questions: 
    *   Q1: Why we need to return INT in further labs?
    *   Q2: What is "INT" actually?
    */
    //printf("lexer:int\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("INT\tint");
    return INT;
}
"void" {
    //printf("lexer:void\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("VOID\tvoid");
    return VOID;
}
"if" {
    //printf("lexer:if\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("IF\tif");
    return IF;
};
"else" {
    //("lexer:else\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("ELSE\telse");
    return ELSE;
};
"return" {
    //printf("lexer:return\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("RETURN\treturn");
    return RETURN;
}

"while" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("WHILE\twhile");
    return WHILE;
}
">=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("NOLESS\t<");
    return NOLESS;
}


"<=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("NOMORE\t<");
    return NOMORE;
}


">" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("MORE\t<");
    return MORE;
}


"<" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return LESS;
}


"||" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("OR\t=");
    return OR;
}

"&&" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("AND\t=");
    return AND;
}

"==" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("EQUAL\t=");
    return EQUAL;
}

"!=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("NOTEQUAL\t=");
    return NOTEQUAL;
}

"=" {
    //printf("lexer:ASSIGN\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("ASSIGN\t=");
    return ASSIGN;
}

"<" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return LESS;
}
"+" {
    //printf("lexer:ADD\n");
    if(dump_tokens)
        DEBUG_FOR_LAB4("ADD\t+");
    return ADD;
}
"-" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("SUB\t+");
    return SUB;
}
"*" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("MULTIPY\t+");
    return MUL;
}
"/" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("DIV\t+");
    return DIV;
}

"%" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("MOD\t+");
    return MOD;
}
"!" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("NOT\t+");
    return NOT;
}
";" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("SEMICOLON\t;");
    return SEMICOLON;
}

"," {
    if(dump_tokens)
        DEBUG_FOR_LAB4("COMMA\t");
    return COMMA;
}
"const" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("Const\t");
    return CONST;
}
"[" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LPAREN\t(");
    return LSBRACE;
}
"]" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RPAREN\t)");
    return RSBRACE;
}
"(" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LPAREN\t(");
    return LPAREN;
}
")" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RPAREN\t)");
    return RPAREN;
}
"{" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LBRACE\t{");
    return LBRACE;
}
"}" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RBRACE\t}");
    return RBRACE;
}

{DECIMIAL} {
    if(dump_tokens)
        DEBUG_FOR_LAB4(yytext);
    yylval.itype = atoi(yytext);
    return INTEGER;
}
{OCTAL} {
    int tmp=OtoD(yytext);
    if(dump_tokens)
    {
        DEBUG_FOR_LAB4(yytext);
    }
    yylval.itype = tmp;
    return INTEGER;
}


{HEXADECIMAL} {
    int tmp=HtoD(yytext);
    if(dump_tokens)
    {
        DEBUG_FOR_LAB4(yytext);
    }
    yylval.itype = tmp;
    return INTEGER;
}
{ID} {
    if(dump_tokens)
        DEBUG_FOR_LAB4(yytext);
    char *lexeme;
    lexeme = new char[strlen(yytext) + 1];
    strcpy(lexeme, yytext);
    yylval.strtype = lexeme;
    return ID;
}
{EOL} yylineno++;
{WHITE}
%%
