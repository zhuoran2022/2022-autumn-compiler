// %code top{
//     #include <iostream>
//     #include <assert.h>
//     #include "parser.h"
//     extern Ast ast;
//     int yylex();
//     int yyerror( char const * );
// }

// %code requires {
//     #include "Ast.h"
//     #include "SymbolTable.h"
//     #include "Type.h"
// }

// %union {
//     int itype;
//     char* strtype;
//     StmtNode* stmttype;
//     ExprNode* exprtype;
//     Type* type;
// }

// %start Program
// %token <strtype> ID 
// %token <itype> INTEGER
// %token IF ELSE
// %token WHILE
// %token CONST
// %token INT VOID
// %token LPAREN RPAREN LBRACE RBRACE SEMICOLON COMMA LSBRACE RSBRACE
// %token OR
// %token AND
// %token EQUAL NOTEQUAL
// %token ADD SUB 
// %token MUL DIV MOD
// %token LESS MORE NOMORE NOLESS ASSIGN
// %token NOT
// %token RETURN

// %nterm <stmttype> Stmts Stmt AssignStmt BlockStmt IfStmt ReturnStmt DeclStmt FuncDef VarDeclStmt ConstDeclStmt VarDefs VarDef ConstDef ConstDefs FuncParams  OptFuncParams FuncParam FuncCallParams UselessStmt
// %nterm <exprtype> Exp AddExp UnaryExp Cond LOrExp PrimaryExp LVal RelExp LAndExp EqExp MulExp 
// %nterm <type> Type

// %precedence THEN
// %precedence ELSE
// %%
// Program
//     : Stmts {
//         ast.setRoot($1);
//     }
//     ;
// Stmts
//     : Stmt {$$=$1;}
//     | Stmts Stmt{
//         $$ = new SeqNode($1, $2);
//     }
//     ;
// Stmt
//     : AssignStmt {$$=$1;}
//     | BlockStmt {$$=$1;}
//     | IfStmt {$$=$1;}
//     | ReturnStmt {$$=$1;}
//     | DeclStmt {$$=$1;}
//     | FuncDef {$$=$1;}
//     | UselessStmt {$$ = $1;}
//     ;
// LVal
//     : ID {
//         SymbolEntry *se;
//         se = identifiers->lookup($1);
//         if(se == nullptr)
//         {
//             fprintf(stderr, "identifier \"%s\" is undefined\n", (char*)$1);
//             delete [](char*)$1;
//             assert(se != nullptr);
//         }
//         $$ = new Id(se);
//         delete []$1;
//     }
//     ;
// AssignStmt
//     :
//     LVal ASSIGN Exp SEMICOLON {
//         $$ = new AssignStmt($1, $3);
//     }
//     ;
// BlockStmt
//     :   LBRACE 
//         {identifiers = new SymbolTable(identifiers);} 
//         Stmts RBRACE 
//         {
//             $$ = new CompoundStmt($3);
//             SymbolTable *top = identifiers;
//             identifiers = identifiers->getPrev();
//             delete top;
//         }
//     |
//     LBRACE 
//     {identifiers = new SymbolTable(identifiers);}//now identifiers is a new SymbolTable
//      RBRACE 
//     {
//         $$ = new CompoundStmt(nullptr);
//         SymbolTable *top = identifiers;//now top is identifiers
//         identifiers = identifiers->getPrev();//now identifiers is before identifiers
//         delete top;//why delete new ones?create,and delete,why?
//     }
//     ;
// IfStmt
//     : IF LPAREN Cond RPAREN Stmt %prec THEN {
//         $$ = new IfStmt($3, $5);
//     }
//     | IF LPAREN Cond RPAREN Stmt ELSE Stmt {
//         $$ = new IfElseStmt($3, $5, $7);
//     }
//     ;
// // LoopStmt
// //     : WHILE LPAREN Cond RPAREN Stmt {
// //         $$ = new WhileStmt($3, $5);
// //     }
// //     ;
// ReturnStmt
//     :
//     RETURN Exp SEMICOLON{
//         $$ = new ReturnStmt($2);
//     }
//     ;
// Exp
//     :
//     AddExp {$$ = $1;}
//     ;
// Cond
//     :
//     LOrExp {$$ = $1;}
//     ;
// UnaryExp
//     :
//     PrimaryExp{
//         $$=$1;
//     }
//     |
//     ADD UnaryExp{
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         $$ = new UnaryExpr(se, UnaryExpr::ADD, $2);
//     }
//     |
//     SUB UnaryExp{
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         printf("SUB UnaryExp\n");
//         $$ = new UnaryExpr(se, UnaryExpr::SUB, $2);
//     }
//     |
//     ID LPAREN FuncCallParams RPAREN{
//         printf("unaryexp--funccall\n");
//         SymbolEntry *se;
//         se = identifiers->lookup($1);
//         if(se == nullptr)
//         {
//             fprintf(stderr, "identifier \"%s\" is undefined\n", (char*)$1);
//             delete [](char*)$1;
//             printf("ID = %send\n", $1);
//             assert(se != nullptr);
//         }
//         //SymbolEntry *temp = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         //SymbolEntry* f = lookup() !!about getint()
//         $$ = new FuncCallExpr(se,new Id(se),(FuncCallParamsnode*)$3);
//         if($$->getOperand() == nullptr)
//         {
//             printf("$$->getOperand() == nullptr in parser\n");
//         }
//     }
//     |
//     NOT UnaryExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new UnaryExpr(se, UnaryExpr::NOT, $2);
//     }
//     ;
// PrimaryExp
//     :
//     LVal {
//         $$ = $1;
//     }
//     | INTEGER {
//         SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, $1);
//         // int t = (dynamic_cast<ConstantSymbolEntry *>(se))->getValue();
//         // printf("constantsymbolentry!!!%d\n", t);
//         $$ = new Constant(se);
//         // SymbolEntry *se2 = $$->getSymPtr();
//         // t = (dynamic_cast<ConstantSymbolEntry *>(se2))->getValue();
//         // printf("constantsymbolentry2!!!%d\n", t);
//     }
//     | LPAREN Exp RPAREN {
//         $$ = $2;
//     }
//     ;
//     ;
// AddExp
//     :
//     MulExp {$$ = $1;}
//     |
//     AddExp ADD MulExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::ADD, $1, $3);
//     }
//     |
//     AddExp SUB MulExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::SUB, $1, $3);
//     }
//     ;
// MulExp
//     :
//     UnaryExp {$$ = $1;}
//     | 
//     MulExp MUL UnaryExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::MUL, $1, $3);
//     }
//     |
//     MulExp DIV UnaryExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::DIV, $1, $3);
//     }
//     |
//     MulExp MOD UnaryExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::MOD, $1, $3);
//     }
//     ;
// RelExp
//     :
//     AddExp {$$ = $1;}
//     |
//     RelExp LESS AddExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::LESS, $1, $3);
//     }
//     |
//     RelExp MORE AddExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         printf("BinaryExpr::MORE\n");
//         $$ = new BinaryExpr(se, BinaryExpr::MORE, $1, $3);
//     }
//     |
//     RelExp NOMORE AddExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::NOMORE, $1, $3);
//     }
//     |
//     RelExp NOLESS AddExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::NOLESS, $1, $3);
//     }
//     ;
// EqExp
//     :
//     RelExp{$$=$1;}
//     |
//     EqExp EQUAL RelExp{
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::EQUAL, $1, $3);
//     }
//     |
//     EqExp NOTEQUAL RelExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::NOTEQUAL, $1, $3);
//     }
//     ;
// LAndExp
//     :
//     EqExp {$$ = $1;}
//     |
//     LAndExp AND EqExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::AND, $1, $3);
//     }
//     ;
// LOrExp
//     :
//     LAndExp {$$ = $1;}
//     |
//     LOrExp OR LAndExp
//     {
//         SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
//         $$ = new BinaryExpr(se, BinaryExpr::OR, $1, $3);
//     }
//     ;
// Type
//     : INT {
//         $$ = TypeSystem::intType;
//     }
//     | VOID {
//         $$ = TypeSystem::voidType;
//     }
//     ;
// // DeclStmt
// //     :
// //     Type ID SEMICOLON {
// //         SymbolEntry *se;
// //         se = new IdentifierSymbolEntry($1, $2, identifiers->getLevel());
// //         identifiers->install($2, se);
// //         $$ = new DeclStmt(new Id(se));
// //         delete []$2;
// //     }
// //     ;
// DeclStmt
//     : 
//     VarDeclStmt {
//         $$=$1;
//     }
//     |
//     ConstDeclStmt {
//         $$=$1;
//     }
//     ;

// ConstDeclStmt
//     :
//     CONST Type ConstDefs SEMICOLON  {
//          $$=new ConstDeclStmt($3);
//     }
//     ;

// VarDeclStmt
//     :
//     Type VarDefs SEMICOLON {
//         $$=new VarDeclStmt($2);
//     }
//     ;

// VarDefs
//     :
//     VarDef COMMA VarDefs{
//         $$=new Defs($1,$3);
//     }
//     |
//     VarDef{
//         $$=new Defs($1);
//     }
//     ;

// ConstDefs
//     :
//     ConstDef COMMA ConstDefs{
//         $$=new Defs($1,$3);
//     }
//     |
//     ConstDef{
//         $$=new Defs($1);
//     }
//     ;

// VarDef
//     :
//     ID{
//         SymbolEntry *se;
//         se = new IdentifierSymbolEntry(TypeSystem::intType ,$1, identifiers->getLevel());
//         dynamic_cast<IdentifierSymbolEntry*>(se)->setvar();
//         dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
//         identifiers->install($1, se);
//         $$ = new Def(new Id(se));
//         delete []$1;
//     }
//     |
//     ID ASSIGN Exp{
//         SymbolEntry *se;
//         se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
//         dynamic_cast<IdentifierSymbolEntry*>(se)->setvar();
//         dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
//         identifiers->install($1, se);
//         $$=new Def(new Id(se),$3);
//         delete[]$1;
//     }
//     ;

// ConstDef
//     :
//     ID ASSIGN Exp {
//         SymbolEntry *se;
//         se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
//         dynamic_cast<IdentifierSymbolEntry*>(se)->setcon();
//         dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
//         identifiers->install($1, se);
//         $$=new Def(new Id(se),$3);
//         delete[]$1;
//     }
//     ;
// FuncDef
//     :
//     Type ID {
//         printf("funcdef -- type...\n");
//         Type *funcType;
//         funcType = new FunctionType($1,{});//param2 is an empty vector for type
//         SymbolEntry *se = new IdentifierSymbolEntry(funcType, $2, identifiers->getLevel());
//         identifiers->install($2, se);
//         identifiers = new SymbolTable(identifiers);
//     }
//     LPAREN OptFuncParams{
//         SymbolEntry *se;
//         se = identifiers->lookup($2);//check if funcname is inserted successfully
//         assert(se!=nullptr);
//         if($5 != nullptr)
//         {
//            FunctionType *funcType = (FunctionType*)(se->getType());//get the functype we def above
//            std::vector<Type*> paramType = ((FuncParamsnode*)$5)->getParamsType();//get the type part of FuncParamsnode class
//            funcType->setparamsType(paramType);//set it
//         }
//     }
//     RPAREN
//     BlockStmt
//     {
//         SymbolEntry *se;
//         se = identifiers->lookup($2);
//         assert(se != nullptr);
//         $$ = new FunctionDef(se, (FuncParamsnode*)$5,$8);
//         SymbolTable *top = identifiers;
//         identifiers = identifiers->getPrev();
//         delete top;
//         delete []$2;
//     }
//     ;
// OptFuncParams
//     :
//     FuncParams{$$=$1;}
//     |
//     %empty{$$ = nullptr;}
//     ;

// FuncParams
//     :
//     FuncParams COMMA FuncParam{
//         FuncParamsnode* node=(FuncParamsnode* )$1;//get the longer FuncParamsnode
//         node->append(((FuncParam*)$3)->getid());//append the onlyone type-id to the longer one
//         $$=node;
//     }
//     |
//     FuncParam{
//         FuncParamsnode* node=new FuncParamsnode();//get type-id from a FuncParam class and new a FuncParamsnode class
//         node->append(((FuncParam*)$1)->getid());//put the type-id into the FuncParamsnode's vector<Id*>, now the vector has onlyone type-id
//         $$=node;
//     }
//     ;

// FuncParam
//     :
//     Type ID{
//         SymbolEntry *se;
//         se = new IdentifierSymbolEntry($1,$2, identifiers->getLevel());
//         identifiers->install($2, se);
//         $$ = new FuncParam(new Id(se));//put the type-id in a FuncParam class
//     }
//     ;

// FuncCallParams
//     :
//     FuncCallParams COMMA Exp{
//         FuncCallParamsnode* node=(FuncCallParamsnode*)$1;
//         node->append($3);
//         $$=node;
//     }
//     |
//     Exp{
//         FuncCallParamsnode* node= new FuncCallParamsnode();
//         node->append($1);
//         $$=node;
//     }
//     |
//     %empty{
//         $$=nullptr;
//     }
//     ;
// UselessStmt
//     :
//     Exp SEMICOLON{}
//     |
//     SEMICOLON{$$=new EmptyStmt();}
//     ;
// %%

// int yyerror(char const* message)
// {
//     std::cerr<<message<<std::endl;
//     return -1;
// }


%code top{
    #include <iostream>
    #include <assert.h>
    #include "parser.h"
    extern Ast ast;
    int yylex();
    int yyerror( char const * );
}

%code requires {
    #include "Ast.h"
    #include "SymbolTable.h"
    #include "Type.h"
}

%{
FunctionDef *func;
FuncCallExpr *call;
std::vector<FuncCallExpr*> callBeforeExp;
ReturnStmt *ret=nullptr;
%}

%union {
    int itype;
    char* strtype;
    StmtNode* stmttype;
    ExprNode* exprtype;
    Type* type;
}

%start Program
%token <strtype> ID 
%token <itype> INTEGER
%token IF ELSE
%token WHILE
%token CONST
%token INT VOID
%token LPAREN RPAREN LBRACE RBRACE SEMICOLON COMMA LSBRACE RSBRACE
%token OR
%token AND
%token EQUAL NOTEQUAL
%token ADD SUB 
%token MUL DIV MOD
%token LESS MORE NOMORE NOLESS ASSIGN
%token NOT
%token RETURN

%nterm <stmttype> Stmts Stmt AssignStmt BlockStmt IfStmt ReturnStmt DeclStmt FuncDef VarDeclStmt ConstDeclStmt VarDefs VarDef ConstDef ConstDefs UselessStmt WhileStmt
%nterm <exprtype> Exp AddExp UnaryExp Cond LOrExp PrimaryExp LVal RelExp LAndExp EqExp MulExp FuncFParams FuncFParam FuncRParams FuncRParam
%nterm <type> Type

%precedence THEN
%precedence ELSE
%%
Program
    : Stmts {
        ast.setRoot($1);
    }
    ;
Stmts
    : Stmt {$$=$1;}
    | Stmts Stmt{
        $$ = new SeqNode($1, $2);
    }
    ;
Stmt
    : AssignStmt {$$=$1;}
    | BlockStmt {$$=$1;}
    | IfStmt {$$=$1;}
    | WhileStmt {$$=$1;}
    | ReturnStmt {$$=$1;}
    | DeclStmt {$$=$1;}
    | FuncDef {$$=$1;}
    | UselessStmt {$$ = $1;}
    ;
LVal
    : ID {
        SymbolEntry *se;
        se = identifiers->lookup($1);
        if(se == nullptr)
        {
            fprintf(stderr, "identifier \"%s\" is undefined\n", (char*)$1);
            delete [](char*)$1;
            assert(se != nullptr);
        }
        $$ = new Id(se);
        delete []$1;

        (dynamic_cast<Id *>$$)->getType()->isVoid();
    }
    ;
AssignStmt
    :
    LVal ASSIGN Exp SEMICOLON {
        $$ = new AssignStmt($1, $3);
    }
    ;
BlockStmt
    :   LBRACE 
        {identifiers = new SymbolTable(identifiers);} 
        Stmts RBRACE 
        {
            $$ = new CompoundStmt($3);
            SymbolTable *top = identifiers;
            identifiers = identifiers->getPrev();
            delete top;
        }
    |
    LBRACE 
    {identifiers = new SymbolTable(identifiers);}//now identifiers is a new SymbolTable
    RBRACE 
    {
        $$ = new CompoundStmt(nullptr);
        SymbolTable *top = identifiers;//now top is identifiers
        identifiers = identifiers->getPrev();//now identifiers is before identifiers
        delete top;//why delete new ones?create,and delete,why?
    }
    ;
IfStmt
    : IF LPAREN Cond RPAREN Stmt %prec THEN {
        $$ = new IfStmt($3, $5);
    }
    | IF LPAREN Cond RPAREN Stmt ELSE Stmt {
        $$ = new IfElseStmt($3, $5, $7);
    }
    ;

WhileStmt
    : WHILE LPAREN Cond RPAREN Stmt {
         $$ = new WhileStmt($3, $5);
    }
    ;
ReturnStmt
    :
    RETURN Exp SEMICOLON{
        $$ = new ReturnStmt($2);
    }
    |
    RETURN SEMICOLON 
    {
        $$ = new ReturnStmt(nullptr);
    }
    ;
Exp
    :
    AddExp {$$ = $1;}
    ;
Cond
    :
    LOrExp {$$ = $1;}
    ;
UnaryExp
    :
    PrimaryExp{
        $$=$1;
    }
    |
    ADD UnaryExp{
        // SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        // $$ = new UnaryExpr(se, UnaryExpr::ADD, $2);
        // printf("unaryexpr op = %d\n", (dynamic_cast<UnaryExpr *>$$)->op);
        $$ = $2;
    }
    |
    SUB UnaryExp{
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        printf("SUB UnaryExp\n");
        $$ = new UnaryExpr(se, UnaryExpr::SUB, $2);
        printf("unaryexpr op = %d\n", (dynamic_cast<UnaryExpr *>$$)->op);
    }
    |
    NOT UnaryExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new UnaryExpr(se, UnaryExpr::NOT, $2);
        printf("unaryexpr op = %d\n", (dynamic_cast<UnaryExpr *>$$)->op);
    }
    |
    ID LPAREN{
        //函数调用
        //FuncRParams为函数实参
        SymbolEntry *se;
        se = identifiers->lookup($1);
        printf("parser.y--funccall ID = %s\n", $1);
        printf("parser.y--IdEntryname = %s\n", ((IdentifierSymbolEntry*)se)->getname().c_str());

        //此处为在构建语法树的过程中进行的类型检查，若函数未声明则输出相应提示信息并退出程序
        if(se==nullptr)
        {
            fprintf(stderr,"Function undeclared!\n");
            exit(EXIT_FAILURE);
        }
        //assert(se!=nullptr);

        //检查是否返回值为void
        Type* idType=se->getType();
        if(!idType->isFunc())
        {
            fprintf(stderr,"expect a function!");
            exit(EXIT_FAILURE);
        }

        Type* retType=dynamic_cast<FunctionType*>(se->getType())->getRetType();
        SymbolEntry *tmp = new TemporarySymbolEntry(retType, SymbolTable::getLabel());
        call=new FuncCallExpr(se,tmp); 
        printf("parser.y--IdEntryname2 = %s\n", ((IdentifierSymbolEntry*)se)->getname().c_str());
        printf("funccallexpr1\n"); 
        //delete []$1;  
        //在解析参数时，call可能会被参数里面的call覆盖
    } FuncRParams RPAREN{ $$ = call;} 
    ;

FuncRParams
    :
    %empty{$$=nullptr; }
    |
    {callBeforeExp.push_back(call);
    }
    FuncRParam
    {
        $$=$2;
        
    }
    |
    FuncRParams COMMA {callBeforeExp.push_back(call);}FuncRParam
    {
       $$=$4;
    }
    ;

FuncRParam
    :
    Exp
    {
        //不用创建新的类，只用每次插入就行

        //ExprNode *expr=$1;
        
        //创建新的变量
        //SymbolEntry *tmp = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        //SymbolEntry *se = expr->getSymPtr();

        //Operand* dst = expr->getOperand();//在call指令时在dst中写入调用
        if($1!=NULL)
        {
            call=*(callBeforeExp.rbegin());
            callBeforeExp.pop_back();
            call->addParam($1);
            
        }

    }
    ;

PrimaryExp
    :
    LVal {
        $$ = $1;
    }
    | INTEGER {
        SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, $1);
        // int t = (dynamic_cast<ConstantSymbolEntry *>(se))->getValue();
        // printf("constantsymbolentry!!!%d\n", t);
        $$ = new Constant(se);
        // SymbolEntry *se2 = $$->getSymPtr();
        // t = (dynamic_cast<ConstantSymbolEntry *>(se2))->getValue();
        // printf("constantsymbolentry2!!!%d\n", t);
        printf("consttype size = %d\n", (dynamic_cast<Constant *>($$))->getType()->getSize());
    }
    | LPAREN Exp RPAREN {
        $$ = $2;
    }
    ;
    ;
AddExp
    :
    MulExp {$$ = $1;}
    |
    AddExp ADD MulExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::ADD, $1, $3);
    }
    |
    AddExp SUB MulExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::SUB, $1, $3);
    }
    ;
MulExp
    :
    UnaryExp {$$ = $1;}
    | 
    MulExp MUL UnaryExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::MUL, $1, $3);
    }
    |
    MulExp DIV UnaryExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::DIV, $1, $3);
    }
    |
    MulExp MOD UnaryExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::MOD, $1, $3);
    }
    ;
RelExp
    :
    AddExp {$$ = $1;}
    |
    RelExp LESS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::LESS, $1, $3);
    }
    |
    RelExp MORE AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        printf("BinaryExpr::MORE\n");
        $$ = new BinaryExpr(se, BinaryExpr::MORE, $1, $3);
    }
    |
    RelExp NOMORE AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::NOMORE, $1, $3);
    }
    |
    RelExp NOLESS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::NOLESS, $1, $3);
    }
    ;
EqExp
    :
    RelExp{$$=$1;}
    |
    EqExp EQUAL RelExp{
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::EQUAL, $1, $3);
    }
    |
    EqExp NOTEQUAL RelExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::NOTEQUAL, $1, $3);
    }
    ;
LAndExp
    :
    EqExp {$$ = $1;}
    |
    LAndExp AND EqExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::AND, $1, $3);
    }
    ;
LOrExp
    :
    LAndExp {$$ = $1;}
    |
    LOrExp OR LAndExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::boolType, SymbolTable::getLabel());
        se->getType()->size = 1;
        $$ = new BinaryExpr(se, BinaryExpr::OR, $1, $3);
    }
    ;
Type
    : INT {
        $$ = TypeSystem::intType;
    }
    | VOID {
        $$ = TypeSystem::voidType;
    }
    ;
// DeclStmt
//     :
//     Type ID SEMICOLON {
//         SymbolEntry *se;
//         se = new IdentifierSymbolEntry($1, $2, identifiers->getLevel());
//         identifiers->install($2, se);
//         $$ = new DeclStmt(new Id(se));
//         delete []$2;
//     }
//     ;
DeclStmt
    : 
    VarDeclStmt {
        $$=$1;
    }
    |
    ConstDeclStmt {
        $$=$1;
    }
    ;

ConstDeclStmt
    :
    CONST Type ConstDefs SEMICOLON  {
         $$=new ConstDeclStmt($3);
    }
    ;

VarDeclStmt
    :
    Type VarDefs SEMICOLON {
        $$=new VarDeclStmt($2);
    }
    ;

VarDefs
    :
    VarDef COMMA VarDefs{
        $$=new Defs($1,$3);
    }
    |
    VarDef{
        $$=new Defs($1);
    }
    ;

ConstDefs
    :
    ConstDef COMMA ConstDefs{
        $$=new Defs($1,$3);
    }
    |
    ConstDef{
        $$=new Defs($1);
    }
    ;

VarDef
    :
    ID{
        if(identifiers->isRedefine($1)) {
            fprintf(stderr, "identifier %s redefine\n", $1);
            exit(EXIT_FAILURE);
        }
        SymbolEntry *se;
        se = new IdentifierSymbolEntry(TypeSystem::intType ,$1, identifiers->getLevel());
        dynamic_cast<IdentifierSymbolEntry*>(se)->setvar();
        dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
        identifiers->install($1, se);
        //se->getType()->setType(32);
        se->getType()->size = 32;
        printf("check getsize\n");
        int s = se->getType()->getSize();
        printf("size = %d\n", s);
        $$ = new Def(new Id(se));
        delete []$1;
    }
    |
    ID ASSIGN Exp{
        if(identifiers->isRedefine($1)) {
            fprintf(stderr, "identifier %s redefine\n", $1);
            exit(EXIT_FAILURE);
        }
        SymbolEntry *se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        dynamic_cast<IdentifierSymbolEntry*>(se)->setvar();
        dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
        se->getType()->size = 32;
        identifiers->install($1, se);
        $$=new Def(new Id(se),$3);
        delete[]$1;
    }
    ;

ConstDef
    :
    ID ASSIGN Exp {
        if(identifiers->isRedefine($1)) {
            fprintf(stderr, "identifier %s redefine\n", $1);
            exit(EXIT_FAILURE);
        }
        SymbolEntry *se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        dynamic_cast<IdentifierSymbolEntry*>(se)->setcon();
        dynamic_cast<IdentifierSymbolEntry*>(se)->setType();
        identifiers->install($1, se);
        $$=new Def(new Id(se),$3);
        delete[]$1;
    }
    ;
/*FuncDef
    :
    Type ID {
        printf("funcdef -- type...\n");
        Type *funcType;
        funcType = new FunctionType($1,{});//param2 is an empty vector for type
        SymbolEntry *se = new IdentifierSymbolEntry(funcType, $2, identifiers->getLevel());
        identifiers->install($2, se);
        identifiers = new SymbolTable(identifiers);
    }
    LPAREN OptFuncParams{
       // SymbolEntry *se;
       // se = identifiers->lookup($2);//check if funcname is inserted successfully
       // assert(se!=nullptr);
       // if($5 != nullptr)
       // {
       //    FunctionType *funcType = (FunctionType*)(se->getType());//get the functype we def above
       //    std::vector<Type*> paramType = ((FuncParamsnode*)$5)->getParamsType();//get the type part of FuncParamsnode class
       //    funcType->setparamsType(paramType);//set it
       // }
    }
    RPAREN
    BlockStmt
    {
        SymbolEntry *se;
        se = identifiers->lookup($2);
        assert(se != nullptr);
        $$ = new FunctionDef(se, (FuncParamsnode*)$5,$8);
        SymbolTable *top = identifiers;
        identifiers = identifiers->getPrev();
        delete top;
        delete []$2;
    }
    ;
OptFuncParams
    :
    FuncParams{$$=$1;}
    |
    %empty{$$ = nullptr;}
    ;

FuncParams
    :
    FuncParams COMMA FuncParam{
        FuncParamsnode* node=(FuncParamsnode* )$1;//get the longer FuncParamsnode
        node->append(((FuncParam*)$3)->getid());//append the onlyone type-id to the longer one
        $$=node;
    }
    |
    FuncParam{
        FuncParamsnode* node=new FuncParamsnode();//get type-id from a FuncParam class and new a FuncParamsnode class
        node->append(((FuncParam*)$1)->getid());//put the type-id into the FuncParamsnode's vector<Id*>, now the vector has onlyone type-id
        $$=node;
    }
    ;

FuncParam
    :
    Type ID{
        SymbolEntry *se;
        se = new IdentifierSymbolEntry($1,$2, identifiers->getLevel());
        identifiers->install($2, se);
        $$ = new FuncParam(new Id(se));//put the type-id in a FuncParam class
    }
    ;

FuncCallParams
    :
    FuncCallParams COMMA Exp{
        FuncCallParamsnode* node=(FuncCallParamsnode*)$1;
        node->append($3);
        $$=node;
    }
    |
    Exp{
        FuncCallParamsnode* node= new FuncCallParamsnode();
        node->append($1);
        $$=node;
    }
    |
    %empty{
        $$=nullptr;
    }
    ;
*/

FuncDef
    :
    Type ID 
    {
        Type *funcType;
        funcType = new FunctionType($1,{});
        SymbolEntry *se = new IdentifierSymbolEntry(funcType, $2, identifiers->getLevel());
        identifiers->install($2, se);
        identifiers = new SymbolTable(identifiers);
        //$$ = new FunctionDef(se);
        //func=$$;
        func = new FunctionDef(se);
    }
    LPAREN FuncFParams
    {
        


    }
    RPAREN
    BlockStmt
    {
        func->setStmtNode($8);
        identifiers = identifiers->getPrev();
        func->setRetStmt(ret);
        ret=nullptr;
        $$=func;
        delete []$2;
    }   
    ;

FuncFParams
    :
    %empty{$$=nullptr;}
    |
    FuncFParam
    {
        $$=$1;
    }
    |
    FuncFParams COMMA FuncFParam
    {
        $$=$3;
    }
    ;

FuncFParam
    :
    Type ID
    {
        SymbolEntry *se;
        se = new IdentifierSymbolEntry($1 ,$2, identifiers->getLevel());
        identifiers->install($2, se);
        func->addParam(new Id(se));
        SymbolEntry* se1=func->getSymPtr();
        FunctionType* type=dynamic_cast<FunctionType*>(se1->getType());
        type->addParamType($1);
        delete []$2;
    }
    ;

UselessStmt
    :
    Exp SEMICOLON{}
    |
    SEMICOLON{$$=new EmptyStmt();}
    ;
%%

int yyerror(char const* message)
{
    std::cerr<<message<<std::endl;
    return -1;
}
