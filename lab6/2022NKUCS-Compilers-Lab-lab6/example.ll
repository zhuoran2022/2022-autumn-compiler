define void @func1(i32) {
B14:
  %t15 = alloca i32, align 4
  store i32 %0, i32* %t15, align 4
  %t2 = load i32, i32* %t15, align 4
  %t3 = add i32 %t2, 1
  store i32 %t3, i32* %t15, align 4
  ret void
}
define void @func2(i32) {
B16:
  %t17 = alloca i32, align 4
  store i32 %0, i32* %t17, align 4
  %t5 = load i32, i32* %t17, align 4
  %t6 = icmp slt i32 %t5, 0
  br i1 %t6, label %B18, label %B23
B18:                               	; preds = %B16
  store i32 0, i32* %t17, align 4
  br label %B20
B23:                               	; preds = %B16
  br label %B19
B20:                               	; preds = %B18, %B19
  ret void
B19:                               	; preds = %B23
  store i32 1, i32* %t17, align 4
  br label %B20
}
define i32 @main() {
B24:
  %t25 = alloca i32, align 4
  call void @func1( i32 1)
  call void @func2( i32 2)
  %t11 = add i32 %t9, %t10
  store i32 %t11, i32* %t25, align 4
  %t13 = load i32, i32* %t25, align 4
  ret i32 %t13
}
