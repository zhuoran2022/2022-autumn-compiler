define i32 @fun(i32,i32) {
B30:
  %t33 = alloca i32, align 4
  %t32 = alloca i32, align 4
  %t31 = alloca i32, align 4
  store i32 %0, i32* %t31, align 4
  store i32 %1, i32* %t32, align 4
  br label %B35
B35:                               	; preds = %B30, %B34
  %t3 = load i32, i32* %t32, align 4
  %t4 = icmp sgt i32 %t3, 0
  br i1 %t4, label %B34, label %B39
B34:                               	; preds = %B35
  %t6 = load i32, i32* %t31, align 4
  %t7 = load i32, i32* %t32, align 4
  %t8 = srem i32 %t6, %t7
  store i32 %t8, i32* %t33, align 4
  %t10 = load i32, i32* %t32, align 4
  store i32 %t10, i32* %t31, align 4
  %t12 = load i32, i32* %t33, align 4
  store i32 %t12, i32* %t32, align 4
  br label %B35
B39:                               	; preds = %B35
  br label %B36
B36:                               	; preds = %B39
  %t13 = load i32, i32* %t31, align 4
  ret i32 %t13
}
define i32 @main() {
B40:
  %t43 = alloca i32, align 4
  %t42 = alloca i32, align 4
  %t41 = alloca i32, align 4
  %t19 = call i32 @getint()
  store i32 %t19, i32* %t42, align 4
  %t22 = call i32 @getint()
  store i32 %t22, i32* %t41, align 4
  %t26 = load i32, i32* %t42, align 4
  %t27 = load i32, i32* %t41, align 4
  %t25 = call i32 @fun( i32 %t26 , i32 %t27 )
  store i32 %t25, i32* %t43, align 4
  %t29 = load i32, i32* %t43, align 4
  call void @putint( i32 %t29)
  ret i32 0
}
declare i32 @getint()
declare void @putint(i32)
