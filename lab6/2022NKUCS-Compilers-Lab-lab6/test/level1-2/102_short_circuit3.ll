@a = global i32 0, align 4
@b = global i32 0, align 4
@d = global i32 0, align 4
define i32 @set_a(i32) {
B150:
  %t151 = alloca i32, align 4
  store i32 %0, i32* %t151, align 4
  %t5 = load i32, i32* %t151, align 4
  store i32 %t5, i32* @a, align 4
  %t6 = load i32, i32* @a, align 4
  ret i32 %t6
}
define i32 @set_b(i32) {
B152:
  %t153 = alloca i32, align 4
  store i32 %0, i32* %t153, align 4
  %t9 = load i32, i32* %t153, align 4
  store i32 %t9, i32* @b, align 4
  %t10 = load i32, i32* @b, align 4
  ret i32 %t10
}
define i32 @set_d(i32) {
B154:
  %t155 = alloca i32, align 4
  store i32 %0, i32* %t155, align 4
  %t13 = load i32, i32* %t155, align 4
  store i32 %t13, i32* @d, align 4
  %t14 = load i32, i32* @d, align 4
  ret i32 %t14
}
define i32 @main() {
B156:
  %t233 = alloca i32, align 4
  %t232 = alloca i32, align 4
  %t231 = alloca i32, align 4
  %t230 = alloca i32, align 4
  %t229 = alloca i32, align 4
  %t175 = alloca i32, align 4
  store i32 2, i32* @a, align 4
  store i32 3, i32* @b, align 4
  %t18 = call i32 @set_a( i32 0)
  %t22 = icmp ne i32 %t18, 0
  br i1 %t22, label %B159, label %B161
B159:                               	; preds = %B156
  %t20 = call i32 @set_b( i32 1)
  %t23 = icmp ne i32 %t20, 0
  br i1 %t23, label %B157, label %B164
B161:                               	; preds = %B156
  br label %B158
B157:                               	; preds = %B159
  br label %B158
B164:                               	; preds = %B159
  br label %B158
B158:                               	; preds = %B157, %B161, %B164
  %t25 = load i32, i32* @a, align 4
  call void @putint( i32 %t25)
  call void @putch( i32 32)
  %t28 = load i32, i32* @b, align 4
  call void @putint( i32 %t28)
  call void @putch( i32 32)
  store i32 2, i32* @a, align 4
  store i32 3, i32* @b, align 4
  %t33 = call i32 @set_a( i32 0)
  %t37 = icmp ne i32 %t33, 0
  br i1 %t37, label %B168, label %B170
B168:                               	; preds = %B158
  %t35 = call i32 @set_b( i32 1)
  %t38 = icmp ne i32 %t35, 0
  br i1 %t38, label %B166, label %B173
B170:                               	; preds = %B158
  br label %B167
B166:                               	; preds = %B168
  br label %B167
B173:                               	; preds = %B168
  br label %B167
B167:                               	; preds = %B166, %B170, %B173
  %t40 = load i32, i32* @a, align 4
  call void @putint( i32 %t40)
  call void @putch( i32 32)
  %t43 = load i32, i32* @b, align 4
  call void @putint( i32 %t43)
  call void @putch( i32 10)
  store i32 1, i32* %t175, align 4
  store i32 2, i32* @d, align 4
  %t47 = load i32, i32* %t175, align 4
  %t48 = icmp sge i32 %t47, 1
  br i1 %t48, label %B178, label %B181
B178:                               	; preds = %B167
  %t50 = call i32 @set_d( i32 3)
  %t52 = icmp ne i32 %t50, 0
  br i1 %t52, label %B176, label %B183
B181:                               	; preds = %B167
  br label %B177
B176:                               	; preds = %B178
  br label %B177
B183:                               	; preds = %B178
  br label %B177
B177:                               	; preds = %B176, %B181, %B183
  %t54 = load i32, i32* @d, align 4
  call void @putint( i32 %t54)
  call void @putch( i32 32)
  %t56 = load i32, i32* %t175, align 4
  %t57 = icmp sle i32 %t56, 1
  br i1 %t57, label %B185, label %B190
B185:                               	; preds = %B177, %B187
  br label %B186
B190:                               	; preds = %B177
  br label %B187
B186:                               	; preds = %B185, %B192
  %t63 = load i32, i32* @d, align 4
  call void @putint( i32 %t63)
  call void @putch( i32 10)
  %t65 = add i32 2, 1
  %t66 = sub i32 3, %t65
  %t67 = icmp sge i32 16, %t66
  br i1 %t67, label %B194, label %B198
B187:                               	; preds = %B190
  %t59 = call i32 @set_d( i32 4)
  %t61 = icmp ne i32 %t59, 0
  br i1 %t61, label %B185, label %B192
B194:                               	; preds = %B186
  call void @putch( i32 65)
  br label %B195
B198:                               	; preds = %B186
  br label %B195
B192:                               	; preds = %B187
  br label %B186
B195:                               	; preds = %B194, %B198
  %t69 = sub i32 25, 7
  %t70 = mul i32 6, 3
  %t71 = sub i32 36, %t70
  %t72 = icmp ne i32 %t69, %t71
  br i1 %t72, label %B199, label %B203
B199:                               	; preds = %B195
  call void @putch( i32 66)
  br label %B200
B203:                               	; preds = %B195
  br label %B200
B200:                               	; preds = %B199, %B203
  %t74 = icmp slt i32 1, 8
  %t75 = srem i32 7, 2
  %t206 = zext i1 %t74 to i32
  %t76 = icmp ne i32 %t206, %t75
  br i1 %t76, label %B204, label %B209
B204:                               	; preds = %B200
  call void @putch( i32 67)
  br label %B205
B209:                               	; preds = %B200
  br label %B205
B205:                               	; preds = %B204, %B209
  %t78 = icmp sgt i32 3, 4
  %t212 = zext i1 %t78 to i32
  %t79 = icmp eq i32 %t212, 0
  br i1 %t79, label %B210, label %B215
B210:                               	; preds = %B205
  call void @putch( i32 68)
  br label %B211
B215:                               	; preds = %B205
  br label %B211
B211:                               	; preds = %B210, %B215
  %t81 = icmp sle i32 102, 63
  %t218 = zext i1 %t81 to i32
  %t82 = icmp eq i32 1, %t218
  br i1 %t82, label %B216, label %B221
B216:                               	; preds = %B211
  call void @putch( i32 69)
  br label %B217
B221:                               	; preds = %B211
  br label %B217
B217:                               	; preds = %B216, %B221
  %t84 = sub i32 5, 6
  %t224 = icmp ne i32 0, 0
  %t85 = xor i1 %t224, true
  %t225 = zext i1 %t85 to i32
  %t86 = sub i32 0, %t225
  %t87 = icmp eq i32 %t84, %t86
  br i1 %t87, label %B222, label %B228
B222:                               	; preds = %B217
  call void @putch( i32 70)
  br label %B223
B228:                               	; preds = %B217
  br label %B223
B223:                               	; preds = %B222, %B228
  call void @putch( i32 10)
  store i32 0, i32* %t229, align 4
  store i32 1, i32* %t230, align 4
  store i32 2, i32* %t231, align 4
  store i32 3, i32* %t232, align 4
  store i32 4, i32* %t233, align 4
  br label %B235
B235:                               	; preds = %B223, %B234
  %t95 = load i32, i32* %t229, align 4
  %t98 = icmp ne i32 %t95, 0
  br i1 %t98, label %B237, label %B239
B237:                               	; preds = %B235
  %t96 = load i32, i32* %t230, align 4
  %t99 = icmp ne i32 %t96, 0
  br i1 %t99, label %B234, label %B242
B239:                               	; preds = %B235
  br label %B236
B234:                               	; preds = %B237
  call void @putch( i32 32)
  br label %B235
B242:                               	; preds = %B237
  br label %B236
B236:                               	; preds = %B239, %B242
  %t101 = load i32, i32* %t229, align 4
  %t104 = icmp ne i32 %t101, 0
  br i1 %t104, label %B244, label %B248
B244:                               	; preds = %B236, %B246
  call void @putch( i32 67)
  br label %B245
B248:                               	; preds = %B236
  br label %B246
B245:                               	; preds = %B244, %B251
  %t107 = load i32, i32* %t229, align 4
  %t108 = load i32, i32* %t230, align 4
  %t109 = icmp sge i32 %t107, %t108
  br i1 %t109, label %B253, label %B258
B246:                               	; preds = %B248
  %t102 = load i32, i32* %t230, align 4
  %t105 = icmp ne i32 %t102, 0
  br i1 %t105, label %B244, label %B251
B253:                               	; preds = %B245, %B255
  call void @putch( i32 72)
  br label %B254
B258:                               	; preds = %B245
  br label %B255
B251:                               	; preds = %B246
  br label %B245
B254:                               	; preds = %B253, %B261
  %t115 = load i32, i32* %t231, align 4
  %t116 = load i32, i32* %t230, align 4
  %t117 = icmp sge i32 %t115, %t116
  br i1 %t117, label %B264, label %B267
B255:                               	; preds = %B258
  %t110 = load i32, i32* %t230, align 4
  %t111 = load i32, i32* %t229, align 4
  %t112 = icmp sle i32 %t110, %t111
  br i1 %t112, label %B253, label %B261
B264:                               	; preds = %B254
  %t118 = load i32, i32* %t233, align 4
  %t119 = load i32, i32* %t232, align 4
  %t120 = icmp ne i32 %t118, %t119
  br i1 %t120, label %B262, label %B270
B267:                               	; preds = %B254
  br label %B263
B261:                               	; preds = %B255
  br label %B254
B262:                               	; preds = %B264
  call void @putch( i32 73)
  br label %B263
B270:                               	; preds = %B264
  br label %B263
B263:                               	; preds = %B262, %B267, %B270
  %t123 = load i32, i32* %t229, align 4
  %t124 = load i32, i32* %t230, align 4
  %t275 = icmp ne i32 %t124, 0
  %t125 = xor i1 %t275, true
  %t276 = zext i1 %t125 to i32
  %t126 = icmp eq i32 %t123, %t276
  br i1 %t126, label %B274, label %B279
B274:                               	; preds = %B263
  %t127 = load i32, i32* %t232, align 4
  %t128 = load i32, i32* %t232, align 4
  %t129 = icmp slt i32 %t127, %t128
  br i1 %t129, label %B271, label %B282
B279:                               	; preds = %B263
  br label %B273
B271:                               	; preds = %B273, %B274
  call void @putch( i32 74)
  br label %B272
B282:                               	; preds = %B274
  br label %B273
B273:                               	; preds = %B279, %B282
  %t131 = load i32, i32* %t233, align 4
  %t132 = load i32, i32* %t233, align 4
  %t133 = icmp sge i32 %t131, %t132
  br i1 %t133, label %B271, label %B285
B272:                               	; preds = %B271, %B285
  %t136 = load i32, i32* %t229, align 4
  %t137 = load i32, i32* %t230, align 4
  %t289 = icmp ne i32 %t137, 0
  %t138 = xor i1 %t289, true
  %t290 = zext i1 %t138 to i32
  %t139 = icmp eq i32 %t136, %t290
  br i1 %t139, label %B286, label %B293
B285:                               	; preds = %B273
  br label %B272
B286:                               	; preds = %B272, %B294
  call void @putch( i32 75)
  br label %B287
B293:                               	; preds = %B272
  br label %B288
B287:                               	; preds = %B286, %B297, %B300
  call void @putch( i32 10)
  ret i32 0
B288:                               	; preds = %B293
  %t140 = load i32, i32* %t232, align 4
  %t141 = load i32, i32* %t232, align 4
  %t142 = icmp slt i32 %t140, %t141
  br i1 %t142, label %B294, label %B297
B294:                               	; preds = %B288
  %t143 = load i32, i32* %t233, align 4
  %t144 = load i32, i32* %t233, align 4
  %t145 = icmp sge i32 %t143, %t144
  br i1 %t145, label %B286, label %B300
B297:                               	; preds = %B288
  br label %B287
B300:                               	; preds = %B294
  br label %B287
}
declare void @putint(i32)
declare void @putch(i32)
