define i32 @enc(i32) {
B36:
  %t37 = alloca i32, align 4
  store i32 %0, i32* %t37, align 4
  %t1 = load i32, i32* %t37, align 4
  %t2 = icmp sgt i32 %t1, 25
  br i1 %t2, label %B38, label %B43
B38:                               	; preds = %B36
  %t4 = load i32, i32* %t37, align 4
  %t5 = add i32 %t4, 60
  store i32 %t5, i32* %t37, align 4
  br label %B40
B43:                               	; preds = %B36
  br label %B39
B40:                               	; preds = %B38, %B39
  %t9 = load i32, i32* %t37, align 4
  ret i32 %t9
B39:                               	; preds = %B43
  %t7 = load i32, i32* %t37, align 4
  %t8 = sub i32 %t7, 15
  store i32 %t8, i32* %t37, align 4
  br label %B40
}
define i32 @dec(i32) {
B44:
  %t45 = alloca i32, align 4
  store i32 %0, i32* %t45, align 4
  %t11 = load i32, i32* %t45, align 4
  %t12 = icmp sgt i32 %t11, 85
  br i1 %t12, label %B46, label %B51
B46:                               	; preds = %B44
  %t14 = load i32, i32* %t45, align 4
  %t15 = sub i32 %t14, 59
  store i32 %t15, i32* %t45, align 4
  br label %B48
B51:                               	; preds = %B44
  br label %B47
B48:                               	; preds = %B46, %B47
  %t19 = load i32, i32* %t45, align 4
  ret i32 %t19
B47:                               	; preds = %B51
  %t17 = load i32, i32* %t45, align 4
  %t18 = add i32 %t17, 14
  store i32 %t18, i32* %t45, align 4
  br label %B48
}
define i32 @main() {
B52:
  %t54 = alloca i32, align 4
  %t53 = alloca i32, align 4
  store i32 400, i32* %t53, align 4
  %t26 = load i32, i32* %t53, align 4
  %t25 = call i32 @enc( i32 %t26)
  store i32 %t25, i32* %t54, align 4
  %t30 = load i32, i32* %t54, align 4
  %t29 = call i32 @dec( i32 %t30)
  store i32 %t29, i32* %t54, align 4
  %t32 = load i32, i32* %t54, align 4
  call void @putint( i32 %t32)
  store i32 10, i32* %t54, align 4
  %t35 = load i32, i32* %t54, align 4
  call void @putch( i32 %t35)
  ret i32 0
}
declare void @putint(i32)
declare void @putch(i32)
