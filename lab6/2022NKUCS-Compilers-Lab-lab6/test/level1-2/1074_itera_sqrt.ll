define i32 @fsqrt(i32) {
B34:
  %t37 = alloca i32, align 4
  %t36 = alloca i32, align 4
  %t35 = alloca i32, align 4
  store i32 %0, i32* %t35, align 4
  store i32 0, i32* %t36, align 4
  %t4 = load i32, i32* %t35, align 4
  %t5 = sdiv i32 %t4, 2
  store i32 %t5, i32* %t37, align 4
  br label %B39
B39:                               	; preds = %B34, %B38
  %t6 = load i32, i32* %t36, align 4
  %t7 = load i32, i32* %t37, align 4
  %t8 = sub i32 %t6, %t7
  %t9 = icmp ne i32 %t8, 0
  br i1 %t9, label %B38, label %B43
B38:                               	; preds = %B39
  %t11 = load i32, i32* %t37, align 4
  store i32 %t11, i32* %t36, align 4
  %t13 = load i32, i32* %t36, align 4
  %t14 = load i32, i32* %t35, align 4
  %t15 = load i32, i32* %t36, align 4
  %t16 = sdiv i32 %t14, %t15
  %t17 = add i32 %t13, %t16
  store i32 %t17, i32* %t37, align 4
  %t19 = load i32, i32* %t37, align 4
  %t20 = sdiv i32 %t19, 2
  store i32 %t20, i32* %t37, align 4
  br label %B39
B43:                               	; preds = %B39
  br label %B40
B40:                               	; preds = %B43
  %t21 = load i32, i32* %t37, align 4
  ret i32 %t21
}
define i32 @main() {
B44:
  %t46 = alloca i32, align 4
  %t45 = alloca i32, align 4
  store i32 400, i32* %t45, align 4
  %t28 = load i32, i32* %t45, align 4
  %t27 = call i32 @fsqrt( i32 %t28)
  store i32 %t27, i32* %t46, align 4
  %t30 = load i32, i32* %t46, align 4
  call void @putint( i32 %t30)
  store i32 10, i32* %t46, align 4
  %t33 = load i32, i32* %t46, align 4
  call void @putch( i32 %t33)
  ret i32 0
}
declare void @putint(i32)
declare void @putch(i32)
