@s = global i32 0, align 4
define i32 @get_ans_se(i32,i32,i32) {
B206:
  %t210 = alloca i32, align 4
  %t209 = alloca i32, align 4
  %t208 = alloca i32, align 4
  %t207 = alloca i32, align 4
  store i32 %0, i32* %t207, align 4
  store i32 %1, i32* %t208, align 4
  store i32 %2, i32* %t209, align 4
  store i32 0, i32* %t210, align 4
  %t5 = load i32, i32* %t208, align 4
  %t6 = load i32, i32* %t209, align 4
  %t7 = icmp eq i32 %t5, %t6
  br i1 %t7, label %B211, label %B215
B211:                               	; preds = %B206
  store i32 1, i32* %t210, align 4
  br label %B212
B215:                               	; preds = %B206
  br label %B212
B212:                               	; preds = %B211, %B215
  %t10 = load i32, i32* %t207, align 4
  %t11 = mul i32 %t10, 2
  store i32 %t11, i32* %t207, align 4
  %t13 = load i32, i32* %t207, align 4
  %t14 = load i32, i32* %t210, align 4
  %t15 = add i32 %t13, %t14
  store i32 %t15, i32* %t207, align 4
  %t17 = load i32, i32* @s, align 4
  %t18 = load i32, i32* %t207, align 4
  %t19 = add i32 %t17, %t18
  store i32 %t19, i32* @s, align 4
  %t20 = load i32, i32* %t207, align 4
  ret i32 %t20
}
define i32 @get_ans(i32,i32,i32) {
B216:
  %t220 = alloca i32, align 4
  %t219 = alloca i32, align 4
  %t218 = alloca i32, align 4
  %t217 = alloca i32, align 4
  store i32 %0, i32* %t217, align 4
  store i32 %1, i32* %t218, align 4
  store i32 %2, i32* %t219, align 4
  store i32 0, i32* %t220, align 4
  %t25 = load i32, i32* %t218, align 4
  %t26 = load i32, i32* %t219, align 4
  %t27 = icmp eq i32 %t25, %t26
  br i1 %t27, label %B221, label %B225
B221:                               	; preds = %B216
  store i32 1, i32* %t220, align 4
  br label %B222
B225:                               	; preds = %B216
  br label %B222
B222:                               	; preds = %B221, %B225
  %t30 = load i32, i32* %t217, align 4
  %t31 = mul i32 %t30, 2
  store i32 %t31, i32* %t217, align 4
  %t33 = load i32, i32* %t217, align 4
  %t34 = load i32, i32* %t220, align 4
  %t35 = add i32 %t33, %t34
  store i32 %t35, i32* %t217, align 4
  %t36 = load i32, i32* %t217, align 4
  ret i32 %t36
}
define i32 @main() {
B226:
  %t235 = alloca i32, align 4
  %t234 = alloca i32, align 4
  %t233 = alloca i32, align 4
  %t232 = alloca i32, align 4
  %t231 = alloca i32, align 4
  %t230 = alloca i32, align 4
  %t229 = alloca i32, align 4
  %t228 = alloca i32, align 4
  %t227 = alloca i32, align 4
  %t37 = sub i32 0, -2147483648
  store i32 %t37, i32* %t227, align 4
  store i32 -2147483648, i32* %t228, align 4
  %t40 = add i32 -2147483648, 1
  store i32 %t40, i32* %t229, align 4
  store i32 2147483647, i32* %t230, align 4
  %t43 = sub i32 2147483647, 1
  store i32 %t43, i32* %t231, align 4
  %t52 = load i32, i32* %t227, align 4
  %t53 = load i32, i32* %t228, align 4
  %t51 = call i32 @get_ans( i32 0 , i32 %t52 , i32 %t53 )
  store i32 %t51, i32* %t232, align 4
  %t57 = load i32, i32* %t232, align 4
  %t58 = load i32, i32* %t227, align 4
  %t59 = add i32 %t58, 1
  %t60 = load i32, i32* %t229, align 4
  %t56 = call i32 @get_ans( i32 %t57 , i32 %t59 , i32 %t60 )
  store i32 %t56, i32* %t232, align 4
  %t64 = load i32, i32* %t232, align 4
  %t65 = load i32, i32* %t227, align 4
  %t66 = load i32, i32* %t230, align 4
  %t67 = sub i32 0, %t66
  %t68 = sub i32 %t67, 1
  %t63 = call i32 @get_ans( i32 %t64 , i32 %t65 , i32 %t68 )
  store i32 %t63, i32* %t232, align 4
  %t72 = load i32, i32* %t232, align 4
  %t73 = load i32, i32* %t227, align 4
  %t74 = load i32, i32* %t231, align 4
  %t75 = add i32 %t74, 1
  %t71 = call i32 @get_ans( i32 %t72 , i32 %t73 , i32 %t75 )
  store i32 %t71, i32* %t232, align 4
  %t79 = load i32, i32* %t232, align 4
  %t80 = load i32, i32* %t228, align 4
  %t81 = sdiv i32 %t80, 2
  %t82 = load i32, i32* %t229, align 4
  %t83 = sdiv i32 %t82, 2
  %t78 = call i32 @get_ans( i32 %t79 , i32 %t81 , i32 %t83 )
  store i32 %t78, i32* %t232, align 4
  %t87 = load i32, i32* %t232, align 4
  %t88 = load i32, i32* %t228, align 4
  %t89 = load i32, i32* %t230, align 4
  %t90 = sub i32 0, %t89
  %t91 = sub i32 %t90, 1
  %t86 = call i32 @get_ans( i32 %t87 , i32 %t88 , i32 %t91 )
  store i32 %t86, i32* %t232, align 4
  %t95 = load i32, i32* %t232, align 4
  %t96 = load i32, i32* %t228, align 4
  %t97 = load i32, i32* %t231, align 4
  %t98 = add i32 %t97, 1
  %t94 = call i32 @get_ans( i32 %t95 , i32 %t96 , i32 %t98 )
  store i32 %t94, i32* %t232, align 4
  %t102 = load i32, i32* %t229, align 4
  %t103 = load i32, i32* %t230, align 4
  %t101 = call i32 @get_ans( i32 0 , i32 %t102 , i32 %t103 )
  store i32 %t101, i32* %t233, align 4
  %t107 = load i32, i32* %t233, align 4
  %t108 = load i32, i32* %t229, align 4
  %t109 = load i32, i32* %t231, align 4
  %t106 = call i32 @get_ans( i32 %t107 , i32 %t108 , i32 %t109 )
  store i32 %t106, i32* %t233, align 4
  %t113 = load i32, i32* %t233, align 4
  %t114 = load i32, i32* %t230, align 4
  %t115 = load i32, i32* %t231, align 4
  %t112 = call i32 @get_ans( i32 %t113 , i32 %t114 , i32 %t115 )
  store i32 %t112, i32* %t233, align 4
  %t119 = load i32, i32* %t233, align 4
  %t120 = load i32, i32* %t227, align 4
  %t121 = sdiv i32 %t120, 2
  %t122 = load i32, i32* %t228, align 4
  %t123 = sdiv i32 %t122, 2
  %t118 = call i32 @get_ans( i32 %t119 , i32 %t121 , i32 %t123 )
  store i32 %t118, i32* %t233, align 4
  %t127 = load i32, i32* %t227, align 4
  %t128 = load i32, i32* %t228, align 4
  %t126 = call i32 @get_ans_se( i32 0 , i32 %t127 , i32 %t128 )
  store i32 %t126, i32* %t234, align 4
  %t132 = load i32, i32* %t234, align 4
  %t133 = load i32, i32* %t227, align 4
  %t134 = add i32 %t133, 1
  %t135 = load i32, i32* %t229, align 4
  %t131 = call i32 @get_ans_se( i32 %t132 , i32 %t134 , i32 %t135 )
  store i32 %t131, i32* %t234, align 4
  %t139 = load i32, i32* %t234, align 4
  %t140 = load i32, i32* %t227, align 4
  %t141 = load i32, i32* %t230, align 4
  %t142 = sub i32 0, %t141
  %t143 = sub i32 %t142, 1
  %t138 = call i32 @get_ans_se( i32 %t139 , i32 %t140 , i32 %t143 )
  store i32 %t138, i32* %t234, align 4
  %t147 = load i32, i32* %t234, align 4
  %t148 = load i32, i32* %t227, align 4
  %t149 = load i32, i32* %t231, align 4
  %t150 = add i32 %t149, 1
  %t146 = call i32 @get_ans_se( i32 %t147 , i32 %t148 , i32 %t150 )
  store i32 %t146, i32* %t234, align 4
  %t154 = load i32, i32* %t234, align 4
  %t155 = load i32, i32* %t228, align 4
  %t156 = sdiv i32 %t155, 2
  %t157 = load i32, i32* %t229, align 4
  %t158 = sdiv i32 %t157, 2
  %t153 = call i32 @get_ans_se( i32 %t154 , i32 %t156 , i32 %t158 )
  store i32 %t153, i32* %t234, align 4
  %t162 = load i32, i32* %t234, align 4
  %t163 = load i32, i32* %t228, align 4
  %t164 = load i32, i32* %t230, align 4
  %t165 = sub i32 0, %t164
  %t166 = sub i32 %t165, 1
  %t161 = call i32 @get_ans_se( i32 %t162 , i32 %t163 , i32 %t166 )
  store i32 %t161, i32* %t234, align 4
  %t170 = load i32, i32* %t234, align 4
  %t171 = load i32, i32* %t228, align 4
  %t172 = load i32, i32* %t231, align 4
  %t173 = add i32 %t172, 1
  %t169 = call i32 @get_ans_se( i32 %t170 , i32 %t171 , i32 %t173 )
  store i32 %t169, i32* %t234, align 4
  %t177 = load i32, i32* %t229, align 4
  %t178 = load i32, i32* %t230, align 4
  %t176 = call i32 @get_ans_se( i32 0 , i32 %t177 , i32 %t178 )
  store i32 %t176, i32* %t235, align 4
  %t182 = load i32, i32* %t235, align 4
  %t183 = load i32, i32* %t229, align 4
  %t184 = load i32, i32* %t231, align 4
  %t181 = call i32 @get_ans_se( i32 %t182 , i32 %t183 , i32 %t184 )
  store i32 %t181, i32* %t235, align 4
  %t188 = load i32, i32* %t235, align 4
  %t189 = load i32, i32* %t230, align 4
  %t190 = load i32, i32* %t231, align 4
  %t187 = call i32 @get_ans_se( i32 %t188 , i32 %t189 , i32 %t190 )
  store i32 %t187, i32* %t235, align 4
  %t194 = load i32, i32* %t235, align 4
  %t195 = load i32, i32* %t227, align 4
  %t196 = sdiv i32 %t195, 2
  %t197 = load i32, i32* %t228, align 4
  %t198 = sdiv i32 %t197, 2
  %t193 = call i32 @get_ans_se( i32 %t194 , i32 %t196 , i32 %t198 )
  store i32 %t193, i32* %t235, align 4
  %t199 = load i32, i32* %t232, align 4
  %t200 = load i32, i32* %t233, align 4
  %t201 = add i32 %t199, %t200
  %t202 = load i32, i32* %t234, align 4
  %t203 = add i32 %t201, %t202
  %t204 = load i32, i32* %t235, align 4
  %t205 = add i32 %t203, %t204
  ret i32 %t205
}
