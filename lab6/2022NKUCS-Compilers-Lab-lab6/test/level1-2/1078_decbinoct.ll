define i32 @dec2bin(i32) {
B39:
  %t44 = alloca i32, align 4
  %t43 = alloca i32, align 4
  %t42 = alloca i32, align 4
  %t41 = alloca i32, align 4
  %t40 = alloca i32, align 4
  store i32 %0, i32* %t40, align 4
  store i32 0, i32* %t41, align 4
  store i32 1, i32* %t42, align 4
  %t8 = load i32, i32* %t40, align 4
  store i32 %t8, i32* %t44, align 4
  br label %B46
B46:                               	; preds = %B39, %B45
  %t9 = load i32, i32* %t44, align 4
  %t10 = icmp ne i32 %t9, 0
  br i1 %t10, label %B45, label %B50
B45:                               	; preds = %B46
  %t12 = load i32, i32* %t44, align 4
  %t13 = srem i32 %t12, 2
  store i32 %t13, i32* %t43, align 4
  %t15 = load i32, i32* %t42, align 4
  %t16 = load i32, i32* %t43, align 4
  %t17 = mul i32 %t15, %t16
  %t18 = load i32, i32* %t41, align 4
  %t19 = add i32 %t17, %t18
  store i32 %t19, i32* %t41, align 4
  %t21 = load i32, i32* %t42, align 4
  %t22 = mul i32 %t21, 10
  store i32 %t22, i32* %t42, align 4
  %t24 = load i32, i32* %t44, align 4
  %t25 = sdiv i32 %t24, 2
  store i32 %t25, i32* %t44, align 4
  br label %B46
B50:                               	; preds = %B46
  br label %B47
B47:                               	; preds = %B50
  %t26 = load i32, i32* %t41, align 4
  ret i32 %t26
}
define i32 @main() {
B51:
  %t53 = alloca i32, align 4
  %t52 = alloca i32, align 4
  store i32 400, i32* %t52, align 4
  %t33 = load i32, i32* %t52, align 4
  %t32 = call i32 @dec2bin( i32 %t33)
  store i32 %t32, i32* %t53, align 4
  %t35 = load i32, i32* %t53, align 4
  call void @putint( i32 %t35)
  store i32 10, i32* %t53, align 4
  %t38 = load i32, i32* %t53, align 4
  call void @putch( i32 %t38)
  ret i32 0
}
declare void @putint(i32)
declare void @putch(i32)
