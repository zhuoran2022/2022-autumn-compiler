@n = global i32 0, align 4
define i32 @main() {
B26:
  %t29 = alloca i32, align 4
  %t28 = alloca i32, align 4
  %t27 = alloca i32, align 4
  %t5 = call i32 @getint()
  store i32 %t5, i32* %t27, align 4
  %t8 = call i32 @getint()
  store i32 %t8, i32* %t28, align 4
  %t11 = load i32, i32* %t27, align 4
  store i32 %t11, i32* %t29, align 4
  %t13 = load i32, i32* %t28, align 4
  store i32 %t13, i32* %t27, align 4
  %t15 = load i32, i32* %t29, align 4
  store i32 %t15, i32* %t28, align 4
  %t17 = load i32, i32* %t27, align 4
  call void @putint( i32 %t17)
  store i32 10, i32* %t29, align 4
  %t20 = load i32, i32* %t29, align 4
  call void @putch( i32 %t20)
  %t22 = load i32, i32* %t28, align 4
  call void @putint( i32 %t22)
  store i32 10, i32* %t29, align 4
  %t25 = load i32, i32* %t29, align 4
  call void @putch( i32 %t25)
  ret i32 0
}
declare i32 @getint()
declare void @putint(i32)
declare void @putch(i32)
