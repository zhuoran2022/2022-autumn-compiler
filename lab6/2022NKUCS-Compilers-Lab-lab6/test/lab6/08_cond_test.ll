define i32 @main() {
B25:
  %t34 = alloca i32, align 4
  %t27 = alloca i32, align 4
  %t26 = alloca i32, align 4
  store i32 0, i32* %t26, align 4
  store i32 0, i32* %t27, align 4
  br label %B29
B29:                               	; preds = %B25, %B37
  %t2 = load i32, i32* %t27, align 4
  %t3 = icmp slt i32 %t2, 10
  br i1 %t3, label %B28, label %B33
B28:                               	; preds = %B29
  store i32 10, i32* %t34, align 4
  br label %B36
B33:                               	; preds = %B29
  br label %B30
B36:                               	; preds = %B28, %B42
  %t5 = load i32, i32* %t34, align 4
  %t20 = icmp ne i32 %t5, 0
  br i1 %t20, label %B35, label %B39
B30:                               	; preds = %B33
  ret i32 0
B35:                               	; preds = %B36
  %t6 = load i32, i32* %t27, align 4
  %t9 = icmp ne i32 %t6, 0
  br i1 %t9, label %B43, label %B45
B39:                               	; preds = %B36
  br label %B37
B43:                               	; preds = %B35
  %t7 = load i32, i32* %t34, align 4
  %t10 = icmp ne i32 %t7, 0
  br i1 %t10, label %B41, label %B48
B45:                               	; preds = %B35
  br label %B42
B37:                               	; preds = %B39
  %t22 = load i32, i32* %t27, align 4
  %t23 = load i32, i32* %t27, align 4
  %t24 = add i32 %t22, %t23
  store i32 %t24, i32* %t27, align 4
  br label %B29
B41:                               	; preds = %B43
  %t12 = load i32, i32* %t26, align 4
  %t13 = load i32, i32* %t27, align 4
  %t14 = add i32 %t12, %t13
  %t15 = load i32, i32* %t34, align 4
  %t16 = add i32 %t14, %t15
  store i32 %t16, i32* %t26, align 4
  br label %B42
B48:                               	; preds = %B43
  br label %B42
B42:                               	; preds = %B41, %B45, %B48
  %t18 = load i32, i32* %t34, align 4
  %t19 = sub i32 %t18, 1
  store i32 %t19, i32* %t34, align 4
  br label %B36
}
