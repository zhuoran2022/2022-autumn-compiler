%{
/************************************************************
expr2.y
YACC file
Date: 2022/10/11
wzr <2011579@nankai.edu.cn>
************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#ifndef YYSTYPE
#define YYSTYPE char*
#endif
char idStr[50];
char numStr[50];
int yylex();
extern int yyparse();
FILE* yyin;
void yyerror(const char* s);
%}

%token NUMBER
%token ID
%token ADD
%token SUB
%token MUL
%token DIV
%left ADD SUB
%left MUL DIV
%right UMINUS

%%


lines	:	lines expr ';'	{ printf("res = %s\n", $2);  }
	|	lines ';'
	|
	;

expr	:	expr ADD expr	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, $1);strcat($$, $3);strcat($$, "+ "); }//printf("$1 = %f\n", $1); printf("$3 = %f\n", $3); printf("$$ = %f\n", $$); }
	|	expr SUB expr	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, $1);strcat($$, $3);strcat($$, "- "); }
	|	expr MUL expr	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, $1);strcat($$, $3);strcat($$, "* "); }
	|	expr DIV expr	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, $1);strcat($$, $3);strcat($$, "/ "); }
	|	'(' expr ')'	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, $2);/*strcat($$, " ");*/ }//no need to append " ", expr will add " " in other candidates
	|	'-' expr %prec UMINUS	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, "-"); strcat($$, $2); }//no need to append " ", expr will add " " in other candidates
	|	NUMBER	{ $$ = (char*)malloc(50*sizeof(char)); strcpy($$, $1);strcat($$, " "); }
	|	ID {$$ = (char*)malloc(50*sizeof(char)); strcpy($$, $1);strcat($$, " ");}
	;

/*
NUMBER	:	'0'	{ $$ = 0.0; }
	|	'1'	{ $$ = 1.0; }
	|	'2'	{ $$ = 2.0; }
	|	'3'	{ $$ = 3.0; }
	|	'4'	{ $$ = 4.0; }
	|	'5'	{ $$ = 5.0; }
	|	'6'	{ $$ = 6.0; }
	|	'7'	{ $$ = 7.0; }
	|	'8'	{ $$ = 8.0; }
	|	'9'	{ $$ = 9.0; }
	;*/


%%

//programs section

int isLetter(char t){
	if((t >= 'a' && t <= 'z') || (t >= 'A' && t <= 'Z'))
		return 1;
	return 0;
}

int yylex() {
	//place your token retrieving code here
	char t;
	while (1) {
		t = getchar();
		//printf("%sfirst getchar\n", &t);
		if (t == ' ' || t == '\t' || t == '\n') {
			//do nothing
		}
		else if(isdigit(t)) {
			//printf("isdigit(t) == 1\n");
			int c = 0;
			yylval = 0;
			//double myDigit = 0;
			while (isdigit(t)) {
				numStr[c++] = t;
				//yylval = yylval * 10 + t - '0';
				//printf("t = %s\n", &t);
				//double b = t;
				//printf("b = %f\n", b);
				//myDigit = myDigit * 10 + b - '0';
				//printf("myDigit = %f\n", myDigit);
				t = getchar();
			}
			numStr[c] = '\0';
			yylval = numStr;
			//printf("myDigit = %f\n", myDigit);
			//yylval = myDigit;
			//printf("yylval = %f\n", yylval);
			//printf("NUMBER = %f\n", NUMBER);
			ungetc(t, stdin);
			
			//printf("%f\n", NUMBER);
			return NUMBER;
		}
		else if((isLetter(t)==1) || (t == '_')){
			int c = 0;
			while((isLetter(t)==1) || (t == '_') || (isdigit(t))){
				idStr[c++] = t;
				t = getchar();
			}
			idStr[c] = '\0';
			yylval = idStr;
			ungetc(t, stdin);
			return ID;
		}		
		else if (t == '+') {
			return ADD;
		}
		else  if(t == '-'){return SUB;}
		else  if(t == '*'){return MUL;}
		else  if(t == '/'){return DIV;}
		else  if(t == '('){return '(';}
		else  if(t == ')'){return ')';}
		else {
			return t;
		}
	}
}

int main(void) {
	yyin = stdin;
	do {
		//yylex();
		yyparse();
	} while (!feof(yyin));
	return 0;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}
