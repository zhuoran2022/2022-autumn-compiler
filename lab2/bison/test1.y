%{
/************************************************************
expr1.y
YACC file
Date: 2022/10/11
wzr <2011579@nankai.edu.cn>
************************************************************/
#include<stdio.h>
#include<stdlib.h>
#ifndef YYSTYPE
#define YYSTYPE double
#endif
int yylex();
extern int yyparse();
FILE* yyin;
void yyerror(const char* s);
%}

%token NUMBER
%token ADD
%token SUB
%token MUL
%token DIV
%left ADD SUB
%left MUL DIV
%right UMINUS

%%


lines	:	lines expr ';'	{ printf("res = %f\n", $2);  }
	|	lines ';'
	|
	;

expr	:	expr ADD expr	{ $$ = $1 + $3; }//printf("$1 = %f\n", $1); printf("$3 = %f\n", $3); printf("$$ = %f\n", $$); }
	|	expr SUB expr	{ $$ = $1 - $3; }
	|	expr MUL expr	{ $$ = $1 * $3; }
	|	expr DIV expr	{ $$ = $1 / $3; }
	|	'(' expr ')'	{ $$ = $2; }
	|	'-' expr %prec UMINUS	{ $$ = -$2; }
	|	NUMBER	{ $$ = $1; }//printf("$1 = %f\n", $1); printf("$$ = %f\n", $$);}
	;

/*
NUMBER	:	'0'	{ $$ = 0.0; }
	|	'1'	{ $$ = 1.0; }
	|	'2'	{ $$ = 2.0; }
	|	'3'	{ $$ = 3.0; }
	|	'4'	{ $$ = 4.0; }
	|	'5'	{ $$ = 5.0; }
	|	'6'	{ $$ = 6.0; }
	|	'7'	{ $$ = 7.0; }
	|	'8'	{ $$ = 8.0; }
	|	'9'	{ $$ = 9.0; }
	;*/


%%

//programs section

int yylex() {
	//place your token retrieving code here
	char t;
	while (1) {
		t = getchar();
		//printf("%sfirst getchar\n", &t);
		if (t == ' ' || t == '\t' || t == '\n') {
			//do nothing
		}
		else if(isdigit(t)) {
			//printf("isdigit(t) == 1\n");
			yylval = 0;
			//double myDigit = 0;
			while (isdigit(t)) {
				yylval = yylval * 10 + t - '0';
				//printf("t = %s\n", &t);
				//double b = t;
				//printf("b = %f\n", b);
				//myDigit = myDigit * 10 + b - '0';
				//printf("myDigit = %f\n", myDigit);
				t = getchar();
			}
			//printf("myDigit = %f\n", myDigit);
			//yylval = myDigit;
			//printf("yylval = %f\n", yylval);
			//printf("NUMBER = %f\n", NUMBER);
			ungetc(t, stdin);
			
			//printf("%f\n", NUMBER);
			return NUMBER;
		}
		else if (t == '+') {
			return ADD;
		}
		else  if(t == '-'){return SUB;}
		else  if(t == '*'){return MUL;}
		else  if(t == '/'){return DIV;}
		else  if(t == '('){return '(';}
		else  if(t == ')'){return ')';}
		else {
			return t;
		}
	}
}

int main(void) {
	yyin = stdin;
	do {
		//yylex();
		yyparse();
	} while (!feof(yyin));
	return 0;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}
