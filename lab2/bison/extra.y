%{
/************************************************************
expr1.y
YACC file
Date: 2022/10/11
wzr <2011579@nankai.edu.cn>
************************************************************/
#include<stdio.h>
#include<stdlib.h>
#ifndef YYSTYPE
#define YYSTYPE double
//#define YYSTYPE int
#endif
int debug1012 = 0;
const int varnum = 100;
char idStr[50];//variable length max 50
char idmap[100][50];//variable number max 100
double idvalue[100];
int idmap_id = 0;
int yylex();
extern int yyparse();
FILE* yyin;
void yyerror(const char* s);
%}

%token NUMBER
%token ID
%token ADD
%token SUB
%token MUL
%token DIV
%token '='
%right '='
%left ADD SUB
%left MUL DIV
%right UMINUS '-'

%%


lines	:	lines expr ';'	{ printf("res = %f\n", $2);  }
	|	lines ';'
	|
	;

expr	:	expr ADD expr	{ $$ = $1 + $3; }//printf("$1 = %f\n", $1); printf("$3 = %f\n", $3); printf("$$ = %f\n", $$); }
	|	expr SUB expr	{ $$ = $1 - $3; }
	|	expr MUL expr	{ $$ = $1 * $3; }
	|	expr DIV expr	{ $$ = $1 / $3; }
	|	ID '=' expr	{ if(debug1012)printf("start ID = expr!\n"); $$ = $3; int index = $1; idvalue[index] = $3; }
	|	'(' expr ')'	{ $$ = $2; }
	|	'-' expr %prec UMINUS	{ $$ = -$2; }
	|	NUMBER	{ $$ = $1; }//printf("$1 = %f\n", $1); printf("$$ = %f\n", $$);}
	|	ID { if(debug1012)printf("start id\n"); int index = $1; $$ = idvalue[index]; if(debug1012)printf("idmap_id = %d\n", idmap_id); }
	;

/*
NUMBER	:	'0'	{ $$ = 0.0; }
	|	'1'	{ $$ = 1.0; }
	|	'2'	{ $$ = 2.0; }
	|	'3'	{ $$ = 3.0; }
	|	'4'	{ $$ = 4.0; }
	|	'5'	{ $$ = 5.0; }
	|	'6'	{ $$ = 6.0; }
	|	'7'	{ $$ = 7.0; }
	|	'8'	{ $$ = 8.0; }
	|	'9'	{ $$ = 9.0; }
	;*/


%%

//programs section

int isLetter(char t){
	if((t >= 'a' && t <= 'z') || (t >= 'A' && t <= 'Z'))
		return 1;
	return 0;
}

int mystrcmp(char* idmap_s, char* s, int s_len){
	for(int j = 0; j <= s_len; j ++){
		if(idmap_s[j] != s[j]){
			return 0;
		}
	}
	return 1;
}

int search(char* s, int len){
	if(debug1012)printf("start searach");
	for(int i = 0; i < varnum; i ++){
		if(debug1012)printf("i = %d\n", i);
		if(debug1012)printf("idmap[i] = %s\n", idmap[i]);
		if(debug1012)printf("s = %s\n", s);	
		if(strcmp(idmap[i], s) == 0){
			return i;
		}
	}
	return -1;
}

int yylex() {
	//place your token retrieving code here
	char t;
	while (1) {
		t = getchar();
		//printf("%sfirst getchar\n", &t);
		if (t == ' ' || t == '\t' || t == '\n') {
			//do nothing
		}
		else if(isdigit(t)) {
			//printf("isdigit(t) == 1\n");
			yylval = 0;
			//double myDigit = 0;
			while (isdigit(t)) {
				yylval = yylval * 10 + t - '0';
				//printf("t = %s\n", &t);
				//double b = t;
				//printf("b = %f\n", b);
				//myDigit = myDigit * 10 + b - '0';
				//printf("myDigit = %f\n", myDigit);
				t = getchar();
			}
			//printf("myDigit = %f\n", myDigit);
			//yylval = myDigit;
			//printf("yylval = %f\n", yylval);
			//printf("NUMBER = %f\n", NUMBER);
			ungetc(t, stdin);
			
			//printf("%f\n", NUMBER);
			return NUMBER;
		}
		else if((isLetter(t)==1) || (t == '_')){
			int c = 0;
			int id = 0;
			yylval = 0;
			while((isLetter(t)==1) || (t == '_') || (isdigit(t))){
				idStr[c++] = t;
				t = getchar();
			}
			idStr[c] = '\0';
			//yylval = idStr;
			if(debug1012)printf("idStr = %s\n", idStr);
			int exist = search(idStr, c+1);
			if(debug1012)printf("get exist = %d\n", exist);
			if(exist >= 0){
				//find it
				yylval = (double)exist;
				if(debug1012)printf("branch 1: yylval = %d\n", yylval);
			}
			else{
				//cannot find it, new id
				//idmap[idmap_id++] = idStr;
				for(int i = 0; i < c; i ++){
					idmap[idmap_id][i] = idStr[i];
				}				
				yylval = (double)idmap_id;
				idmap_id ++;
				if(debug1012)printf("branch 2: yylval = %f\n", yylval);
			}
			//idmap[idmap_id++] = idStr;
			ungetc(t, stdin);
			if(debug1012)printf("yylval = %f\n", yylval);
			if(debug1012)printf("id identify finished! ID = %f\n", ID);
			return ID;
		}
		else if(t == '='){
			return '=';
		}
		else if (t == '+') {
			return ADD;
		}
else  if(t == '-'){return SUB;}
else  if(t == '*'){return MUL;}
else  if(t == '/'){return DIV;}
else  if(t == '('){return '(';}
else  if(t == ')'){return ')';}
		else {
			return t;
		}
	}
}

int main(void) {
	yyin = stdin;
	do {
		//yylex();
		yyparse();
	} while (!feof(yyin));
	return 0;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}
