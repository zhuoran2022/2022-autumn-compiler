 #include<stdio.h>
    
    void factorial(int n){
        int i = 2, f = 1;
        while(i <= n){
            f *= i;
            i ++;
        }
        putf("f = %d\n", f);
    }
    
    int main(){
        int n;
        putf("please input n\n");
        n = getint();
        factorial(n);
        return 0;
    }